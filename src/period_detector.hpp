//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 531 2017-11-07 16:07:56Z jules $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules1/src/hub.hpp $
//-
//- DESCRIPTION:
//- Declaration of the period detector Class.
//-
//- This class is used to detect when specified periods have expired.
//-
//- Initialise it with the time out period.
//-
//- When you want to use it to detect that a period has elapsed,
//- start it with the current time.
//-
//- You can find out if the period is elapsing (an event) for the current time.
//- You can find out if the period has elapsed (the state) for the current time.

// Time flow model
//
//                    --------------------
// -------------------                    ---------------------
// ^^^ not elapsed ^^^^^^^^^^^^^^^^^^^^^^^
//             start ^
//                               elapsing ^
//                                elapsed ^^^^^^^^^^^^^^^^^^^^^

//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef PERIOD_DETECTOR_HPP
#define PERIOD_DETECTOR_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <climits>

#include "hithub_types.hpp"
#include "time.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

//-----------------------------------------------------------------------------

// Classes

//-----------------------------------------------------------------------------

class PeriodDetector {
    public:

        ///
        /// constructor
        ///
        PeriodDetector();

        ///
        /// initialise the object with the number of seconds in the period
        ///
        void initialise(long atimeoutSeconds);

        ///
        /// start the detection from now
        ///
        void start(Time::HubTime nowSeconds);

        ///
        /// determine whether the period is elapsing (event)
        ///
        bool isElapsing(Time::HubTime nowSeconds);

        ///
        /// determine whether the period has elapsed (state)
        ///
        bool isElapsed(Time::HubTime nowSeconds);

    private: protected:
        // control
            Seconds timeoutSeconds;

        // status
            Time::HubTime nextSeconds;
            bool elapsed;

};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif // PERIOD_DETECTOR_HPP
