//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- NtpMonitor Class.  Monitors synchronisation with the NTP server.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "ntp_monitor.hpp"
#include "operational_log.hpp"
#include "hub_exception.hpp"
#include "time.hpp"
#include "config_file.hpp"
#include <cstdlib>
#include <sstream>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------


// Constants

// Interval between checks on NTP synchronisation status. 
//      The value is chosen to be close to the required maximum (300s), but 
//      sufficiently less that the maximum period will not be exceeded.
const int NtpMonitor::intervalSeconds = 295;  

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------


NtpMonitor::NtpMonitor() :
    secondsToNextCheck(intervalSeconds),
    currentSyncStatus(unset),
    prevSyncStatus(unset),
    ntpHasBeenSynced(false)
{}


void NtpMonitor::tick1s()
{   
    // Define static var for detecting time shifts.
    static EpochMilliSecond previousHubMs = Time::getEpochMilliSecond() - 1000;
    // The initilisation ensures that the first tick is always accepted as valid.
    // Detect and log any unexpected shift in HubTime
    EpochMilliSecond currentHubMs = Time::getEpochMilliSecond();
    MilliSeconds diffMs = abs(currentHubMs - previousHubMs);
    if (diffMs > (1000+500))
    {
        LOG_WARNING << "<ntp>" "\t" "Time anomaly detected. Prev time(ms): " << previousHubMs <<
                       "; Current time(ms): " << currentHubMs << " diff(ms):" << diffMs;
    }

    // Set previousHubMs for next iteration
    previousHubMs = currentHubMs;

    // Wait for intervalSeconds then check for NTP synchronisation status
    if ( --secondsToNextCheck <= 0 )
    {
        syncCheck();
    }
}

void NtpMonitor::startupSync()
{
    std::string configFileName = "ntpStartupSyncStatus.cfg";
    std::string successStateID = "Startup.Success.State";
    
    try
    {
        ConfigFile configFile(configFileName, "=");
        if (configFile.getValue(successStateID) == "0")
        {
            LOG_INFO << "<ntp>" "\t" "NTP Successfully Synced on VM Startup";
            currentSyncStatus = synchronised;
            ntpHasBeenSynced = true;
        }
        else
        {
            LOG_ERROR << "<ntp>" "\t" "NTP Unsuccessfully Synced on VM Startup";
            LOG_ALERT << "<ntp>" "\t" "Failed to sync with the NTP server on Startup";
            ntpHasBeenSynced = false;
        }
    }
    catch( FileReadException &e )
    {
        LOG_ERROR << "<ntp>" "\t" "Unable to read NTP Config File. Unable to determine success state of NTP Startup Synchronisation";
        LOG_ALERT << "<ntp>" "\t" "Failed to sync with the NTP server on Startup";
        ntpHasBeenSynced = false;
    }
    catch( InvalidConfigDataException &e )
    {
        LOG_ERROR << "<ntp>" "\t" "NTP Config File is invalid. Unable to determine success state of NTP Startup Synchronisation";
        LOG_ALERT << "<ntp>" "\t" "Failed to sync with the NTP server on Startup";
        ntpHasBeenSynced = false;
    }
    catch( ConfigFile::MissingKeyException &e )
    {
        LOG_ERROR << "<ntp>" "\t" << successStateID << " is missing from NTP Config File. Unable to determine success state of NTP Startup Synchronisation";
        LOG_ALERT << "<ntp>" "\t" "Failed to sync with the NTP server on Startup";
        ntpHasBeenSynced = false;
    }
	
	if (! ntpHasBeenSynced)
    {
        LOG_TRACE << "<ntp>" "\t" "Syncing NTP";
    }
    
    while (! ntpHasBeenSynced)
    {
        // If the ntp did not sync on startup then wait for synchronisation
        syncCheck();
        sleep(1);
    }
}

void NtpMonitor::syncCheck()
{
    secondsToNextCheck = intervalSeconds;
    prevSyncStatus = currentSyncStatus;
        
    // Perform system call eliminating any writes to stdout or stderr
    int cmdStatus = system("ntpstat > /dev/null 2>&1");

    // Check result
    if (WIFEXITED(cmdStatus))
    {
        currentSyncStatus = static_cast<ntpStatusCodes>(WEXITSTATUS(cmdStatus));
            
        // Handle any change of status
        if ( currentSyncStatus != prevSyncStatus )
        {
            if ( currentSyncStatus == synchronised )
            {
                ntpHasBeenSynced = true;
                LOG_INFO << "<ntp>" "\t" "NTP Synchronisation Status change: Synchronised";
            }
            else
            {
                LOG_ALERT << "<ntp>" "\t" "NTP Synchronisation (error code = " << currentSyncStatus << ")";
            }
        }
    }
    else
    {
        // System call failure. Generate error string and throw exception.
        std::ostringstream ss;
        ss << cmdStatus;
        std::string exceptionMsg = "NtpMonitor system call failed (error code = " +  
            ss.str() + ")";
        throw HubException(exceptionMsg , true);
    }
}

bool NtpMonitor::isSynchronised()
{
    return (currentSyncStatus == synchronised);
}

bool NtpMonitor::isAvailable()
{
    return (isSynchronised() || (currentSyncStatus == notSynchronised));
}

bool NtpMonitor::hasBeenSynced()
{
    return ntpHasBeenSynced;
}

