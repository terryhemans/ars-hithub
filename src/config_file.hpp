//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION:
//- ConfigFileTemplate Class.  Encapsulates the reading of a configuration file
//- consisting of key value pairs separated by a delimiter.  The class reads
//- the file, removes all whitespace and comments, and makes the data
//- available in the form of a vector of key value pairs.
//-
//-----------------------------------------------------------------------------

#ifndef KEY_VALUE_CONFIG_FILE_HPP
#define KEY_VALUE_CONFIG_FILE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "hub_exception.hpp"
#include "config_file_template.hpp"

#include <string>
#include <array>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class ConfigFile : protected ConfigFileTemplate<2>
{

    public:

        ///
        /// Exception Declarations
        ///

        // Thrown by getValue() if a key cannot be found in the configuration data
        class MissingKeyException : public HubException {
           public: MissingKeyException( std::string key ) : HubException( key, true ){};
        };


        ///
        /// Constructor
        ///
        /// Opens and reads the given file ignoring blank and comment lines and extracting pairs of string values
        /// from the file.
        ///
        /// @param filename - the path to the file to read
        /// @param delimiters - a string containing one or more characters
        ///
        /// Throws FileReadException if the file cannot be read.
        /// Throws InvalidConfigDataException if the file cannot be parsed
        ///
        ConfigFile( std::string filename, std::string delimiters ) throw (HubException) :
            ConfigFileTemplate<2>( filename, delimiters ){};


        ConfigFile() : ConfigFileTemplate<2>(){};

        using ConfigFileTemplate<2>::Initialise;
        using ConfigFileTemplate<2>::getData;


        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// returns true if the key is found in the configuration data
        ///
        bool findLong(const std::string & key, long & value) const throw (InvalidConfigDataException);

        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// returns the value
        ///
        /// Can throw MissingKeyException or InvalidConfigDataException
        ///
        long getLong(const std::string & key) const throw (MissingKeyException, InvalidConfigDataException);

        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// returns true if the key is found in the configuration data
        ///
        bool findBool(const std::string & key, bool & value) const throw (InvalidConfigDataException);

        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// returns the value
        ///
        /// Can throw MissingKeyException or InvalidConfigDataException
        ///
        bool getBool(const std::string & key) const throw (MissingKeyException, InvalidConfigDataException);

        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// returns true if the key is not found in the configuration data
        ///
        bool findValue(const std::string & key, std::string & value) const;

        ///
        /// Searches the left side of each pair for the given key and returns the corresponding right side.
        ///
        /// Throws MissingKeyException if the key is not found in the configuration data
        ///
        std::string getValue( std::string key ) const throw (MissingKeyException);

protected:
        ///
        /// Searches the left side of each pair for the given key and returns index (0+).
        ///
        /// returns <0 if the key is not found in the configuration data
        ///
        int getKeyIndex(const std::string & key) const;

        ///
        /// return true if key allowed
        ///
        virtual bool keyAllowed(const std::string & key);
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif  // KEY_VALUE_CONFIG_FILE_HPP
