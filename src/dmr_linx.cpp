//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.    All rights reserved.
//-
//- $Id: linx.cpp 1879 2018-04-03 15:26:26Z jules $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules2/src/dmr_linx.cpp $
//-
//- DESCRIPTION:
//- Implementation of the DmrLinx class.
//- Provides Linx access incorporating DMR.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>

#include "dmr_linx.hpp"
#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

const unsigned int NUM_OF_QUEUE_ITEMS = 1000;

// Enumerations

// Structures

// Variables


//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

DmrLinx::DmrLinx() :
    DmrMachine("linx",10) //**** read primary check period from config file
{
    LOG_TRACE << "Constructing the DmrLinx class";
}

//-----------------------------------------------------------------------------

void DmrLinx::initialiseConnection(const LinxInterface::ConnectionDetails connection )
{
    LOG_TRACE << "Initiate DmrLinx connection";

    linxes[Primary].initialiseConnection(connection);

    LinxInterface::ConnectionDetails connection2 = connection;

    connection2.connectionName = connection2.secondaryConnectionName;

    connection2.secondaryConnectionName = "ThisIsSecondaryconnection";

    linxes[Secondary].initialiseConnection(connection2);
}

//-----------------------------------------------------------------------------

DmrLinx::~DmrLinx()
{
    linxes[Primary].disconnect();
    linxes[Secondary].disconnect();
}

//-----------------------------------------------------------------------------

bool DmrLinx::isAvailable()
{
    Time::HubTime nowSeconds = Time::getCurrentTime();
    bool primaryAvailable = linxes[Primary].isAvailable();
    bool secondaryAvailable = linxes[Secondary].isAvailable();
    return dmrUpdate(nowSeconds, primaryAvailable, secondaryAvailable);
}

//-----------------------------------------------------------------------------

bool DmrLinx::dmrConnect(Selected selected)
{
    LOG_TRACE << "DmrLinx::dmrConnect:" << selected;

    linxes[selected].connect();

    return linxes[selected].isAvailable();
}

//-----------------------------------------------------------------------------

void DmrLinx::dmrDisconnect(Selected selected)
{
    LOG_TRACE << "DmrLinx::dmrDisconnect:" << selected;

    linxes[selected].disconnect();
}

//-----------------------------------------------------------------------------

bool DmrLinx::removeMessage(FlowId flowId, std::string & message)
{
    return linxes[getSelected()].removeMessage(flowId, message);
}

//-----------------------------------------------------------------------------

bool DmrLinx::appendMessage(FlowId flowId, const char * chars)
{
    return linxes[getSelected()].appendMessage(flowId, chars);
}

//-----------------------------------------------------------------------------

void DmrLinx::tick1s()
{
    linxes[getSelected()].tick1s();

    isAvailable(); // update DMR machine
}

//-----------------------------------------------------------------------------
