//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- The implementation of dmr machine
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "dmr_machine.hpp"

#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//  class DmrMachine

//-----------------------------------------------------------------------------

DmrMachine::DmrMachine(std::string aName, Seconds aPrimaryCheckPeriod) :
    name(aName),
    primaryCheckPeriod(aPrimaryCheckPeriod),
    state(dmsConnectToPrimary),
    selected(Primary),
    overallAvailability(false)
{
    LOG_TRACE << "In DmrMachine Constructor";
	dmrReset();
}

//-----------------------------------------------------------------------------

void DmrMachine::dmrReset()
{
    state = dmsConnectToPrimary;
    selected = Primary;
    overallAvailability = false;

    primaryCheckPeriodDetector.initialise(primaryCheckPeriod);
}

//-----------------------------------------------------------------------------

bool DmrMachine::dmrUpdate(Time::HubTime nowSeconds, bool primaryAvailable, bool secondaryAvailable)
{
    #define LOG_MESSAGE(channel, text1, text2) \
        LOG_INFO << \
        "<interface>" \
        "<" << name << ">" \
        "<" << channel << ">" \
        "\t" << text1 << \
        " " << name << \
        " " << text2

    switch (state) {

    case dmsConnectToPrimary:
        // SWREQ 3.2-1
        LOG_MESSAGE("primary", "Connecting", "primary link");
        primaryAvailable = dmrConnect(Primary);
        if (primaryAvailable) {
            state = dmsPrimaryConnected;
            // SWREQ 3.2-9
            LOG_MESSAGE("primary", "Connected", "primary link");
            overallAvailability = true;
            selected = Primary;
        } else {
            state = dmsConnectToSecondary;
        }
        break;

    case dmsPrimaryConnected:
        if (! primaryAvailable) {
            state = dmsConnectToSecondary;
            // SWREQ 3.2-7
            LOG_MESSAGE("primary", "Disconnected", "primary link");
            dmrDisconnect(Primary);
            overallAvailability = false;
        } // else no change
        break;

    case dmsConnectToSecondary:
        // SWREQ 3.2-2
        LOG_MESSAGE("secondary", "Connecting", "secondary link");
        secondaryAvailable = dmrConnect(Secondary);
        if (secondaryAvailable) {
            state = dmsSecondaryConnected;
            // SWREQ 3.2-10
            LOG_MESSAGE("secondary", "Connected", "secondary link");
            overallAvailability = true;
            selected = Secondary;
            primaryCheckPeriodDetector.start(nowSeconds);
        } else {
            state = dmsConnectToPrimary;
        }
        break;
        
    case dmsSecondaryConnected:
        if (! secondaryAvailable) {
            state = dmsConnectToPrimary;
            // SWREQ 3.2-8
            LOG_MESSAGE("secondary", "Disconnected", "secondary link");
            dmrDisconnect(Secondary);
            overallAvailability = false;
        } else {
            if (primaryCheckPeriodDetector.isElapsing(nowSeconds)) {
                LOG_MESSAGE("primary", "Retrying connection to", "primary link");
                // SWREQ 3.2-3
                state = dmsPrimaryRetry;
            } // else no change
        }
        break;

    case dmsPrimaryRetry:
        primaryAvailable = dmrConnect(Primary);
        if (primaryAvailable) {
            // SWREQ 3.2-3
            state = dmsPrimaryConnected;
            // SWREQ 3.2-6
            dmrDisconnect(Secondary);
            LOG_MESSAGE("secondary", "Disconnected", "secondary link");
            // SWREQ 3.2-9
            LOG_MESSAGE("primary", "Connected", "primary link");
            overallAvailability = true;
            selected = Primary;
        } else {
            LOG_MESSAGE("primary", "Failed to reconnect to", "primary link");
            state = dmsSecondaryConnected;
            primaryCheckPeriodDetector.start(nowSeconds);
        }
        break;
    }

    return overallAvailability;
}

//-----------------------------------------------------------------------------

// eof
