//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- Copyright (c) 2017 Hitachi Limited. All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION: Declares the Hitachi Hub config keys.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef CONFIG_KEYS_HPP
#define CONFIG_KEYS_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- CONSTANT DEFINITIONS
//-----------------------------------------------------------------------------

#define LINX_CONNECTION_PERIOD_SECONDS "linx.logging.connectionPeriodSeconds"
#define LINX_DISCONNECTION_PERIOD_SECONDS "linx.logging.disconnectionPeriodSeconds"
#define LINX_INTERMITTENT_PERIOD_SECONDS "linx.logging.intermittentPeriodSeconds"
#define LINX_INTERMITTENT_THRESHOLD "linx.logging.intermittentLimit"

#define TRANISTA_CONNECTION_PERIOD_SECONDS "tranista.logging.connectionPeriodSeconds"
#define TRANISTA_DISCONNECTION_PERIOD_SECONDS "tranista.logging.disconnectionPeriodSeconds"
#define TRANISTA_INTERMITTENT_PERIOD_SECONDS "tranista.logging.intermittentPeriodSeconds"
#define TRANISTA_INTERMITTENT_THRESHOLD "tranista.logging.intermittentLimit"

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

#endif  // CONFIG_KEYS_HPP
