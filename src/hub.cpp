//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.cpp 4588 2020-01-17 14:15:51Z terry $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/hub.cpp $
//-
//- DESCRIPTION:
//- Hub Class.  Top level class representing the Hitachi Hub.  All of the
//- functional logic is implemented here, with interface logic implemented in
//- the Linx and Tranista classes.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <algorithm>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

#include "flows.hpp"
#include "xmlConverter.hpp"
#include "xmlValidator.hpp"

#include "hub.hpp"

#include "config_data.hpp"
#include "config_file.hpp"
#include "linx_connection_machine.hpp"
#include "linx_interface.hpp"
#include "operational_log.hpp"
#include "time.hpp"
#include "version.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

Hub::Hub(LinxInterface * linxInterface) :
    healthStatus(starting),
    linx(* linxInterface),
    linxConnectionMachine_p(0),
    tranistaErrorsDisabled(false),
    disableForecast(false),
    disableTJM(false),
	xmlValidator("xsdFiles/GB_TAF_TAP_TSI_complete.xsd")
{
    setLogStatus("Starting");

    LOG_INFO << "<status>" "\t" "Starting";
    LOG_INFO << "\t" "Starting Hitachi Hub...";
    LOG_INFO << "\t" "Version: " << HITHUB_VERSION;

    flushOperationalLog();

    // Read the configuration data and construct the Hub components
    try{

        // Read and extract the configuration data
        ConfigFile configFile(configData.hithubFilename(), "=");

        LinxInterface::ConnectionDetails linxConDetails;
        linxConDetails.connectionName = configFile.getValue("linx.connection");
        linxConDetails.secondaryConnectionName = configFile.getValue("linx.secondary.connection");
        linxConDetails.managerName = configFile.getValue("linx.manager");
        linxConDetails.username = configFile.getValue("linx.username");
        linxConDetails.password = configFile.getValue("linx.password");
        linxConDetails.channelName = configFile.getValue("linx.channel");
        linxConDetails.queueNames[FlowIdS002] = configFile.getValue("linx.s002.queue");
        linxConDetails.queueNames[FlowIdS009] = configFile.getValue("linx.s009.queue");
        linxConDetails.queueNames[FlowIdS013] = configFile.getValue("linx.s013.queue");
        linxConDetails.queueNames[FlowIdS015] = configFile.getValue("linx.s015.queue");

        linxConnectionMachine_p = new LinxConnectionMachine(configFile);

        // initialise the LINX interface
        linx.initialiseConnection(linxConDetails);

        {
            long v = 0;
            if (configFile.findLong("log.level", v)) {
                logLevel = static_cast<LogLevel>(v);
            }
        }

		ntpMonitor.startupSync();
    }
    catch (std::bad_alloc e)
    {
      throw HubException( std::string("Error allocating memory for message handlers. ") + e.what(), false);
    }
    catch( FileReadException &e )
    {
        throw HubException( std::string("Error reading hithub.cfg: ") + e.what(), false);
    }
    catch( InvalidConfigDataException &e )
    {
        throw HubException( std::string("hithub.cfg is invalid: ") +  e.what(), false);
    }
    catch( ConfigFile::MissingKeyException &e )
    {
        throw HubException( std::string("Field '") + e.what() + "' is missing from hithub.cfg", false);
    }
    catch( std::exception &e )
    {
        throw HubException( std::string("Error parsing hithub.cfg: ") + e.what(), false);
    }
}

//-----------------------------------------------------------------------------

Hub::~Hub()
{
    LOG_INFO << "\t" "Shutting Down...";

    delete linxConnectionMachine_p;
	
    LOG_INFO << "\t" "Hitachi Hub End";
}

//-----------------------------------------------------------------------------

void Hub::run(long testIterations, long testDurationSeconds)
{
	if( testIterations >= 0 ) {
        LOG_DEBUG << "<status>" "\t" "TestIterations: " << testIterations;
    }

    if( testDurationSeconds >= 0 ) {
        LOG_DEBUG << "<status>" "\t" "TestDuration: " << testDurationSeconds;
    }
	
    EpochMilliSecond firstTimeInMs = 0;
    EpochMilliSecond lastTimeInMs = Time::getEpochMilliSecond(Time::gctInitial);

    linx.tick1s(); // get connected

    while( (testIterations != 0) && (testDurationSeconds != 0))
    {
        EpochMilliSecond currentTimeInMs = Time::getEpochMilliSecond(Time::gctNextFrame);
        Time::HubTime currentTimeInSeconds = currentTimeInMs / 1000;

        // decrement timer for test purposes
        if( testDurationSeconds >= 0 ) {
            if (firstTimeInMs == 0) {
                firstTimeInMs = currentTimeInMs;
            }

            if (lastTimeInMs != 0) {
                EpochMilliSecond expireTimeMs = firstTimeInMs + testDurationSeconds * 1000;
                if (currentTimeInMs >= expireTimeMs) {
                   testDurationSeconds = 0;
                }
            }
        }

        // get the status of all external links and update the software health status
        bool linxAvailable = linx.isAvailable();
        bool ttsAvailable = true;

        updateHealthStatus( linxAvailable, ttsAvailable );

        // Determine if the second has changed
        bool secondChanged = (currentTimeInMs < lastTimeInMs) || ((currentTimeInMs - lastTimeInMs) >= 1000);

        if( secondChanged )
        {
            //LOG_TRACE << "<Hub::run><secondChanged>" "\t" << "s(" << currentTimeInSeconds << ") ms(" << currentTimeInMs << ")";
            
            if ((currentTimeInMs < lastTimeInMs) || ((currentTimeInMs - lastTimeInMs) >= (2 * 1000))) {
                // reset at start of program or if we encounter a big time jump
                lastTimeInMs = currentTimeInMs;
                
            } else {
                lastTimeInMs += 1000;
            }

            // maintain the linx connection
            linx.tick1s();

            // monitor NTP status
            ntpMonitor.tick1s();

            // this is done to just produce the logs and alerts
            linxConnectionMachine_p->isHighlyAvailable(currentTimeInSeconds, linxAvailable);
        }

        // Perform main processing
        if (ntpMonitor.hasBeenSynced())
        {			
			for (int flowInt = FlowIdS002; flowInt != FlowIdCount; flowInt++)
			{
				std::string message = "";
				linx.removeMessage(static_cast<FlowId>(flowInt), message);
				if (message != "" && xmlValidator.validateXmlAgainstXsd(message))
				{
					std::string convertedXML = converter.convertXMLtoJSON(message);
					if (convertedXML != "")
					{
						LOG_TRACE << convertedXML;
					}
				}
			}
        }

        // flush the logger
        flushOperationalLog();

        // decrement counter for test purposes
        if( testIterations > 0 ) testIterations--;
    }

    if( testIterations == 0 ) {
        LOG_DEBUG << "<status>" "\t" "TestIterations completed";
    }

    if( testDurationSeconds == 0 ) {
        LOG_DEBUG << "<status>" "\t" "TestDuration expired";
    }
}


//-----------------------------------------------------------------------------

void Hub::updateHealthStatus( bool linxAvailable, bool ttsAvailable )
{
    switch( healthStatus )
    {
        case starting:
            if( linxAvailable && ttsAvailable )
            {
                healthStatus = healthy;
                setLogStatus("Healthy");
                LOG_INFO << "<status>" "\t" "Healthy";
            }
            else
            {
                healthStatus = compromised;
                setLogStatus("Compromised");
                LOG_INFO << "<status>" "\t" "Compromised";
            }
            break;
        case healthy:
            if( !(linxAvailable && ttsAvailable) )
            {
                healthStatus = compromised;
                setLogStatus("Compromised");
                LOG_INFO << "<status>" "\t" "Compromised";
            }
            break;
        case compromised:
            if( linxAvailable && ttsAvailable )
            {
                healthStatus = healthy;
                setLogStatus("Healthy");
                LOG_INFO << "<status>" "\t" "Healthy";
            }
            break;
        case failed:
            break;
    }
}
//-----------------------------------------------------------------------------
