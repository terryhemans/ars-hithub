#include "xmlValidator.hpp"
#include "operational_log.hpp"
#include "common.hpp"
#include <iostream>
#include <stdio.h>
#include <stdexcept>

XMLValidator::XMLValidator(std::string filename)
{
	xsdFilename = filename;
}

bool XMLValidator::validateXmlAgainstXsd(const std::string & xml)
{
	std::string xmllint = "xmllint --noout --schema ";
	std::string command = xmllint + xsdFilename + " - <<<'" + xml + "' 2>/dev/null";

	int cmd = system(command.c_str());

	if (! WIFEXITED(cmd))
	{
		LOG_ERROR << "[validateXmlAgainstXsd:throw]";
	}
	
	bool result = (WEXITSTATUS(cmd) == 0);

	if (! result) {
		
		command = xmllint + xsdFilename + " - <<<'" + xml + "' 2>&1";

		std::string result = "XML Validation Fail: " + runCommandGetOutput(command.c_str());
		LOG_WARNING << result;
	}

	return result;
}

std::string XMLValidator::runCommandGetOutput(const char* cmd)
{
	char buffer[128];
	std::string result = "";
	FILE* pipe = popen(cmd, "r");
	if(!pipe)
	{
		throw std::runtime_error("popen() failed");
	}
	try
	{
		while (fgets(buffer, sizeof buffer, pipe) != NULL)
		{
			result += buffer;
		}
	}catch (...)
	{
		pclose(pipe);
		throw;
	}
	pclose(pipe);
	replaceSubStrings(result, "\n", "");
	trimWhitespace(result);
	return result;
}

