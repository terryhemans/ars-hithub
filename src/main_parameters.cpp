//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: main.cpp 373 2017-10-23 13:59:44Z jules $
//-
//- $URL: svn+ssh://77.68.12.143/var/svn/repos/hithub/branches/jules1/src/main.cpp $
//-
//- DESCRIPTION:
//- Provides parameter handling function for main.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <chrono>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include "main_parameters.hpp"

#include "common.hpp"
#include "config_data.hpp"
#include "operational_log.hpp"
#include "time.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

static EpochMilliSecond the_startTimeMs = 0;
static EpochMilliSecond the_testTimeMs = 0;
static EpochMilliSecond the_lastTimeMs = 0;
static MilliSeconds the_testTimeDeltaMs = 0;
static long the_testTimeRate = 1;

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

static void usage(const char * chars = 0)
{
    if (chars) printf("\n" "hithub command line tail error: %s\n", chars);

    printf("hithub [-c[a,b,c,h,l,t] ARG] [-d ARG] [-i ARG] [-l ARG] [-r ARG] [-s ARG] [-t ARG]\n"
           "    [-c config filename]\n"
           "       Use -c or -ch for overriding 'hithub.cfg'\n"
           "    [-d test time delta in milliseconds]\n"
           "    [-i test iterations]\n"
           "    [-l log level]\n"
           "    [-r test time rate]\n"
           "    [-s test runtime duration in seconds]\n"
           "    [-t test start time in seconds]\n");

    exit(1);
}

//-----------------------------------------------------------------------------

// test time routine: provides controllable time

static EpochMilliSecond get_time_milli_second(Time::GetCurrentTimeType currentTimeType)
{
    if (currentTimeType == Time::gctInitial) {
        // this value returned for some old tests which require it
        return 0;

    } else { // currentTimeType != Time::gctInitial
        if (the_testTimeDeltaMs == 0) {
            // configured the_testTimeMs and/or the_testTimeRate
            EpochMilliSecond nowMs = Time::getEpochMilliSecondNow();
            MilliSeconds sinceStartMs = nowMs - the_startTimeMs;
            MilliSeconds ratedMs = the_testTimeRate * sinceStartMs;
            the_lastTimeMs = the_testTimeMs + ratedMs;

            return the_lastTimeMs;
            
        } else if (currentTimeType == Time::gctSnapShot) {
            // produce Snap Shot time
            EpochMilliSecond nowMs = Time::getEpochMilliSecondNow();
            MilliSeconds sinceStartMs = nowMs - the_startTimeMs;
            MilliSeconds ratedMs = the_testTimeRate * sinceStartMs;
            MilliSeconds resultantMs = the_testTimeMs + ratedMs;

            return resultantMs;
            
        } else if (currentTimeType == Time::gctCurrentFrame) {
            // do not change time during frame
            return the_lastTimeMs;

        } else { // currentTimeType == Time::gctNextFrame
            // only change time at start of frame
            the_lastTimeMs = the_testTimeMs;

            // increment the time by given milliseconds each frame
            the_testTimeMs += the_testTimeDeltaMs;

            return the_lastTimeMs;
        }
    }
}

//-----------------------------------------------------------------------------

/*export*/ long handleParameters(int argc, char* argv[], Seconds &testDuration)
{
    long testIterations = -1;

    testDuration = -1;

    if ((argc & 1) == 0) usage();

    the_startTimeMs = Time::getEpochMilliSecondNow();
    the_testTimeMs = the_startTimeMs;
    
    for (int i = 1; i < argc; i +=2) {
        if (argv[i][0] != '-') usage();

        std::string s;
        
        switch (argv[i][1]) {

        case 'c':
            switch (argv[i][2]) {
            case '\0':
            case 'h':
                configData.hithubFilenameSet(argv[i + 1]);
                printf("hithub: config filename: %s\n", configData.hithubFilename().c_str());
                break;
            default:
                usage("Unknown option");
                break;
            }
            break;

        case 'i':
            testIterations = atoi(argv[i + 1]);
            if (testIterations == 0) usage("Invalid iterations");
            printf("hithub: test iterations: %ld\n", testIterations);
            break;

        case 'l':
            logLevel = static_cast<LogLevel>(atoi(argv[i + 1]));
            printf("hithub: logLevel: %ld\n", static_cast<long>(logLevel));
            break;

        case 'r':
            the_testTimeRate = atoi(argv[i + 1]);
            if (the_testTimeRate < 1) usage("Invalid time rate");
            get_time_milli_second_p = get_time_milli_second;
            printf("hithub: test time rate: %ld\n", the_testTimeRate);
            break;

        case 's':
            testDuration = atoi(argv[i + 1]);
            printf("hithub: test runtime duration in seconds: %ld\n", testDuration);
            break;

        case 't':
            s = argv[i + 1];
            the_testTimeMs = std::stoll(s) * 1000;
            if (the_testTimeMs == 0) usage("Invalid start time");
            get_time_milli_second_p = get_time_milli_second;
            printf("hithub: test start time in seconds: %lld\n", the_testTimeMs / 1000);
            break;

        case 'd':
            the_testTimeDeltaMs = atoi(argv[i + 1]);
            if (the_testTimeDeltaMs < 1) usage("Invalid time delta");
            get_time_milli_second_p = get_time_milli_second;
            printf("hithub: test time delta in milliseconds: %lld\n", the_testTimeDeltaMs);
            break;

        default:
            usage("Unknown option");
            break;
        }
    }

    return testIterations;
}

//-----------------------------------------------------------------------------
