//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- The implementation of connection machine
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "connection_machine.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//  class ConnectionMachine

//-----------------------------------------------------------------------------

ConnectionMachine::ConnectionMachine(long aConnectionPeriod, long aDisconnectionPeriod,
                 long aIntermittentPeriod, long aIntermittentLimit)
{
    // control
        connectionPeriod = aConnectionPeriod;
        disconnectionPeriod = aDisconnectionPeriod;
        intermittentPeriod = aIntermittentPeriod;
        intermittentLimit = aIntermittentLimit;

    // status
        state = cmsStartup;

        connectionCount = 0;
        intermittentCount = 0;

    // initialise
        connectionPeriodDetector.initialise(connectionPeriod);
        disconnectionPeriodDetector.initialise(disconnectionPeriod);
        intermittentPeriodDetector.initialise(intermittentPeriod);
}

//-----------------------------------------------------------------------------

ConnectionMachine::Transitions ConnectionMachine::update(Time::HubTime nowSeconds, bool available)
{
    Transitions transition = cmtNoChange;

    switch (state) {

    case cmsStartup:
        if (available) {
            state = cmsConnecting;
            connectionPeriodDetector.start(nowSeconds);
        }
        else{
            state = cmsDisconnecting;
            disconnectionPeriodDetector.start(nowSeconds);
        }
        intermittentPeriodDetector.start(nowSeconds);
        intermittentCount = 0;
        break;

    case cmsDisconnected:
        if (available) {
            state = cmsConnecting;
            transition = cmtConnecting;
            connectionPeriodDetector.start(nowSeconds);
            intermittentCount = LONG_MIN; // disable detection
        }
        break;

    case cmsConnecting:
        if (available) {
            if (connectionPeriodDetector.isElapsing(nowSeconds)) {
                state = cmsConnected;
                transition = connectionCount ? cmtReconnected : cmtConnected;
                connectionCount++;
            }
        } else {
            state = cmsDisconnecting;
            transition = cmtDisconnecting;
            intermittentCount++;
            if (intermittentCount >= intermittentLimit) {
                if (intermittentPeriodDetector.isElapsed(nowSeconds)) {
                    transition = cmtIntermittent;

                    // From req anal:
                    // "When an intermittent failure is logged
                    // it is assumed that the Hub should
                    // clear its detection algorithm and
                    // only log the same event again after
                    // another period has expired if the problem continues"
                    intermittentPeriodDetector.start(nowSeconds);
                    intermittentCount = 0;
                }
            }
        }
        break;

    case cmsConnected:
        if (! available) {
            state = cmsDisconnecting;
            transition = cmtDisconnecting;
            disconnectionPeriodDetector.start(nowSeconds);
            intermittentPeriodDetector.start(nowSeconds);
            intermittentCount = 0;
        }
        break;

    case cmsDisconnecting:
        if (! available) {
            if (disconnectionPeriodDetector.isElapsing(nowSeconds)) {
                state = cmsDisconnected;
                transition = cmtDisconnected;
            }
        } else {
            state = cmsConnecting;
            transition = cmtConnecting;
            connectionPeriodDetector.start(nowSeconds);
        }
        break;
    }

    return transition;
}

//-----------------------------------------------------------------------------

// eof
