//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- Copyright (c) 2017 Hitachi Limited. All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION: Declares the Hitachi Hub Tranista types.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef HITHUB_TYPES_HPP
#define HITHUB_TYPES_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- CONSTANT DEFINITIONS
//-----------------------------------------------------------------------------

const long secondsPerDay = 24 * 60 * 60;

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

// general

typedef std::vector<std::string> StringList;

typedef std::vector<StringList> StringLists;

// times

// the Epoch started at 00:00 in 1970/01/01

typedef long DaySecond; // the second in a day: 0 .. (24 * 60 * 60) - 1

typedef long EpochDay; // the day in the Epoch
typedef long long EpochSecond; // the second in the Epoch
typedef long long EpochMilliSecond; // the millisecond in the Epoch

// durations

typedef long Days; // a number of days
typedef long Seconds; // a number of seconds
typedef long long MilliSeconds; // a number of milliseconds

// database

typedef std::vector<std::string> Record;

typedef std::vector<Record> Table;

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#ifdef __GNUG__
    // not provided in GCC 6.4.0, but useful to enable compilation
    extern char *strptime(const char *, const char *, struct tm *);
#endif

//-----------------------------------------------------------------------------

#endif  // HITHUB_TYPES_HPP
