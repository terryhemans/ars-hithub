//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:$
//-
//- $URL:$
//-
//- DESCRIPTION:
//- HubException class used by the application for all exception handling.
//- A HubException is a runtime exception with an added 'recoverable' flag to
//- indicate whether the error is transient or permanent.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef HUB_EXCEPTION_HPP
#define HUB_EXCEPTION_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <stdexcept>
#include <string>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class HubException : public std::runtime_error
{
    private:

        /// Indicates whether the error is temporary and hence whether the action may be retried later
        bool recoverable;

    public:

        ///
        /// Constructor
        ///
        /// Constructs a runtime exception with the given message and stores the recoverable flag.
        ///
        HubException( std::string msg, bool recoverable = true ) : std::runtime_error(msg)
        {
            this->recoverable = recoverable;
        }


        ///
        /// isRecoverable()
        ///
        /// Returns true if the error is transient indicating that the operation may succeed if tried again.
        ///
        virtual bool isRecoverable(){ return recoverable; };

};


#endif // HUB_EXCEPTION_HPP
