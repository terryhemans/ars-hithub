//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- The implementation of linx connection machine
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "linx_connection_machine.hpp"

#include "config_file.hpp"
#include "config_keys.hpp"
#include "connection_machine.hpp"
#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//  class LinxConnectionMachine

//-----------------------------------------------------------------------------

LinxConnectionMachine::LinxConnectionMachine(const ConfigFile & configFile)
{
    connectionMachine_p = 0;

    previousAvailability = false;

    // SWREQ-2.3.3-2
    long linxConnectionPeriodSeconds =
        std::stoi( configFile.getValue(LINX_CONNECTION_PERIOD_SECONDS) );

    // SWREQ-2.3.3-1
    long linxDisconnectionPeriodSeconds =
        std::stoi( configFile.getValue(LINX_DISCONNECTION_PERIOD_SECONDS) );

    // SWREQ-2.3.3-3
    long linxIntermittentPeriodSeconds =
        std::stoi( configFile.getValue(LINX_INTERMITTENT_PERIOD_SECONDS) );

    // SWREQ-2.3.3-3
    long linxIntermittentLimit =
        std::stoi( configFile.getValue(LINX_INTERMITTENT_THRESHOLD) );

    connectionMachine_p = new ConnectionMachine(
        linxConnectionPeriodSeconds, linxDisconnectionPeriodSeconds,
        linxIntermittentPeriodSeconds,linxIntermittentLimit);
}

//-----------------------------------------------------------------------------

LinxConnectionMachine::~LinxConnectionMachine()
{
    if (connectionMachine_p) delete connectionMachine_p;
}

//-----------------------------------------------------------------------------

bool LinxConnectionMachine::isHighlyAvailable(Time::HubTime time, bool availability)
{
    ConnectionMachine::Transitions transition =
            connectionMachine_p->update(time, availability);

    switch (transition) {

    case ConnectionMachine::cmtReconnected:
        // assumed to be the same as below
        // fall thru
    case ConnectionMachine::cmtConnected:
        // SWREQ-2.3.3-2
        LOG_INFO << "<interface><linx>" "\t" "Connection with LINX established";
        //not reqd LOG_ALERT << "<interface><linx>" "\t" "Connection with LINX established";
        previousAvailability = true;
        break;

    case ConnectionMachine::cmtDisconnected:
        // SWREQ-2.3.3-1
        // SWREQ-2.3.3-4
        LOG_INFO << "<interface><linx>" "\t" "Connection with LINX lost";
        LOG_ALERT << "<interface><linx>" "\t" "Connection with LINX lost";
        previousAvailability = false;
        break;

    case ConnectionMachine::cmtIntermittent:
        // SWREQ-2.3.3-3
        LOG_INFO << "<interface><linx>" "\t" "LINX Connection intermittent failure has occurred";
        LOG_ALERT << "<interface><linx>" "\t" "LINX Connection intermittent failure has occurred";
        previousAvailability = false;
        break;

    case ConnectionMachine::cmtNoChange:
        // do nothing
        break;

    case ConnectionMachine::cmtConnecting:
        // no requirement
        LOG_TRACE << "<interface><linx>" "\t" "Connecting with LINX";
        previousAvailability = false;
        break;

    case ConnectionMachine::cmtDisconnecting:
        // no requirement
        LOG_TRACE << "<interface><linx>" "\t" "Disconnecting from LINX";
        previousAvailability = false;
        break;
    }

    return previousAvailability;
}

//-----------------------------------------------------------------------------

// eof
