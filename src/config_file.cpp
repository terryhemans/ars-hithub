//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:$
//-
//- $URL:$
//-
//- DESCRIPTION:
//- Implementation of the ConfigFileTemplate class.  Reads the file into the public
//- 'contents' field on construction, throwing an error if the file is missing
//- or invalid.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "config_file.hpp"

#include "common.hpp"
#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Enumerations

// Structures

// Constants

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

// Class

//-----------------------------------------------------------------------------

bool ConfigFile::findLong(const std::string & key, long & value) const throw (InvalidConfigDataException)
{
    std::string valueString;

    bool found = findValue(key, valueString);
    
    if (found) 
    {
        if (!longOfString(valueString, value))
        {
            std::string exceptionString = "Cannot convert value for key " + key + " to number";
            throw InvalidConfigDataException(exceptionString);
        }
    }

    return found;
}

//-----------------------------------------------------------------------------

long ConfigFile::getLong(const std::string & key) const throw (MissingKeyException, InvalidConfigDataException)
{
    long result;

    bool found = findLong(key, result);

    if (! found) 
    {
        std::string exceptionString = "Cannot find key " + key + " in config data";
        throw MissingKeyException(exceptionString);
    }

    return result;
}

//-----------------------------------------------------------------------------

bool ConfigFile::findBool(const std::string & key, bool & value) const throw (InvalidConfigDataException)
{
    std::string valueString;

    bool found = findValue(key, valueString);

    if (found) 
    {
        if (!boolOfString(valueString, value))
        {
            std::string exceptionString = "Cannot convert value for key " + key + " to bool";
            throw InvalidConfigDataException(exceptionString);
        }
    }
    
    return found;
}

//-----------------------------------------------------------------------------

bool ConfigFile::getBool(const std::string & key) const throw (MissingKeyException, InvalidConfigDataException)
{
    bool result;

    bool found = findBool(key, result);

    if (! found) 
    {
        std::string exceptionString = "Cannot find key " + key + " in config data";
        throw MissingKeyException(exceptionString);
    }

    return result;
}

//-----------------------------------------------------------------------------

int ConfigFile::getKeyIndex(const std::string & key) const
{
    const std::string upperKey = upperCaseOf(key);
    
    unsigned int size = contents.size();

    for( unsigned int i=0; i<size; i++ )
    {
        const std::array<std::string,2> & fields = contents.at(i);

        if( upperCaseOf(fields[0]) == upperKey )
        {
            //LOG_TRACE << "ConfigFile::getKeyIndex:Key(" << key << ")index(" << index << ")";

            return i;
        }
    }

    LOG_TRACE << "ConfigFile::getKeyIndex:Key(" << key << ")index(Not Found)";

    return -1;
}

//-----------------------------------------------------------------------------

bool ConfigFile::findValue(const std::string & key, std::string & value) const
{
    int index = getKeyIndex(key);
    
    bool result = index >= 0;

    if (result) 
    {
        const std::array<std::string,2> & fields = contents.at(index);

        value = fields[1];

        LOG_TRACE << "ConfigFile::findValue:Key(" << key << ")index(" << index << ")Value(" << value << ")";
    }

    return result;
}

//-----------------------------------------------------------------------------

std::string ConfigFile::getValue( std::string key ) const throw (MissingKeyException)
{
    std::string value;

    bool found = findValue(key, value);

    if (! found)
    {
        std::string exceptionString = "Cannot find key " + key + " in config data";
        throw MissingKeyException(exceptionString);
    }

    return value;
}

//-----------------------------------------------------------------------------

bool ConfigFile::keyAllowed(const std::string & key)
{
    // do not allow duplicate keys

    int index = getKeyIndex(key);

    return index < 0;
}

//-----------------------------------------------------------------------------
