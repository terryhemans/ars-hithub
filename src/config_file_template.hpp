//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION:
//- ConfigFileTemplate Class.  Encapsulates the reading of a configuration file
//- consisting of key value pairs separated by a delimiter.  The class reads
//- the file, removes all whitespace and comments, and makes the data
//- available in the form of a vector of key value pairs.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef CONFIG_FILE_HPP
#define CONFIG_FILE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "common.hpp"
#include "hub_exception.hpp"

#include <string>
#include <vector>
#include <fstream>
#include <exception>
#include <cstring>
#include <array>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

///
/// Exception Declarations
///

// Thrown if the file cannot be read, e.g. it is missing or the user does not have permission
class FileReadException : public HubException {
   public: FileReadException( std::string reason ) : HubException( reason, false ){};
};

// Thrown if the configuration file is invalid.  The error message includes the line number
class InvalidConfigDataException : public HubException {
   public: InvalidConfigDataException( std::string reason ) : HubException( reason, false ){};
};


template <size_t NumberOfFields>
class ConfigFileTemplate
{

    public:

        ///
        /// Constructor
        ///
        /// Opens and reads the given file ignoring blank and comment lines and extracting pairs of string values
        /// from the file.
        ///
        /// @param filename - the path to the file to read
        /// @param delimiters - a string containing one or more characters
        ///
        /// Throws FileReadException if the file cannot be read.
        /// Throws InvalidConfigDataException if the file cannot be parsed
        ///
        ConfigFileTemplate( std::string filename, std::string delimiters ) throw (HubException)
        {
            Initialise( filename, delimiters );
        }

        ///
        /// Constructor
        ///
        /// This constructor allows for a subsequent call to Initialise
        ///
        ConfigFileTemplate(){};

        ///
        /// Initialise
        ///
        /// Opens and reads the given file ignoring blank and comment lines and extracting pairs of string values
        /// from the file.
        ///
        /// @param filename - the path to the file to read
        /// @param delimiters - a string containing one or more characters
        ///
        /// Throws FileReadException if the file cannot be read.
        /// Throws InvalidConfigDataException if the file cannot be parsed
        ///
        void Initialise(const std::string & filename, const std::string & delimiters) throw (HubException)
        {

            // Open and read the given filename
            std::ifstream file(filename);

            if(!file)
            {
                throw FileReadException( std::strerror(errno) );
            }

            try{
                StringList stringList;

                std::string rawLine;
                while( std::getline(file, rawLine) )
                {
                    stringList.push_back( rawLine );
                }
                
                Initialise(stringList, delimiters);
            }
            catch( const InvalidConfigDataException &e ){
                throw InvalidConfigDataException( "File " + filename + " " + e.what() );
            }
            catch( const std::exception &e ){
                throw FileReadException( e.what() );
            }
        }


        ///
        /// Initialise
        ///
        /// Opens and reads the given file ignoring blank and comment lines and extracting pairs of string values
        /// from the file.
        ///
        /// @param stringList - a list of strings, one per line
        /// @param delimiters - a string containing one or more characters
        ///
        /// Throws InvalidConfigDataException if the file cannot be parsed
        ///
        void Initialise(const StringList & stringList, const std::string & delimiters) throw (HubException)
        {
            contents.resize(0);
            
            for (size_t index = 0; index < stringList.size(); index++)
            {
                const std::string & rawLine = stringList[index];

                std::string line = rawLine.substr( 0, rawLine.find_first_of("#") );
                trim(line);
                if (line.length() > 0)
                {
                    // If the line is valid (contains a set of values separated by a delimiter) then add to the contents.
                    // Otherwise throw an exception
                    std::array<std::string, NumberOfFields> fields = split(line, delimiters);
                    if( fields[0] == "" )
                    {
                        throw InvalidConfigDataException( "Invalid content in line " + std::to_string((long long)index+1) + ": '" + rawLine + "'" );
                    }
                    if(! keyAllowed(fields[0]))
                    {
                        throw InvalidConfigDataException( "Duplicate key in line " + std::to_string((long long)index+1) + ": '" + rawLine + "'" );
                    }
                    contents.push_back( fields );
                }
            }
        }


        ///
        /// getData
        ///
        /// @returns the configuration data as a vector of key value pairs
        ///
        std::vector<std::array<std::string, NumberOfFields>> getData(){ return contents; }


    protected:

        /// list of rows read from the config file in the order in which they appear in the file
        std::vector<std::array<std::string, NumberOfFields>> contents;

        ///
        /// return true if key allowed
        ///
        virtual bool keyAllowed(const std::string & key)
        {
            return true;
        }

        ///
        /// Removes whitespace from before and after the given string
        ///
        void trim(std::string& s)
        {
            trimWhitespace(s);
        }

        ///
        /// Splits the given string in two at the first character in the string that matches one of the characters in the
        /// given delim string.
        ///
        /// @return a Pair object containing the left and right parts of the split string.  If the string could not be
        ///         split then a Pair object with empty left and right strings is returned.
        ///
        std::array<std::string, NumberOfFields> split( const std::string str, const std::string delim )
        {
            // initialise the return value
            std::array<std::string, NumberOfFields> fields;
            for( unsigned int i=0; i<NumberOfFields; i++ ){ fields[i] = ""; }
            unsigned int fieldNum = 0;
            // Traverse the string looking for delimiters
            for( unsigned int i=0; fieldNum<NumberOfFields && i<str.length(); i++ )
            {
                char c = str[i];
                bool found = false;
                for( unsigned int j=0; j<delim.length(); j++ )
                {
                    if( c == delim[j] ){
                        fieldNum++;
                        found = true;
                    }
                }
                if( !found && (fieldNum < NumberOfFields) ){ fields[fieldNum] += c; }
            }
            // set first field to empty string if line does not have the correct number of fields
            if( fieldNum != (NumberOfFields-1) ){ fields[0] = ""; }
            for( unsigned int i=0; i<NumberOfFields; i++ ){ trim(fields[i]); }
            return fields;
        }


};


//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------


#endif  // CONFIG_FILE_HPP
