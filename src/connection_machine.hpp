//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 531 2017-11-07 16:07:56Z jules $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules1/src/hub.hpp $
//-
//- DESCRIPTION:
//- Declaration of the ConnectionMachine Class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef CONNECTION_MACHINE_HPP
#define CONNECTION_MACHINE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <climits>

#include "period_detector.hpp"
#include "time.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

//-----------------------------------------------------------------------------

// Classes

//-----------------------------------------------------------------------------

class ConnectionMachine {
    private:

        enum States {
            cmsStartup,
            cmsDisconnected,
            cmsConnecting,
            cmsConnected,
            cmsDisconnecting
        };

    public:

        enum Transitions {
            cmtNoChange,
            cmtConnecting,
            cmtConnected,
            cmtDisconnecting,
            cmtDisconnected,
            cmtIntermittent,
            cmtReconnected
        };

        ConnectionMachine(long aConnectionPeriod, long aDisconnectionPeriod,
                         long aIntermittentPeriod, long aIntermittentLimit);

        ConnectionMachine::Transitions update(Time::HubTime nowSeconds, bool available);

    protected:
        // control
            long connectionPeriod;
            long disconnectionPeriod;
            long intermittentPeriod;
            long intermittentLimit;

        // status
            States state;

            long connectionCount;
            long intermittentCount;

            PeriodDetector connectionPeriodDetector;
            PeriodDetector disconnectionPeriodDetector;
            PeriodDetector intermittentPeriodDetector;
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif // CONNECTION_MACHINE_HPP
