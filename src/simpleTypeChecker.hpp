#pragma once
#include "xmlParser/rapidxml.hpp"
#include "hithub_types.hpp"

class SimpleTypeChecker
{
	public:
		rapidxml::xml_node<>* getChildNodeDefinition(rapidxml::xml_node<>* node, std::string search);
		StringList getArrayTypes(rapidxml::xml_node<>* definition);
		bool shouldNodeValueBeString(rapidxml::xml_node<>* definition);
		bool shouldAttrValueBeString(rapidxml::xml_node<>* definition, std::string attrName);
		
	private:
		void removeNamespace(std::string& attrName);
		
};