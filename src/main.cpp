//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: main.cpp 2500 2018-05-17 15:25:15Z jules $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/main.cpp $
//-
//- DESCRIPTION:
//- Main entry point of the Hitachi Hub application.  Constructs and runs the
//- application and handles an exit signal from the OS.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <signal.h>
#include <stdio.h>
#include <exception>
#include <iostream>

#include "hub.hpp"
#include "dmr_linx.hpp"
#include "hub_exception.hpp"
#include "main_parameters.hpp"
#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

static Hub* hub;

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

///
/// exitApplication
///
/// Handles an exit signal from the OS
///
/// @post   All application objects are deleted and the application exits with the given signal number.
///
void exitApplication(int signum)
{
   delete hub;

   std::cout << std::endl;

   exit(signum);
}

///
/// main
///
/// The Hitachi Hub Application entry point.  Creates and runs the Hub object and captures any uncaught exceptions.
///
/// @post Does not return.
///
int main(int argc, char* argv[])
{
    // register the OS signal handler first
    signal(SIGINT,  exitApplication);
    signal(SIGTERM, exitApplication);

    Seconds testDuration = 0;
    long testIterations = handleParameters(argc, argv, testDuration);

    try
    {
        DmrLinx linx;
        hub = new Hub(&linx);
        if(hub) hub->run(testIterations, testDuration);
        std::cout << std::endl;
    }
    catch( HubException &e )
    {
        setLogStatus("Failed");
        LOG_ERROR << "<status>" "\t" "Hithub Failed: " << e.what();
        std::cerr << formatLogLine(fatal) << e.what() << std::endl;
    }
    catch( std::exception &e )
    {
        setLogStatus("Failed");
        LOG_ERROR << "<status>" "\t" "Hithub Failed:" << e.what();
        std::cerr << formatLogLine(fatal) << "uncaught exception:" << e.what() << std::endl;
    }

    return 0;
}
