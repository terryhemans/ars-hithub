//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 531 2017-11-07 16:07:56Z jules $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules1/src/dmr_machine.hpp $
//-
//- DESCRIPTION:
//- Declaration of the DmrMachine Class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef DMR_MACHINE_HPP
#define DMR_MACHINE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <climits>
#include <string>

#include "period_detector.hpp"
#include "time.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

//-----------------------------------------------------------------------------

// Classes

//-----------------------------------------------------------------------------

class DmrMachine {
    public:

        enum Selected {
            Primary,
            Secondary
        };

        DmrMachine(std::string aName, Seconds aPrimaryCheckPeriod);

        ///
        /// reset the DMR state machine
        ///
        void dmrReset();

        ///
        /// update the DMR state machine and return overall availability
        ///
        bool dmrUpdate(Time::HubTime nowSeconds, bool primaryAvailable, bool secondaryAvailable);

    protected:
        enum States {
            dmsConnectToPrimary,
            dmsPrimaryConnected,
            dmsConnectToSecondary,
            dmsSecondaryConnected,
            dmsPrimaryRetry
        };

        // control
            std::string name;
            Seconds primaryCheckPeriod;

        // status
            States state;

            PeriodDetector primaryCheckPeriodDetector;

        // methods
            ///
            /// invoked by the DMR machine to connect selected links
            ///
            /// returns whether link is established
            ///
            virtual bool dmrConnect(Selected selected) { return false; }

            ///
            /// invoked by the DMR machine to disconnect selected link
            ///
            virtual void dmrDisconnect(Selected selected) {}

            Selected getSelected() const { return selected; }

    private: protected:
        Selected selected;

        bool overallAvailability;
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif // DMR_MACHINE_HPP
