#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include "xmlConverter.hpp"
#include "xmlParser/rapidxml.hpp"
#include "operational_log.hpp"
#include "common.hpp"
#include "hithub_types.hpp"
#include "config_data.hpp"
#include "config_file.hpp"

XMLConverter::XMLConverter()
{
	LOG_TRACE << "Constructing the XML Converter Class";
	CONTENT_PREFIX = "\"content\":";
	ATTR_PREFIX = "@";
	
	// Read the message schemas from config File.
	ConfigFile messageSchemaConfigFile(configData.messageSchemasFilename(), "-");
	
	std::vector<std::array<std::string, 2>> configfileContents = messageSchemaConfigFile.getData();
	
	for(uint i = 0; i < configfileContents.size(); i++)
	{
		messageDefinitionsMap[configfileContents[i][0]] = configfileContents[i][1];
	}
	
}

std::string XMLConverter::convertXMLtoJSON(std::string & rawXML)
{
	if (rawXML != "")
	{
		rapidxml::xml_document<> doc;
		doc.parse<rapidxml::parse_no_entity_translation>(&rawXML[0]);
		rapidxml::xml_node<> *root_node = doc.first_node();
		std::string messageType = root_node->name();
		
		std::string definitionString = messageDefinitionsMap[messageType];
		if(definitionString == "")
		{
			LOG_TRACE << "Error finding element definition";
		}
		rapidxml::xml_document<> defDoc;
		defDoc.parse<0>(&definitionString[0]);
		rapidxml::xml_node<> *definitionNode = defDoc.first_node();
		
		std::string JSON = "{" + convertXML(root_node, definitionNode) + "}";
		replaceAllSpecialCharacters(JSON);
		return JSON;
	}
	else
	{
		LOG_TRACE << "Error!. Supplied XML string is empty";
		return "";
	}
}

std::string XMLConverter::convertXML(rapidxml::xml_node<> *rootNode, rapidxml::xml_node<> *definition)
{
	std::string nameString = "";
	std::string attrString = "";
	std::string childString = "";
	std::string valueString = "";
	std::string nodeString = "";
	
	StringList attrVector;
	StringList childVector;
	
	if(rootNode->name_size() > 0)
	{
		nameString = rootNode->name();
		for(rapidxml::xml_attribute<> *attr = rootNode->first_attribute(); attr; attr = attr->next_attribute())
		{
			std::string attrName = attr->name();
			
			if( (attrName.substr(0, 5) != "xmlns") && (attrName.substr(0, 3) != "xsi") )
			{
				if(typeChecker.shouldAttrValueBeString(definition, attrName))
				{
					attrVector.push_back("\"" + ATTR_PREFIX + attrName + "\":\"" + attr->value() + "\"");
				}
				else
				{
					attrVector.push_back("\"" + ATTR_PREFIX + attrName + "\":" + attr->value());
				}
			}
		}

		buildAttrString(attrVector, attrString);
		
		for(rapidxml::xml_node<> *childNode = rootNode->first_node(); childNode; childNode = childNode->next_sibling())
		{
			if (childNode->name_size() >0)
			{
				rapidxml::xml_node<>* childDefinition = typeChecker.getChildNodeDefinition(definition, childNode->name());
				std::string tempChildString = convertXML(childNode, childDefinition);
				
				if (tempChildString.length() > 0)
				{
					childVector.push_back(tempChildString);
				}
			}
		}
		
		buildChildString(childVector, nameString, childString, definition);
		
		if (rootNode->value_size() > 0)
		{
			if(typeChecker.shouldNodeValueBeString(definition))
			{
				valueString = (std::string)"\"" + rootNode->value() + "\"";
			}
			else
			{
				valueString = rootNode->value();
			}
		}

		buildJsonString(attrString, nameString, childString, valueString, nodeString);
	}
	
	return nodeString;
}

void XMLConverter::buildJsonString(const std::string &attrString,
									const std::string &nameString,
									const std::string &childString,
									const std::string &valueString,
									std::string &nodeString)
{
	if(attrString == "" && childString == "" && valueString !="") //0,0,1
	{
		nodeString = "\"" + nameString + "\":" + valueString;
	}
	else if (attrString == "" && childString != "" && valueString == "") //0,1,0
	{
		nodeString = "\"" + nameString + "\":{" + childString + "}";
	}
	else if (attrString == "" && childString != "" && valueString != "") //0,1,1
	{
		nodeString = "\"" + nameString + "\":{" + childString + "," + CONTENT_PREFIX + valueString + "}";
	}
	else if (attrString != "" && childString == "" && valueString == "") //1,0,0
	{
		nodeString = "\"" + nameString + "\":{" + attrString + "}";
	}
	else if (attrString != "" && childString == "" && valueString != "") //1,0,1
	{
		nodeString = "\"" + nameString + "\":{" + attrString + "," + CONTENT_PREFIX + valueString + "}";
	}
	else if (attrString != "" && childString != "" && valueString == "") //1,1,0
	{
		nodeString = "\"" + nameString + "\":{" + attrString + "," + childString + "}";
	}
	else if (attrString != "" && childString != "" && valueString != "") //1,1,1
	{
		nodeString = "\"" + nameString + "\":{" + attrString + "," + childString + "," + CONTENT_PREFIX + valueString + "}";
	}
	else
	{
		nodeString = "\"" + nameString + "\":null";
	}
	//*
	if( (nodeString.length() > 1) && (nodeString[nodeString.length() - 2] == ',') )
	{
		LOG_TRACE << "Errant comma detected";
		nodeString.erase(nodeString.length() - 2, 1);
	}
	//*/
}

void XMLConverter::buildAttrString(const StringList &attrs, std::string &resultString)
{
	for(uint i = 0; i < attrs.size(); i++)
	{
		resultString = resultString + attrs[i];
		if(i != attrs.size() - 1)
		{
			resultString += ",";
		}
	}
}

void XMLConverter::buildChildString(const StringList &childNodes, const std::string & parentName, std::string &resultString, rapidxml::xml_node<>* definition)
{
	if (childNodes.size() != 0)
	{		
		std::map<std::string, std::string> childStringComponents;
		StringList arrayTypes = typeChecker.getArrayTypes(definition);

		for(uint i = 0; i < childNodes.size(); i++)
		{
			std::string name = childNodes[i].substr(1, childNodes[i].find("\":") - 1);
			std::string value = childNodes[i].substr(childNodes[i].find("\":") + 2);
			
			if(isValueInVector(arrayTypes, name))// current childNode is an array type
			{
				if (getValueFromMap(childStringComponents, name) == "") //Have NOT already processed a variable of this type
				{
					std::string tempString = "[" + value + "]";
					childStringComponents[name] = tempString;
				}
				else //Have already processed a variable of this type
				{
					childStringComponents[name].erase(childStringComponents[name].length() - 1);
					std::string temp = "," + value + "]";
					childStringComponents[name] += temp; 
				}
			}
			else //current ChildNode is not an array and can be added to map.
			{
				childStringComponents[name] = value;
			}
		}

		for (std::map<std::string, std::string>::iterator it = childStringComponents.begin(); it != childStringComponents.end(); it++ )
		{		
			resultString += ("\"" + it->first + "\":" + it->second);
			if(std::next(it, 1) != childStringComponents.end())
			{
				resultString += ",";
			}	
		}
	}
}

void XMLConverter::replaceAllSpecialCharacters(std::string & nodeString)
{
	/*
	Conversion rules are:
	&lt; = <
	&gt; = >
	&amp; = &
	&apos; = ‘
	&quot; = \”
	‘…“…’ = “…\”…”
	\ = \\
	tab = \t
	line feed = \n
	*/
	
	replaceSubStrings(nodeString, "&lt;", "<");
	replaceSubStrings(nodeString, "&gt;", ">");
	replaceSubStrings(nodeString, "&amp;", "&");
	replaceSubStrings(nodeString, "&apos;", "'");
	replaceSubStrings(nodeString, "\\", "\\\\");
	replaceSubStrings(nodeString, "&quot;", "\\\"");
	replaceSubStrings(nodeString, " tab ", "\\t"); //Leading and trailing space to avoid cases like "timetable" = "time	le"
	replaceSubStrings(nodeString, "line feed", "\\n");
	
}


