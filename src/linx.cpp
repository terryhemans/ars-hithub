//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.    All rights reserved.
//-
//- $Id: linx.cpp 3101 2018-09-04 15:35:59Z brian $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/linx.cpp $
//-
//- DESCRIPTION:
//- Implementation of the Linx class.
//-
//- This specification conforms to the IBM Websphere MQ version 8.0.0
//-   - Website
//-         https://www.ibm.com/support/knowledgecenter/SSFKSJ_8.0.0/com.ibm.mq.pro.doc/q001010_.htm
//-   - Completion and reason codes:
//-         https://www.ibm.com/support/knowledgecenter/SSFKSJ_8.0.0/com.ibm.mq.tro.doc/q040700_.htm
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <cstring> // strlen

#include "linx.hpp"
#include "operational_log.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

static const int maxMessageLength = 1024 * 1024;

// Enumerations

// Structures

// Variables


//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

Linx::Linx() : LinxInterface(),
    mgr(NULL),
    channel(NULL),
    reportedError(false)
{
    for (unsigned u = 0; u < FlowIdCount; u++) {
        queues[u] = 0;
    }

    LOG_TRACE << "Constructing the Linx class";
}


//-----------------------------------------------------------------------------

void Linx::initialiseConnection(const ConnectionDetails connection )
{
    LOG_TRACE << "Initiate Linx connection";

    myConnectionDetails = connection;

    // setup and configure the Queue ready for connection
    for (unsigned u = 0; u < FlowIdCount; u++) {
        FlowId flowId = static_cast<FlowId>(u);
        if (connection.queueNames[u].length() == 0)
        {
            LOG_TRACE << "Linx::initialiseConnection queue " << FlowName(flowId) << " has no queue name";
        }
        else
        {
            queues[u] = new ImqQueue();
            queues[u]->setName(connection.queueNames[u].c_str());
        }
    }

    // setup and configure the Channel
    channel = new ImqChannel();
    channel->setHeartBeatInterval(1);
    channel->setTransportType(MQXPT_TCP);
    channel->setChannelName(connection.channelName.c_str());
    channel->setConnectionName(connection.connectionName.c_str());

    // setup and configure the Manager
    mgr = new ImqQueueManager();
    mgr->setName(connection.managerName.c_str());
    mgr->setChannelReference(channel);
    mgr->setAuthenticationType(MQCSP_AUTH_USER_ID_AND_PWD);
    mgr->setUserId(connection.username.c_str());
    mgr->setPassword(connection.password.c_str());

    // connect to the remote manager and open the queue
    // now handled by DMR : connect();
}

//-----------------------------------------------------------------------------

void Linx::connect()
{
    if (mgr) {
        for (unsigned u = 0; u < FlowIdCount; u++) {
            FlowId flowId = static_cast<FlowId>(u);
            ImqQueue * queue = queues[u];
            if (queue) {
                // Attempt to connect to the remote queue manager
                ImqBoolean result = mgr->connect();

                if (!result)
                {
                    if (! reportedError) {
                        int reasonCode = mgr->reasonCode();
                        LOG_ERROR << "<interface><linx>" "\t" "LINX MQ connect attempt failed (reason code " << reasonCode << ")";
                        reportedError = true;
                    }
                }
                else
                {
                    // Associate queue with queue manager.
                    queue->setConnectionReference(mgr);

                    // Open the named message queue
                    bool isInput = FlowIsLinxToTranista(flowId);

                    queue->setOpenOptions( isInput ? (MQOO_INPUT_SHARED + MQOO_INQUIRE) : MQOO_OUTPUT );
                    queue->open();

                    // report reason, if any
                    const std::string & queueName = myConnectionDetails.queueNames[u];
                    if (queue->reasonCode())
                    {
                        LOG_ERROR << "<interface><linx>" "\t" "failed to open LINX MQ (" << queueName <<
                                ") (reason code " << static_cast<long>(queue->reasonCode()) << ")";
                    }
                    else if (queue->completionCode() == MQCC_FAILED)
                    {
                        LOG_ERROR << "<interface><linx>" "\t" "failed to open LINX MQ (" << queueName <<
                                ") (response = MQCC_FAILED)";
                    }
                    else
                    {
                        LOG_INFO << "<interface><linx>" "\t" "LINX MQ (" << queueName << ") connected";
                    }
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------

Linx::~Linx()
{

    LOG_TRACE << "<interface><linx>" "\t" "Closing LINX MQ connection";

    // Close the source queue (if it was opened)
    for (unsigned u = 0; u < FlowIdCount; u++) {
        ImqQueue * queue = queues[u];
        if (queue && !queue->close())
        {
            LOG_ERROR << "<interface><linx>" "\t" "Failed to close IMQ cleanly (reason code "
                << static_cast<long>(queue->reasonCode()) << ")";
        }
    }

    disconnect();

    // Tidy up the channel object if allocated.
    if (channel)
    {
        if (mgr) mgr->setChannelReference();
        delete channel;
    }

    // Destruction of the queue manager will ensure that it is disconnected.
    // If the queue object were still available and open (which it is not),
    // then the queue would be closed prior to disconnection.
    if (mgr) delete mgr;

    // Tidy up the queue object if allocated.
    for (unsigned u = 0; u < FlowIdCount; u++) {
        ImqQueue * queue = queues[u];
        if (queue)
        {
            delete queue;
        }
    }
}

//-----------------------------------------------------------------------------

bool Linx::isAvailable()
{
    ImqBoolean connected = false;

    if (mgr) {
        connected = mgr->connectionStatus();
    }

    return connected;
}

//-----------------------------------------------------------------------------

bool Linx::removeMessage(FlowId flowId, std::string & message)
{
    bool result = false;

    ImqQueue * queue = queues[flowId];

    if (queue) {
        const std::string & flowName = FlowName(flowId);

        ImqMessage imqMsg;

        // get the next message in an empty buffer, up to Max_Message_Length bytes
        char buffer[maxMessageLength+1];
        imqMsg.useEmptyBuffer(buffer, maxMessageLength);
        queue->get(imqMsg);

        // Handle the result depending on the completion code
        switch ( queue->completionCode() )
        {

            // a message is received so convert it to a string and return it
            case MQCC_OK:
                buffer[imqMsg.dataLength()] = '\0';  /* add null terminator */
                message = imqMsg.bufferPointer();
                result = true;
                break;

            // failed to read a message off the queue
            case MQCC_WARNING:
                LOG_WARNING << "<interface><linx><" << flowName << ">" "\t"
                    "reading " << flowName << " message failed with warning code " << imqMsg.reasonCode();
                break;

            // failed to read a message off the queue or the queue is empty
            case MQCC_FAILED:
                if( imqMsg.reasonCode() == MQRC_NONE )
                {
                    // queue is empty so do nothing and return an empty string
                }
                else
                {
                    LOG_ERROR << "<interface><linx><" << flowName << ">" "\t"
                        "reading " << flowName << " message failed with error code " << imqMsg.reasonCode();
                }
                break;

            // unexpected result so log an error
            default:
                LOG_ERROR << "<interface><linx><" << flowName << ">" "\t"
                    "reading " << flowName << " message failed with unknown completion code " <<
                    imqMsg.completionCode() << " (reason code " << imqMsg.reasonCode() << ")";
        }
    }

    return result;
}

//-----------------------------------------------------------------------------

void Linx::tick1s()
{
    reportedError = false;
}

//-----------------------------------------------------------------------------

void Linx::disconnect()
{
    // Disconnect from MQM if not already connected (the ImqQueueManager object handles this situation automatically)
    if (mgr && !mgr->disconnect())
    {
        LOG_ERROR << "<interface><linx>" "\t" "Failed to disconnect from the IMQ (reason code "
                  << static_cast<long>(mgr->reasonCode()) << ")";
    }
}

//-----------------------------------------------------------------------------

bool Linx::appendMessage(FlowId flowId, const char * chars)
{
    bool result = false;

    ImqQueue * queue = queues[flowId];

    if (queue) {
        const std::string & flowName = FlowName(flowId);

        // R-534
        LOG_INFO << "<interface><linx><" << flowName << "><tx>" "\t" "message: " << chars;
        ImqMessage imqMessage;

        imqMessage.useFullBuffer(chars, strlen(chars));
        imqMessage.setFormat(MQFMT_STRING);

        if (queue->put(imqMessage)) {
            result = true;

        }
        else
        {
            // Handle the result depending on the completion code
            switch (queue->completionCode())
            {
            case MQCC_OK:
                result = true;
                break;

            case MQCC_WARNING:
                LOG_WARNING << "<interface><linx><tx>" "\t"
                    "appending output message to queue " << flowName <<
                    " failed with warning code " << imqMessage.reasonCode();
                break;

            case MQCC_FAILED:
                LOG_ERROR << "<interface><linx><tx>" "\t"
                    "appending output message to queue " << flowName <<
                    " failed with error code " << imqMessage.reasonCode();
                break;

                // unexpected result so log an error
            default:
                LOG_ERROR << "<interface><linx><tx>" "\t"
                    "appending output message to queue " << flowName <<
                    " failed with unknown completion code " <<
                    imqMessage.completionCode() <<
                    " (reason code " << imqMessage.reasonCode() << ")";
            }
        }
    }

    return result;
}

//-----------------------------------------------------------------------------
