#include <iostream>
#include "xmlParser/rapidxml.hpp"
#include "xmlParser/rapidxml_print.hpp"
#include "simpleTypeChecker.hpp"
#include "operational_log.hpp"
#include "common.hpp"

rapidxml::xml_node<>* SimpleTypeChecker::getChildNodeDefinition(rapidxml::xml_node<>* node, std::string search)
{
	if(node)
	{
		for(rapidxml::xml_node<>* childNode = node->first_node(); childNode; childNode = childNode->next_sibling())
		{
			if(childNode->first_attribute("name"))
			{
				if (childNode->first_attribute("name")->value() == search)
				{
					return childNode;
				}
			}
		}
		LOG_ERROR << "Error! " + search + " Child Node not found in XML Definition for " + node->first_attribute("name")->value();
	}
	return 0;
}

StringList SimpleTypeChecker::getArrayTypes(rapidxml::xml_node<>* definition)
{
	StringList result;
	if(definition)
	{
		for(rapidxml::xml_node<>* childNode = definition->first_node(); childNode; childNode = childNode->next_sibling())
		{
			if(childNode->first_attribute("maxOccurs")) //If maxOccurs is present, can safely assume this is an array type
			{
				result.push_back(childNode->first_attribute("name")->value());
			}
		}	
	}
	return result;
}

bool SimpleTypeChecker::shouldNodeValueBeString(rapidxml::xml_node<>* definition)
{
	if(definition)
	{
		rapidxml::xml_attribute<>* typeAttr = definition->first_attribute("type");
		if(typeAttr)
		{
			std::string elementType = typeAttr->value();
			return (!(elementType == "xs:integer" ||
				elementType == "xs:int" ||
				elementType == "xs:float" ||
				elementType == "xs:decimal" ||
				elementType == "xs:short" ||
				elementType == "xs:long" ||
				elementType == "xs:positiveInteger" ||
				elementType == "xs:boolean"));
		}
	}
	return true;
}

bool SimpleTypeChecker::shouldAttrValueBeString(rapidxml::xml_node<>* definition, std::string attrName)
{
	removeNamespace(attrName);
	
	if(definition)
	{
		for(rapidxml::xml_node<>* attrNode = definition->first_node("xs:attribute"); attrNode; attrNode = attrNode->next_sibling("xs:attribute"))
		{
			rapidxml::xml_attribute<>* nameAttr = attrNode->first_attribute("name");
			if(nameAttr && nameAttr->value() == attrName)
			{
				return shouldNodeValueBeString(attrNode);
			}
		}
	}
	return true;
}

void SimpleTypeChecker::removeNamespace(std::string& attrName)
{
	StringList separatedStrings;
	separateStrings(attrName, separatedStrings, ":");
	if (separatedStrings.size() > 2)
	{
		// This would mean there were 2 or more colons in the attrName.
		LOG_ERROR << "Error parsing attribute name for namespace";
	}
	else
	{
		attrName = separatedStrings[separatedStrings.size() - 1];
	}
}