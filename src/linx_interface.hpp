//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- Copyright (c) 2017 Hitachi Limited. All rights reserved.
//-
//- $Id: linx_interface.hpp 117 2017-05-02 13:20:01Z mike $
//-
//- $URL: svn+ssh://77.68.12.143/var/svn/repos/hithub/branches/TestHarness/src/linx_interface.hpp $
//-
//- DESCRIPTION: Declares the Hitachi Hub Linx Interface abstract class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef LINX_INTERFACE_HPP
#define LINX_INTERFACE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>

#include "flows.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class LinxInterface
{

   public:

      struct ConnectionDetails {
         std::string connectionName;
         std::string secondaryConnectionName;
         std::string managerName;
         std::string channelName;
         std::string queueNames[FlowIdCount];
         std::string username;
         std::string password;
      };

      /// Connects the Websphere MQ.
      ///
      /// @param[in] connection The LINX queue connection details.

      virtual void initialiseConnection(const ConnectionDetails connection) = 0;

      /// Checks whether the Linx interface is available.
      ///
      /// @return true = available, false = not available.

      virtual bool isAvailable() = 0;

      /// Gets the next message from the given queue.
      ///
      /// @return whether the message is returned and the string containing the message.
      ///
      virtual bool removeMessage(FlowId queueId, std::string & message) = 0;

      ///
      /// appendMessage
      ///
      /// Appends the message to the output queue.
      ///
      /// @return false if message not appended.
      ///
      virtual bool appendMessage(FlowId queueId, const char * chars) = 0;

      /// Periodic stimulation for the Linx interface.
      ///
      virtual void tick1s() = 0;

      /// Default destructor
      ///
      virtual ~LinxInterface() {}

   protected: public:
      ConnectionDetails myConnectionDetails;

   protected:
      /// Default constructor
      ///
      LinxInterface() {}

};

#endif  // LINX_INTERFACE_HPP
