//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION:
//- Configdata.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef CONFIG_DATA_HPP
#define CONFIG_DATA_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class ConfigData
{
public:
    ConfigData();

    const std::string & hithubFilename() { return m_hithubFilename; }
    void hithubFilenameSet(const std::string & a_hithubFilename) { m_hithubFilename = a_hithubFilename; }
	
	const std::string & messageSchemasFilename() { return m_messageSchemasFilename; }
    void messageSchemasFilenameSet(const std::string & a_messageSchemasFilename) { m_messageSchemasFilename = a_messageSchemasFilename; }

protected:
    std::string m_hithubFilename;
	std::string m_messageSchemasFilename;
};

// these C++ artefacts provided to assist testing

extern ConfigData theConfigData;

extern ConfigData & configData;

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif  // CONFIG_DATA_HPP
