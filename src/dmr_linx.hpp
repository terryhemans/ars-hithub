//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: linx.hpp 1798 2018-03-13 17:22:35Z david $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules2/src/dmr_linx.hpp $
//-
//- DESCRIPTION:
//- DMR Linx Class.
//- Interface to the Linx incorporating DMR.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef DMR_LINX_HPP
#define DMR_LINX_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "hithub_types.hpp"

#include "dmr_machine.hpp"
#include "linx.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class DmrLinx : public LinxInterface, public DmrMachine
{

    public:

        ///
        /// Constructor
        ///
        /// Constructs the DMR Linx, comprising a primary Linx and a secondary Linx.
        ///
        /// @post the 2 Linxes are created.
        ///
        DmrLinx();


        ///
        /// Destructor
        ///
        /// Closes the DMR Linx
        ///
        /// @post the 2 Linxes are destroyed
        ///
        ~DmrLinx();


        ///
        /// Initialise Connection
        ///
        /// Initialises the DMR Linxes
        ///
        void initialiseConnection(const LinxInterface::ConnectionDetails connection); // LinxInterface


        ///
        /// isAvailable()
        ///
        /// This method is the one that selects between the primary and secondary link.
        /// Thus it should be invoked frequently.
        ///
        /// @return true if at least 1 Linx is connected.  False otherwise.
        ///
        bool isAvailable(); // LinxInterface


        ///
        /// removeMessage
        ///
        /// Gets the next message from the given queue.
        ///
        /// @return whether the message is returned and the string containing the message.
        ///
        bool removeMessage(FlowId flowId, std::string & message); // LinxInterface

        ///
        /// appendMessage
        ///
        /// Appends the message to the output queue.
        ///
        /// @return false if message not appended.
        ///
        bool appendMessage(FlowId flowId, const char * chars); // LinxInterface

        ///
        /// tick1s()
        ///
        /// Handles the DMR availability of the 2 Linxes
        ///
        void tick1s(); // LinxInterface


    protected:

        /// The Linxes
        Linx linxes[2];

        ///
        /// invoked by the DMR machine to connect and disconnect selected Linx
        ///
        /// returns true if connected
        ///
        bool dmrConnect(Selected selected); // DmrMachine

        void dmrDisconnect(Selected selected); // DmrMachine

    private:

};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif  // DMR_LINX_HPP
