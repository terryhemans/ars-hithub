#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include "../xmlParser/rapidxml.hpp"
#include "../xmlParser/rapidxml_print.hpp"
#include "xmlDefinitionGenerator.hpp"

using namespace rapidxml;

/*
Definitions used in this class:

Node type = Refers to the data type. Can be either native (e.g. xs:integer or xs:string) or derived (e.g. FreeText)
Element Node Version = Element nodes can be either reference, type or name.
					   Reference = Element node has a ref attribute. Element is defined elsewhere in XSDs.
					   Type = Element has a type attribute and likely a name attribute as well. Element is of a type defined elsewhere in XSD.
					   Name = Element has a name attribute only. This is the definition of that element.


*/

std::string XMLDefinitionGenerator::getRootNodeDefinition(std::string searchTerm, std::string filePath)
{
	std::vector<std::string> xsdStrings;
	
	getIncludedFiles(xsdStrings, filePath);
	
	const int noOfDocs = xsdStrings.size();
	
	xml_document<> xsdNodes[noOfDocs];
	
	for (uint i = 0; i < xsdStrings.size(); i++)
	{
		xsdNodes[i].parse<0>(&xsdStrings[i][0]);
		rootXSDNodes.push_back(xsdNodes[i].first_node());
	}

	xml_document<> elementDefinition;
	xml_node<>* definitionNode = getElementNode(elementDefinition, searchTerm);
	if(definitionNode)
	{
		elementDefinition.append_node(definitionNode);
		std::string result;
		print(std::back_inserter(result), elementDefinition, 1);
		return result;
	}
	return "";
}

std::string XMLDefinitionGenerator::readFile(std::string filePath)
{
	std::ifstream input;
	input.open( filePath );
	std::ostringstream oss;
    oss << input.rdbuf();
	std::string xsdString = oss.str().data();
	input.close();
	
	return xsdString;
}

void XMLDefinitionGenerator::getIncludedFiles(std::vector<std::string>& xsdStrings, std::string filePath)
{
	std::string fileContents = readFile(filePath);
	xsdStrings.push_back(fileContents);
	
	xml_document<> doc;
	doc.parse<0>(&fileContents[0]);
	
	xml_node<>* rootNode = doc.first_node();
	for (xml_node<>* childNode = rootNode->first_node("xs:include"); childNode; childNode = childNode->next_sibling("xs:include"))
	{
		xml_attribute<>* locationAttr = childNode->first_attribute("schemaLocation");
		if(locationAttr)
		{
			std::string dirPath = "";
			int delimiterLoc = filePath.find_last_of("/");
			if(delimiterLoc >= 0)
			{
				dirPath = filePath.substr(0, filePath.find_last_of("/") + 1);
			}
			std::string includeFilePath = dirPath + locationAttr->value();
			getIncludedFiles(xsdStrings, includeFilePath);
		}
	}
}

xml_node<>* XMLDefinitionGenerator::getElementNode(xml_document<>& elementDefinition, const std::string & searchTerm)
{
	bool found;
	xml_node<>* childNode;
	for(uint i = 0; !found && i< rootXSDNodes.size(); i++)
	{
		for(childNode = rootXSDNodes[i]->first_node("xs:element"); !found && childNode; childNode = childNode->next_sibling("xs:element"))
		{
			xml_attribute<>* nameAttr = childNode->first_attribute("name");
			if(nameAttr && nameAttr->value() == searchTerm)
			{
				found = true;
				break;
			}
		}
	}
	if(found)
	{
		return parseElementNode(elementDefinition, childNode);
	}
	else
	{
		std::cout << "Error. Element Node not found. Search Term: " << searchTerm << std::endl;
		return 0;
	}
}

xml_node<>* XMLDefinitionGenerator::parseElementNode(xml_document<>& elementDefinition, xml_node<>* elementNode)
{
	//std::cout << "In parseElementNode" << std::endl;
	//ref - If it contains a ref attribute then the element is defined elsewhere in the XSDs.
	//type - If it contains a type attribute then the element is of a type (complex or simple) defined elsewhere in the XSDs.
	//name - If it contains a name attribute and only a name attribute then this node is the definition.
	
	xml_attribute<>* refAttr = elementNode->first_attribute("ref");
	xml_attribute<>* typeAttr = elementNode->first_attribute("type");
	xml_attribute<>* nameAttr = elementNode->first_attribute("name");
	xml_attribute<>* maxOccursAttr = elementNode->first_attribute("maxOccurs");
	
	//std::cout << "refAttr: " << refAttr << ", typeAttr: " << typeAttr << ", nameAttr: " << nameAttr << std::endl;
	
	if(refAttr && !typeAttr && !nameAttr) //ref
	{
		xml_node<>* newNode = getElementNode(elementDefinition, refAttr->value());
		if(maxOccursAttr)
		{
			addAttribute(elementDefinition, newNode, "maxOccurs", maxOccursAttr->value());
		}
		return newNode;
	}
	else if (!refAttr && typeAttr && nameAttr) //type
	{
		xml_node<>* newNode =  parseTypeElementNode(elementDefinition, nameAttr->value(), typeAttr->value());
		if(maxOccursAttr)
		{
			addAttribute(elementDefinition, newNode, "maxOccurs", maxOccursAttr->value());
		}
		return newNode;
	}
	else if (!refAttr && !typeAttr && nameAttr) //name
	{
		xml_node<>* newNode = parseNameElementNode(elementDefinition, elementNode, nameAttr->value());
		if(maxOccursAttr)
		{
			addAttribute(elementDefinition, newNode, "maxOccurs", maxOccursAttr->value());
		}
		return newNode;
	}
	else
	{
		std::cout << "Error parsing Element Node. Unrecognised Node version" << std::endl;
		std::cout << "\tnameAttr: " << nameAttr << ", typeAttr: " << typeAttr << ", refAttr: " << refAttr << std::endl;
	}
	return 0;
}

xml_node<>* XMLDefinitionGenerator::parseNameElementNode(xml_document<>& elementDefinition, xml_node<>* nameElementNode, const char* nameAttrValue)
{
	//std::cout << nameAttrValue << " is of a name version" << std::endl;
	xml_node<>* typeChildNode = nameElementNode->first_node("xs:simpleType");
	if(!typeChildNode)
	{
		typeChildNode = nameElementNode->first_node("xs:complexType");
		if (typeChildNode)
		{
			return parseComplexTypeNode(elementDefinition, typeChildNode, nameAttrValue);
		}
	}
	else
	{
		xml_node<>* newNode = createNode(elementDefinition, "xs:element");
		addAttribute(elementDefinition, newNode, "name", nameAttrValue);
		addAttribute(elementDefinition, newNode, "type", parseSimpleTypeNode(typeChildNode));
		return newNode;
	}
	std::cout << "Error Parsing Name Element Node: " << nameAttrValue << std::endl;
	return 0;
}

xml_node<>* XMLDefinitionGenerator::parseTypeElementNode(xml_document<>& elementDefinition, const char* nameAttrValue, const char* typeAttrValue)
{
	//std::cout << nameAttrValue << " is of type version" << std::endl;
	if(isNativeType(typeAttrValue))
	{
		xml_node<>* newNode = createNode(elementDefinition, "xs:element");
		addAttribute(elementDefinition, newNode, "name", nameAttrValue);
		addAttribute(elementDefinition, newNode, "type", typeAttrValue);
		return newNode;
	}
	else
	{
		xml_node<>* typeChildNode = getSimpleTypeNode(typeAttrValue);
		if(!typeChildNode)
		{
			typeChildNode = getComplexTypeNode(typeAttrValue);
		}
		else
		{
			xml_node<>* newNode = createNode(elementDefinition, "xs:element");
			addAttribute(elementDefinition, newNode, "name", nameAttrValue);
			addAttribute(elementDefinition, newNode, "type", parseSimpleTypeNode(typeChildNode));
			return newNode;
		}
		if(!typeChildNode) //ComplexType
		{
			std::cout << "Error finding type Node with name: " << typeAttrValue << std::endl;
		}
		else
		{
			return parseComplexTypeNode(elementDefinition, typeChildNode, nameAttrValue);
		}
	}
	std::cout << "Error Parsing Type Element Node: " << nameAttrValue << std::endl;
	return 0;
}

std::string XMLDefinitionGenerator::parseSimpleTypeNode(xml_node<>* simpleTypeNode)
{
	//std::cout << "In parseSimpleTypeNode" << std::endl;
	std::string baseType = simpleTypeNode->first_node("xs:restriction")->first_attribute("base")->value();
	if(isNativeType(baseType))
	{
		return baseType;
	}
	else
	{
		return parseSimpleTypeNode(getSimpleTypeNode(baseType));
	}
}

xml_node<>* XMLDefinitionGenerator::getSimpleTypeNode(std::string searchTerm)
{
	//std::cout << "In getSimpleTypeNode, searchTerm = " << searchTerm << std::endl;
	for(uint i = 0; i < rootXSDNodes.size(); i++)
	{
		//std::cout << "Searching rootXSDNodes index: " << i << std::endl;
		for(xml_node<>* childNode = rootXSDNodes[i]->first_node("xs:simpleType"); childNode; childNode = childNode->next_sibling("xs:simpleType"))
		{
			xml_attribute<>* nameAttr = childNode->first_attribute("name");
			if(nameAttr && nameAttr->value() == searchTerm)
			{
				return childNode;
			}
		}
	}
	//std::cout << "No xs:simpleType Node with name " << searchTerm << " found" << std::endl;
	return 0;
}

xml_node<>* XMLDefinitionGenerator::parseComplexTypeNode(xml_document<>& elementDefinition, xml_node<>* complexTypeNode, const char* parentNodeName)
{
	//xs:complexType Nodes will have one of these ChildNodes. Sequence, ComplexContent or SimpleContent
	//Sequence node may also come with an xs:attribute node
	//std::cout << "In parseComplexTypeNode" << std::endl;
	if(complexTypeNode->first_node("xs:sequence"))
	{
		xml_node<>* newNode = createNode(elementDefinition, "xs:element");
		addAttribute(elementDefinition, newNode, "name", parentNodeName);
		parseSequenceNode(elementDefinition, complexTypeNode->first_node("xs:sequence"), newNode);
		xml_node<>* attributeNode = complexTypeNode->first_node("xs:attribute");
		if(attributeNode)
		{
			parseAttributeNode(elementDefinition, attributeNode, newNode);
		}
		return newNode;
	}
	else if(complexTypeNode->first_node("xs:complexContent"))
	{
		return parseComplexContentNode(elementDefinition, complexTypeNode->first_node("xs:complexContent"), parentNodeName);
	}
	else if(complexTypeNode->first_node("xs:simpleContent"))
	{
		xml_node<>* newNode = createNode(elementDefinition, "xs:element");
		addAttribute(elementDefinition, newNode, "name", parentNodeName);
		parseSimpleContentNode(elementDefinition, complexTypeNode->first_node("xs:simpleContent"), newNode);
		return newNode;
	}
	else
	{
		std::cout << "Error parsing ComplexType Node: " << *complexTypeNode << std::endl;
		return 0;
	}
}

xml_node<>* XMLDefinitionGenerator::getComplexTypeNode(std::string searchTerm)
{
	//std::cout << "In getComplexTypeNode, searchTerm = " << searchTerm << std::endl;
	for(uint i = 0; i < rootXSDNodes.size(); i++)
	{
		for(xml_node<>* childNode = rootXSDNodes[i]->first_node("xs:complexType"); childNode; childNode = childNode->next_sibling("xs:complexType"))
		{
			xml_attribute<>* nameAttr = childNode->first_attribute("name");
			if(nameAttr && nameAttr->value() == searchTerm)
			{
				return childNode;
			}
		}
	}
	
	//std::cout << "No xs:complexType Node with name " << searchTerm << " found!" << std::endl;
	return 0;
}

void XMLDefinitionGenerator::parseSimpleContentNode(xml_document<>& elementDefinition, xml_node<>* simpleContentNode, xml_node<>* parentNode)
{
	//std::cout << "In parseSimpleContentNode" << std::endl;
	std::string baseType = simpleContentNode->first_node("xs:extension")->first_attribute("base")->value();
	if (isNativeType(baseType))
	{
		addAttribute(elementDefinition, parentNode, "type", baseType);
	}
	else
	{
		xml_node<>* typeChildNode = getSimpleTypeNode(baseType);
		if(!typeChildNode)
		{
			typeChildNode = getComplexTypeNode(baseType);
			if(!typeChildNode)
			{
				std::cout << "Error parsing simpleContent Node, node: " << *simpleContentNode << std::endl;
			}
			else
			{
				//TODO
				//Not certain it's really possible to get here. ComplexType within a SimpleContent doesn't make much sense.
			}
		}
		else
		{
			addAttribute(elementDefinition, parentNode, "type", parseSimpleTypeNode(typeChildNode));
		}
	}
	xml_node<>* attributeNode = simpleContentNode->first_node("xs:extension")->first_node("xs:attribute");
	if (attributeNode)
	{
		parseAttributeNode(elementDefinition, attributeNode, parentNode);
	}
}

xml_node<>* XMLDefinitionGenerator::parseComplexContentNode(xml_document<>& elementDefinition, xml_node<>* complexContentNode, const char* parentNodeName)
{
	//std::cout << "In parseComplexContentNode" << std::endl;
	std::string baseType = complexContentNode->first_node("xs:extension")->first_attribute("base")->value();
	xml_node<>* typeChildNode = getComplexTypeNode(baseType);
	if(!typeChildNode)
	{
		std::cout << "Error finding complexType. Name: " << typeChildNode << std::endl;
		return 0;
	}
	else
	{
		xml_node<>* newNode = parseComplexTypeNode(elementDefinition, getComplexTypeNode(baseType), parentNodeName);
		xml_node<>* sequenceNode = complexContentNode->first_node("xs:extension")->first_node("xs:sequence");
		if(sequenceNode)
		{
			parseSequenceNode(elementDefinition, sequenceNode, newNode); 
		}
		xml_node<>* attributeNode = complexContentNode->first_node("xs:extension")->first_node("xs:attribute");
		if(attributeNode)
		{
			parseAttributeNode(elementDefinition, attributeNode, newNode);
		}
		return newNode;
	}
}

void XMLDefinitionGenerator::parseSequenceNode(xml_document<>& elementDefinition, xml_node<>* sequenceNode, xml_node<>* parentNode)
{
	//std::cout << "In parseSequenceNode" << std::endl;
	for(xml_node<>* sequenceChildNode = sequenceNode->first_node(); sequenceChildNode; sequenceChildNode = sequenceChildNode->next_sibling())
	{
		if(strcmp(sequenceChildNode->name(), "xs:element") == 0)
		{
			xml_node<>* childNode = parseElementNode(elementDefinition, sequenceChildNode);
			addChildToParent(childNode, parentNode);
		}
		else
		{
			std::cout << "Non xs:element child node of xs:sequence found" << std::endl;
		}
	}
}

void XMLDefinitionGenerator::parseAttributeNode(xml_document<>& elementDefinition, xml_node<>* attributeNode, xml_node<>* parentNode)
{
	xml_attribute<>* refAttr = attributeNode->first_attribute("ref");
	xml_attribute<>* typeAttr = attributeNode->first_attribute("type");
	xml_attribute<>* nameAttr = attributeNode->first_attribute("name");
	
	if(refAttr && !typeAttr && !nameAttr) //ref
	{
		parseAttributeNode(elementDefinition, getAttributeNode(refAttr->value()), parentNode);
	}
	else if (!refAttr && typeAttr && nameAttr) //type
	{
		std::string typeAttrValue = typeAttr->value();
		if (!isNativeType(typeAttrValue))
		{
			typeAttrValue = parseSimpleTypeNode(getSimpleTypeNode(typeAttrValue));
		}
		xml_node<>* attrDefNode = createNode(elementDefinition, "xs:attribute");
		addAttribute(elementDefinition, attrDefNode, "name", nameAttr->value());
		addAttribute(elementDefinition, attrDefNode, "type", typeAttrValue);
		addChildToParent(attrDefNode, parentNode);
	}
	else if (!refAttr && !typeAttr && nameAttr) //name
	{
		std::string typeAttrValue = parseSimpleTypeNode(attributeNode->first_node("xs:simpleType"));
		xml_node<>* attrDefNode = createNode(elementDefinition, "xs:attribute");
		addAttribute(elementDefinition, attrDefNode, "name", nameAttr->value());
		addAttribute(elementDefinition, attrDefNode, "type", typeAttrValue);
		addChildToParent(attrDefNode, parentNode);
	}
	else
	{
		std::cout << "Error parsing Attribute Node. Unrecognised Node version" << std::endl;
		std::cout << "\tnameAttr: " << nameAttr << ", typeAttr: " << typeAttr << ", refAttr: " << refAttr << std::endl;
	}
}

xml_node<>* XMLDefinitionGenerator::getAttributeNode(std::string searchTerm)
{
	//std::cout << "In getAttributeNode, searchTerm = " << searchTerm << std::endl;
	for(uint i = 0; i < rootXSDNodes.size(); i++)
	{
		//std::cout << "Searching rootXSDNodes index: " << i << std::endl;
		for(xml_node<>* childNode = rootXSDNodes[i]->first_node("xs:attribute"); childNode; childNode = childNode->next_sibling("xs:attribute"))
		{
			xml_attribute<>* nameAttr = childNode->first_attribute("name");
			if(nameAttr && nameAttr->value() == searchTerm)
			{
				return childNode;
			}
		}
	}
	
	std::cout << "No xs:attribute Node with name " << searchTerm << " found" << std::endl;
	return 0;
}

bool XMLDefinitionGenerator::isNativeType(std::string typeDefinition)
{
	return typeDefinition.substr(0, 3) == "xs:";
}

xml_node<>* XMLDefinitionGenerator::createNode(xml_document<> &xmlDoc, const char* name)
{
	//std::cout << "In createNode" << std::endl;
	char* nodeName = xmlDoc.allocate_string(name);
	return xmlDoc.allocate_node(node_element, nodeName);
}

void XMLDefinitionGenerator::addChildToParent(xml_node<>* childNode, xml_node<>* parentNode)
{
	parentNode->append_node(childNode);
}

void XMLDefinitionGenerator::addAttribute(xml_document<> & xmlDoc, xml_node<>* node, const std::string name, const std::string value)
{
	//std::cout << "In addAttribute name: " << name << ", value: " << value << std::endl;
	char* attrName = xmlDoc.allocate_string(&name[0]);
	char* attrValue = xmlDoc.allocate_string(&value[0]);
	xml_attribute<>* attr = xmlDoc.allocate_attribute(attrName, attrValue);
	node->append_attribute(attr);
	//std::cout << "Attribute added" << std::endl;
}
