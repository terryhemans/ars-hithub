#include <iostream>
#include "xmlDefinitionGenerator.hpp"

int main( const int argc, const char *const argv[] )
{
	if(argc == 3)
	{
		XMLDefinitionGenerator definitionGenerator;
		std::cout << definitionGenerator.getRootNodeDefinition(argv[2], argv[1]) << std::endl;
	}
	else
	{
		std::cout << "Incorrect number of arguments provided" << std::endl;
		std::cout << "Argument 1: XSD filepath" << std::endl;
		std::cout << "Argument 2: Element name to parse" << std::endl;
	}
}