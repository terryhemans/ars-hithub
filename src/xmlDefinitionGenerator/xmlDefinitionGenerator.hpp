#pragma once
#include "../xmlParser/rapidxml.hpp"
#include <vector>

using namespace rapidxml;

class XMLDefinitionGenerator
{
	public:
		std::string getRootNodeDefinition(std::string searchTerm, std::string filePath);

	private:
		xml_node<>* getElementNode(xml_document<>& elementDefinition, const std::string & searchTerm);
		std::string readFile(std::string filePath);
		void getIncludedFiles(std::vector<std::string>& xsdStrings, std::string filePath);
		std::string parseSimpleTypeNode(xml_node<>* node);
		xml_node<>* parseElementNode(xml_document<>& elementDefinition, xml_node<>* elementNode);
		xml_node<>* parseNameElementNode(xml_document<>& elementDefinition, xml_node<>* nameElementNode, const char* nameAttrValue);
		xml_node<>* parseTypeElementNode(xml_document<>& elementDefinition, const char* nameAttrValue, const char* typeAttrValue);
		xml_node<>* getSimpleTypeNode(std::string searchTerm);
		xml_node<>* getComplexTypeNode(std::string searchTerm);
		xml_node<>* parseComplexTypeNode(xml_document<>& elementDefinition, xml_node<>* node, const char* parentNodeName);
		void parseSimpleContentNode(xml_document<>& elementDefinition, xml_node<>* simpleContentNode, xml_node<>* node);
		xml_node<>* parseComplexContentNode(xml_document<>& elementDefinition, xml_node<>* complexContentNode, const char* parentNodeName);
		void parseSequenceNode(xml_document<>& elementDefinition, xml_node<>* sequenceNode, xml_node<>* parentNode);
		void parseAttributeNode(xml_document<>& elementDefinition, xml_node<>* attributeNode, xml_node<>* parentNode);
		xml_node<>* getAttributeNode(std::string searchTerm);
		
		xml_node<>* createNode(xml_document<> &xmlDoc, const char* name);
		bool isNativeType(std::string typeDefinition);
		void addChildToParent(xml_node<>* childNode, xml_node<>* parentNode);
		void addAttribute(xml_document<> & xmlDoc, xml_node<>* node, const std::string name, const std::string value);
	
		std::vector<xml_node<>*> rootXSDNodes;
};