#pragma once
#include <string>

class XMLValidator
{
	public:
		//Constructor
		XMLValidator(std::string filename);
		bool validateXmlAgainstXsd(const std::string & xml);
	
	private:
		std::string xsdFilename;
		std::string runCommandGetOutput(const char* cmd);
};