//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: operational_log.cpp 3146 2018-09-07 16:50:35Z david $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/operational_log.cpp $
//-
//- DESCRIPTION:
//- Provides basic logging functions
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "operational_log.hpp"

#include "common.hpp"
#include "log_streams.hpp"
#include "time.hpp"

#include <chrono>
#include <iostream>


//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

// a null outputstream for discarding logs with a severity lower than configured
class NullBuffer : public std::streambuf
{
public:
    int overflow(int c)
    {
        return c;
    }
};

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables
static LogStreams outputStreams;
static std::ostream nullStream( new NullBuffer );  // for discarding logs with a severity lower than configured
static std::string healthStatus = "";

    LogLevel logLevel = info;

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

///
/// getTimestamp
///
/// Returns the current time formatted as 'yyyy:mm:dd hh:MM:ss.sss'
///
std::string getTimestamp()
{
    EpochMilliSecond epochMs = Time::getEpochMilliSecond(Time::gctSnapShot);
    int ms = epochMs % 1000;

    Time::HubTime epochSec = epochMs / 1000;
    tm *ltm = localtime(&epochSec);

    // construct the time string and return it
    long tzOffset = ltm->tm_gmtoff;
    char plusminus = '+';
    if( tzOffset < 0 )
    {
        plusminus = '-';
        tzOffset = -tzOffset;
    }
    return std::to_string((long long)(ltm->tm_year+1900)) + "-" + digits2(ltm->tm_mon+1) + "-" + digits2(ltm->tm_mday) + " " +
           digits2(ltm->tm_hour) + ":" + digits2(ltm->tm_min) + ":" + digits2(ltm->tm_sec) + "." + digits3(ms) + " " +
           plusminus + digits2(tzOffset/3600) + digits2((tzOffset/60) % 60);
}


//-----------------------------------------------------------------------------

///
/// levelToString
///
/// converts the given log level into a string in square brackets and whitespace
///
std::string levelToString( LogLevel level )
{
    switch(level)
    {
        case trace:   return "[trace]";
        case debug:   return "[debug]";
        case info:    return "[info]";
        case warning: return "[warning]";
        case error:   return "[error]";
        case alert:   return "[alert]";
        case fatal:   return "[fatal]";
    }
    return "[unknown]";
}


//-----------------------------------------------------------------------------

std::ostream& log(LogLevel level)
{
    if( level == alert )
    {
        outputStreams.getAlertStream() << std::endl << getTimestamp() << "\t" "Hitachi_Hub" "\t" << healthStatus << "\t" << std::flush;
        return outputStreams.getAlertStream();
    }
    if( level >= logLevel ) // change the level to 'trace' to enable tracing
    {
        outputStreams.getEventStream() << formatLogLine(level) << std::flush;
        return outputStreams.getEventStream();
    }
    return nullStream;
}


//-----------------------------------------------------------------------------

void flushOperationalLog()
{
    outputStreams.getEventStream() << std::flush;
    outputStreams.getAlertStream() << std::flush;
}


//-----------------------------------------------------------------------------

void setLogStatus( std::string status )
{
    healthStatus = status;
}


//-----------------------------------------------------------------------------

std::string formatLogLine(LogLevel level)
{
    std::string result = "\n" + getTimestamp() + "\t" "Hitachi_Hub" "\t" + levelToString(level) + "\t"
             + healthStatus + "\t";

    return result;
}


//-----------------------------------------------------------------------------

// eof
