//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: operational_log.hpp 4458 2019-11-19 11:38:30Z david $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/operational_log.hpp $
//-
//- DESCRIPTION:
//- Operational Log.  Provides logging functions with severity options.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef operational_log_h__
#define operational_log_h__

#define BOOST_LOG_DYN_LINK 1 // necessary when linking the boost_log library dynamically

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <iostream>
#include <string>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

#define LOG_TRACE   log(trace)
#define LOG_DEBUG   log(debug)
#define LOG_INFO    log(info)
#define LOG_WARNING log(warning)
#define LOG_ERROR   log(error)
#define LOG_ALERT   log(alert)
#define LOG_FATAL   log(fatal)
#define LOG_STATUS  logStatus

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Enumerations

enum LogLevel { trace=1, debug, info, warning, error, alert, fatal };

// Constants

extern LogLevel logLevel;  // Logs >= to this logLevel will be logged

// Structures

// Variables

// Templates

// Classes


//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

///
/// log
///
/// usage: log(<severity level>) << "my message"
///
/// If the severity level is 'alert' then this function logs the given message to stderr preceded by the current time
/// in the format:
///    yyyy:mm:dd hh:MM:ss.sss message
///
/// Otherwise, it logs the given message to stdout preceded by the current time and severity level in the format:
///    yyyy:mm:dd hh:MM:ss.sss [severity level] message
///
/// It ignores any non-alert logged with a <severity level> lower than info.
///
/// The function works by returning an output stream having already written the timestamp and severity level to it.
/// If the log level is less than 'info' then a dummy stream is returned, which will discard anything written to it.
///
/// Change the hardcoded level below to enable debug or trace logs.
///
std::ostream& log(LogLevel level = info);


///
/// flushOperationalLog
///
/// Flushes the log and alert output to ensure all logs are written
///
void flushOperationalLog();


///
/// setLogStatus
///
/// The given status string will be added to every subsequent log event
///
void setLogStatus( std::string status );

///
/// formatLogLine
///
/// Gets the log level line in a format suitable for logging
///
std::string formatLogLine(LogLevel level);

#endif
