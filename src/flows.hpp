//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- Copyright (c) 2017 Hitachi Limited. All rights reserved.
//-
//- $Id:  $
//-
//- $URL: s $
//-
//- DESCRIPTION: declaration of the Flow class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef FLOWS_HPP
#define FLOWS_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

enum FlowId {
    FlowIdS002,
    FlowIdS009,
    FlowIdS013,
    FlowIdS015,
    FlowIdCount
};

enum FlowDirection {
    LinxToTranista,
    TranistaToLinx
};

// Structures

// Variables

// Templates

// Classes

class Flows {
public:
    static const std::string & getName(FlowId flowId);

    static FlowDirection getDirection(FlowId flowId);
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

extern const std::string & FlowName(FlowId flowId);

extern bool FlowIsLinxToTranista(FlowId flowId);

//-----------------------------------------------------------------------------

#endif  // FLOWS_HPP
