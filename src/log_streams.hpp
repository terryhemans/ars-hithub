//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- Defines a stubbable object providing streams for the Event and Alert logs.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef LOG_STREAMS_HPP
#define LOG_STREAMS_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <iostream>
#include <fstream>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Enumerations

// Constants

// Structures

// Variables

// Templates

// Classes
class LogStreams
{
    public:
        ///
        /// Constructor. Populates the private members. If filestreams are used
        /// then they shall be opened here.
        ///
        LogStreams();


        ///
        /// Destructor. If filestreams are used then they shall be closed here.
        ///
        ~LogStreams();

        ///
        /// getEventStream()
        ///
        /// Interrogator function returning the stream to be used for the Event
        /// log.
        ///
        std::ostream& getEventStream(void);


        ///
        /// getAlerttStream()
        ///
        /// Interrogator function returning the stream to be used for the Alert
        /// log.
        ///
        std::ostream& getAlertStream(void);

    private:

        ///
        // Member objects used to store filestream handles if required for the 
        // Event and Alert logs.
        std::ofstream eventLog;
        std::ofstream alertLog;

};

#endif  // LOG_STREAMS_HPP

