//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- Defintion of common functions.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <algorithm>
#include <cctype>
#include <cstring>
#include <sstream>
#include <map>

#include "common.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

bool isDigits(const char * p)
{
    for (;;) {
        char c = *p++;
        if (c == '\0') return true;
        if (c < '0') return false;
        if (c > '9') return false;
    }
}

//-----------------------------------------------------------------------------

bool isDigits(const std::string & str)
{
    return isDigits(str.c_str());
}

//-----------------------------------------------------------------------------

std::string cymdOfDdMonYy(const std::string & ddMonYy)
{
    // input:  "dd-mon-yy"
    //          01 345 78
    // output: "ccyymmdd"
    //          01234567

    std::string cymd;

    if ((ddMonYy.length() >= 9) && (ddMonYy[2] == '-') && (ddMonYy[6] == '-'))
    {
        cymd.resize(8);

        cymd[0] = '2';
        cymd[1] = '0';
        cymd[2] = ddMonYy[7];
        cymd[3] = ddMonYy[8];

        char mon[4];
        mon[0] = ::tolower(ddMonYy[3]);
        mon[1] = ::tolower(ddMonYy[4]);
        mon[2] = ::tolower(ddMonYy[5]);
        mon[3] = '\0';
        static const char months[] = "--- jan feb mar apr may jun jul aug sep oct nov dec";
        const char * pos = strstr(months, mon);
        int m = (pos != 0) ? ((pos - months) / 4) : 0;

        cymd[4] = '0' + (m / 10);
        cymd[5] = '0' + (m % 10);

        cymd[6] = ddMonYy[0];
        cymd[7] = ddMonYy[1];
    }

    return cymd;
}

//-----------------------------------------------------------------------------

std::string cymdOfNiceDate(const std::string & niceDate)
{
	// input:  "ccyy-mm-dd XXXX"
	//          0123 56 89
	// output: "ccyymmdd"
	//          01234567

	std::string cymd;

	if (niceDate.length() >= 10)
	{
		cymd.resize(8);

		cymd[0] = niceDate[0];
		cymd[1] = niceDate[1];
		cymd[2] = niceDate[2];
		cymd[3] = niceDate[3];

		cymd[4] = niceDate[5];
		cymd[5] = niceDate[6];

		cymd[6] = niceDate[8];
		cymd[7] = niceDate[9];
	}

	return cymd;
}

//-----------------------------------------------------------------------------

std::string niceDateOf(const std::string & ccyymmdd, char separator)
{
	// input:  "ccyymmdd"
	//          01234567
	// output: "ccyy-mm-dd"
	//          0123 56 89

	std::string niceDate;

	if (ccyymmdd.length() >= 8)
	{
		niceDate.append(10, separator);

		niceDate[0] = ccyymmdd[0];
		niceDate[1] = ccyymmdd[1];
		niceDate[2] = ccyymmdd[2];
		niceDate[3] = ccyymmdd[3];

		niceDate[5] = ccyymmdd[4];
		niceDate[6] = ccyymmdd[5];

		niceDate[8] = ccyymmdd[6];
		niceDate[9] = ccyymmdd[7];
	}

	return niceDate;
}

//-----------------------------------------------------------------------------

std::string dashDateOf(const std::string & ccyymmdd)
{
	return niceDateOf(ccyymmdd, '-');
}

//-----------------------------------------------------------------------------

std::string slashDateOf(const std::string & ccyymmdd)
{
	return niceDateOf(ccyymmdd, '/');
}

//-----------------------------------------------------------------------------

std::string colonTimeOfDaySecond(const std::string & daySecondStr)
{
	DaySecond daySecond = stoll(daySecondStr);
	return colonTimeOfDaySecond(daySecond);
}

//-----------------------------------------------------------------------------

std::string colonTimeOfDaySecond(DaySecond daySecond)
{
	int hour = daySecond / 3600;
	int minute = (daySecond / 60) % 60;
	int second = daySecond % 60;
	char cs[100];
	sprintf(cs, "%02d:%02d:%02d", hour, minute, second);
	std::string result = cs;
	return result;
}

//-----------------------------------------------------------------------------

bool secondsOfDateTime(const std::string & dateTime, Seconds & seconds )
{
    const std::string dateTimeUtc = dateTime + " UTC";
    struct tm a_tm = {0};
    bool result = strptime(dateTimeUtc.c_str(), "%Y/%m/%d %H:%M:%S %Z", &a_tm) != NULL;

    if (result) {
        time_t a_time = mktime(&a_tm);
        seconds = static_cast<Seconds>(a_time);
    }

    return result;
}

//-----------------------------------------------------------------------------

Seconds secondsOfDateTime(const std::string & dateTime )
{
    Seconds seconds = 0;
    secondsOfDateTime(dateTime, seconds );
    return seconds;
}

//-----------------------------------------------------------------------------

bool dayOfCymd(const std::string & ccyymmdd, EpochDay & epochDay )
{
    struct tm a_tm = {0};
    bool result = strptime(ccyymmdd.c_str(), "%Y%m%d", &a_tm) != NULL;

    if (result) {
        time_t a_time = mktime(&a_tm);
        Seconds seconds = static_cast<Seconds>(a_time);
        epochDay = seconds / secondsPerDay;
    }

    return result;
}

//-----------------------------------------------------------------------------

EpochDay dayOfCymd(const std::string & ccyymmdd )
{
    EpochDay epochDay = 0;
    dayOfCymd(ccyymmdd, epochDay);
    return epochDay;
}

//-----------------------------------------------------------------------------

/* unused ?
Seconds secondsOfCymd(const std::string & ccyymmdd )
{
	struct tm a_tm = { 0,0,0,0,0,0,0,0,0,0,0 };
	strptime(ccyymmdd.c_str(), "%Y%m%d", &a_tm);
	time_t a_time = mktime(&a_tm);
	Seconds seconds = static_cast<Seconds>(a_time);
	return seconds;
}*/

//-----------------------------------------------------------------------------

std::string leftOf(const std::string & s, size_t length, char fill_ch)
{
	std::string result = s;
	result.append(length, fill_ch);
	return result.substr(0, length);
}

//-----------------------------------------------------------------------------

std::string digits2(int x)
{
	std::string str;

	if (x < 10)
	{
		str = "0" + std::to_string(static_cast<long long>(x));
	}
	else
	{
		str = "" + std::to_string(static_cast<long long>(x));
	}

	return str;
}

//-----------------------------------------------------------------------------

std::string digits3(int x)
{
	std::string str;

	if (x < 10)
	{
		str = "00" + std::to_string(static_cast<long long>(x));
	}
	else if (x < 100)
	{
		str = "0" + std::to_string(static_cast<long long>(x));
	}
	else
	{
		str = "" + std::to_string(static_cast<long long>(x));
	}

	return str;
}

//-----------------------------------------------------------------------------

void trimSet(std::string& s, const char set[])
{
	size_t p = s.find_first_not_of(set);
	s.erase(0, p);

	p = s.find_last_not_of(set);
	if (std::string::npos != p) {
		s.erase(p + 1);
	}
}

//-----------------------------------------------------------------------------

std::string trimSetFrom(const std::string& s, const char set[])
{
	std::string result = s;
	trimSet(result, set);
	return result;
}

//-----------------------------------------------------------------------------

void trimWhitespace(std::string& s)
{
	trimSet(s, " \t\r\n\f\v");
}

//-----------------------------------------------------------------------------

std::string trimWhitespaceFrom(const std::string& s)
{
	std::string result = s;
	trimWhitespace(result);
	return result;
}

//-----------------------------------------------------------------------------

void makeLowerCase(std::string & s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
}

//-----------------------------------------------------------------------------

std::string lowerCaseOf(const std::string & s)
{
	std::string result = s;
	makeLowerCase(result);
	return result;
}

//-----------------------------------------------------------------------------

void makeUpperCase(std::string & s)
{
	std::transform(s.begin(), s.end(), s.begin(), ::toupper);
}

//-----------------------------------------------------------------------------

std::string upperCaseOf(const std::string & s)
{
	std::string result = s;
	makeUpperCase(result);
	return result;
}

//-----------------------------------------------------------------------------

bool boolOfString(const std::string& boolString, bool &returnBool)
{
	std::string lowerString = lowerCaseOf(boolString);

	if (lowerString == "true") {
		returnBool = true;
		return true;
	}

	if (lowerString == "false") {
		returnBool = false;
		return true;
	}

	return false;
}

//-----------------------------------------------------------------------------

bool longOfString(const std::string& numericString, long & value)
{
	const char * p = numericString.c_str();
	char * pEnd;
	long int lli = strtol(p, &pEnd, 10);
	if (size_t(pEnd - p) != numericString.length()) {
		return false;
	}

	value = lli;

	return true;
}

//-----------------------------------------------------------------------------

const char * skipWhitespace(const char * p)
{
	while (isspace(*p))
	{
		p++;
	}

	return p;
}

//-----------------------------------------------------------------------------

std::string removeToken(const char *& text_p, const char separatorSet[])
{
    std::string token = removeWhitespaceToken(text_p, separatorSet);
    text_p = skipWhitespace(text_p);
    return trimWhitespaceFrom(token);
}

//-----------------------------------------------------------------------------

std::string removeWhitespaceToken(const char *& text_p, const char separatorSet[])
{
    std::string token;
    for (;;)
    {
        char ch = *text_p;
        if (ch == '\0') {
            return token;
        }
        if (strchr(separatorSet, ch) != NULL) {
            text_p++;
            return token;
        }
        token += ch;
        text_p++;
    }
}

//-----------------------------------------------------------------------------

bool skipToken(const char *& text_p, const char token[])
{
    bool result = skipWhitespaceToken(text_p, token);
    text_p = skipWhitespace(text_p);
    return result;
}

//-----------------------------------------------------------------------------

bool skipWhitespaceToken(const char *& text_p, const char token[])
{
    int len = strlen(token);
    
    if (len > 0) {
        do {
            const char * pos = strstr(text_p, token);
            if (pos == 0)
            {
                return false;
            }

            text_p = pos + len;
            
        } while (isalpha(*text_p) && isalpha(token[len-1]));
    }
    
    return true;
}

//-----------------------------------------------------------------------------

void replaceSubStrings(std::string & str, const std::string & findStr, const std::string & replaceStr)
{
    size_t pos = 0;
    
    for (;;) {
        pos = str.find(findStr, pos);
        if (pos == std::string::npos) break;
        str = str.substr(0, pos) + replaceStr + str.substr(pos + findStr.size());
        pos += replaceStr.size();
    }
}

//-----------------------------------------------------------------------------

std::string replaceSubStrings(const std::string & orgStr, const std::string & findStr, const std::string & replaceStr)
{
    std::string workStr = orgStr;

    replaceSubStrings(workStr, findStr, replaceStr);

    return workStr;
}

//-----------------------------------------------------------------------------

void replaceSubString(std::string & str, const std::string & findStr, const std::string & replaceStr)
{
    size_t pos = str.find(findStr, 0);
    if (pos != std::string::npos) {
        str = str.substr(0, pos) + replaceStr + str.substr(pos + findStr.size());
        pos += replaceStr.size();
    }
}

//-----------------------------------------------------------------------------

std::string replaceSubString(const std::string & orgStr, const std::string & findStr, const std::string & replaceStr)
{
    std::string workStr = orgStr;

    replaceSubString(workStr, findStr, replaceStr);

    return workStr;
}

//-----------------------------------------------------------------------------

std::string visibleControlsOf(const std::string & orgStr)
{
    std::string workStr = orgStr;
    
    replaceSubStrings(workStr, "\r", "<CR>");
    replaceSubStrings(workStr, "\n", "<NL>");
    
    return workStr;
}

//-----------------------------------------------------------------------------

std::string conflateStrings(const StringList & stringList, const std::string & separator)
{
    std::string result;

    const int num = stringList.size();

    if (num > 0) {
        int i;
        for (i = 0; i < (num - 1); i++) {
            result += stringList[i];
            result += separator;
        }
        result += stringList[i];
    }

    return result;
}

//-----------------------------------------------------------------------------

std::string conflateLines(const StringList & lineList)
{
    return conflateStrings(lineList, "\n");
}

//-----------------------------------------------------------------------------

void separateStrings(const std::string & strings, StringList & stringList, const std::string & separator)
{
    size_t begin = 0;
    for (;;) {
        size_t end = strings.find(separator, begin);
        if (end == std::string::npos) {
            if (strings.substr(begin) != "") {
                stringList.push_back(strings.substr(begin));
            }
            break;
        }
        stringList.push_back(strings.substr(begin, end-begin));
        begin = end + separator.size();
    }
}

//-----------------------------------------------------------------------------

void separateLines(const std::string & lines, StringList & lineList)
{
    separateStrings(lines, lineList, "\n");
}

//-----------------------------------------------------------------------------
bool isValueInVector(StringList& searchVector, std::string& searchString)
{
	for(uint i = 0; i< searchVector.size(); i++)
	{
		if(searchVector[i] == searchString)
		{
			return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------
std::string getValueFromMap(std::map<std::string, std::string>& searchMap, std::string& key)
{
	if (searchMap.find(key) == searchMap.end())
	{
		return "";
	}
	else
	{
		return searchMap[key];
	}
}