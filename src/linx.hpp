//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: linx.hpp 4416 2019-11-06 16:08:11Z jules $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/linx.hpp $
//-
//- DESCRIPTION:
//- Linx Class.  Top level class abstracting the interface to the Linx.
//- Interfaces with the IBM WebSphere MQI component via the imqi.hpp interface.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef LINX_HPP
#define LINX_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>
#include <imqi.hpp>

#include "linx_interface.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class Linx : public LinxInterface
{

    public:

        ///
        /// Constructor
        ///
        /// Constructs the MQI manager and channel and calls the connect command.
        ///
        /// The Manager manages a connection across a Channel to a remote manager.  Once connected, the Queue is registered
        ///  with the Manager to open the MQ across the managed channel.
        ///
        /// @post the mgr, channel and queue objects are created.
        /// @post the queue may or may not be connected depending on the success of the connection operation
        ///
        Linx();


        ///
        /// Destructor
        ///
        /// Closes the MQ connection and deletes all IMQ objects
        ///
        /// @post the MQ is disconnected and all Linx MQ queue objects are deleted
        ///
        ~Linx();


        ///
        /// Initialise Connection
        ///
        /// Initialises the MQI manager and channel and calls the connect command.
        ///
        /// The Manager manages a connection across a Channel to a remote manager.  Once connected, the Queue is registered
        ///  with the Manager to open the MQ across the managed channel.
        ///
        /// @post the mgr, channel and queue objects are created.
        /// @post the queue may or may not be connected depending on the success of the connection operation
        ///
        void initialiseConnection(const LinxInterface::ConnectionDetails connection);

        ///
        /// isAvailable()
        ///
        /// @return true if the remote manager is connected.  False otherwise.
        ///
        bool isAvailable();

        /// Gets the next message from the given queue.
        ///
        /// @return whether the message is returned and the string containing the message.
        ///
        bool removeMessage(FlowId flowId, std::string & message);

        ///
        /// appendMessage
        ///
        /// Appends the message to the output queue.
        ///
        /// @return false if message not appended.
        ///
        bool appendMessage(FlowId flowId, const char * chars);

        ///
        /// tick1s()
        ///
        /// Perform periodic activities
        ///
        void tick1s();


    protected:

        /// IBM WebSphere MQI class objects
        ImqQueueManager* mgr;
        ImqChannel* channel;
        ImqQueue* queues[FlowIdCount];
        bool reportedError;

    private: public:

        ///
        /// connect()
        ///
        /// Connects to the remote queue manager, handling any errors.  If successful, the MQ is opened.
        ///
        /// @post the connection to the remote manager is made and the MQ is opened.
        /// @post if the connection failed the MQ will not be opened.  No connection retry attempt is made.
        ///
        void connect();

        ///
        /// Disconnect the Linx interface.
        ///
        void disconnect();
};


//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------


#endif  // LINX_HPP
