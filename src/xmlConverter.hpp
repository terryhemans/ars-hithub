#pragma once
#include <vector>
#include <map>
#include "xmlParser/rapidxml.hpp"
#include "simpleTypeChecker.hpp"

class XMLConverter
{
	public:
		XMLConverter();
		std::string convertXMLtoJSON(std::string & rawXML);
		
	private:
		std::string convertXML(rapidxml::xml_node<> *rootNode, rapidxml::xml_node<> * definition);
		void buildJsonString(const std::string &attrString,
							 const std::string &nameString,
							 const std::string &childString,
							 const std::string &valueString,
							 std::string &nodeString);
		void buildAttrString(const std::vector<std::string> &attrs, std::string &resultString);
		void buildChildString(const std::vector<std::string> &childNodes, const std::string & parentName, std::string &resultString, rapidxml::xml_node<>* definition);
		void replaceAllSpecialCharacters(std::string & nodeString);
		
		std::string CONTENT_PREFIX;
		std::string ATTR_PREFIX;
		SimpleTypeChecker typeChecker;
		std::map<std::string, std::string> messageDefinitionsMap;
};