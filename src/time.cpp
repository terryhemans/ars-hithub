//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:$
//-
//- $URL:$
//-
//- DESCRIPTION:
//- Time Class body.
//-
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <chrono>
#include <time.h>

#include "config_file_template.hpp"
#include "time.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS/DEFINITIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

//-----------------------------------------------------------------------------

// Variables

/*export*/ EpochMilliSecond (*get_time_milli_second_p)(Time::GetCurrentTimeType currentTimeType) = 0;

//-----------------------------------------------------------------------------
//- FUNCTIONS
//-----------------------------------------------------------------------------

bool timeIsSummerTime( const Time::HubTime utcTime )
{
    const time_t a_time = static_cast<time_t>(utcTime);
    const struct tm * a_tm_p = gmtime( &a_time );
    const struct tm & a_tm = *a_tm_p;

    bool isSummerTime = false;

    enum {
        TM_MON_MAR = 3-1,
        TM_MON_OCT = 10-1,
        TM_WEEK_FROM_MON_END = 31-7+1,
        TM_WDAY_SUN = 0,
        TM_HOUR_SPRING_FORWARD = 1,
        TM_HOUR_FALL_BACKWARD = 1
    };

    if ( (a_tm.tm_mon > TM_MON_MAR) && (a_tm.tm_mon < TM_MON_OCT) ) {
        isSummerTime = true;

    } else if ( (a_tm.tm_mon < TM_MON_MAR) || (a_tm.tm_mon > TM_MON_OCT) ) {
        isSummerTime = false;

    } else if (a_tm.tm_mon == TM_MON_MAR) {
        if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
            isSummerTime = false;
            
        } else if (a_tm.tm_wday > TM_WDAY_SUN) {
            isSummerTime = true;
            
        } else {
            isSummerTime = a_tm.tm_hour >= TM_HOUR_SPRING_FORWARD;
        }

    } else { // TM_MON_OCT
        if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
            isSummerTime = true;
            
        } else if (a_tm.tm_wday > TM_WDAY_SUN) {
            isSummerTime = false;
            
        } else {
            isSummerTime = a_tm.tm_hour < TM_HOUR_FALL_BACKWARD;
        }
    }

    return isSummerTime;
}

//-----------------------------------------------------------------------------

std::string getFormattedDateTime(const Time::HubTime utcTime)
{
    char buf[sizeof "2011-10-08T07:07:09+01:00"];

    bool isSummerTime = timeIsSummerTime( utcTime );
    
    if (isSummerTime) {
        const time_t a_time = static_cast<time_t>(utcTime + 3600); // advance by 1 hour
        strftime(buf, sizeof buf, "%FT%T+01:00", gmtime(&a_time));
        
    } else {
        const time_t a_time = static_cast<time_t>(utcTime);
        strftime(buf, sizeof buf, "%FT%T+00:00", gmtime(&a_time));
    }

    std::string result(buf);
    return result;
}

//-----------------------------------------------------------------------------

Time::HubTime Time::getCurrentTime(GetCurrentTimeType currentTimeType)
{
    return getEpochMilliSecond(currentTimeType) / 1000;
}

//-----------------------------------------------------------------------------

EpochMilliSecond Time::getEpochMilliSecond(GetCurrentTimeType currentTimeType)
{
    if (get_time_milli_second_p != 0)
    {
        return (*get_time_milli_second_p)(currentTimeType);
    }
    else if (currentTimeType == gctSnapShot)
    {
        return getEpochMilliSecondNow();
    }
    else 
    {
        // HUB-685
        static EpochMilliSecond epochMilliSecond = 0;

        if (currentTimeType == gctNextFrame)
        {
            // only change at start of frame
            epochMilliSecond = getEpochMilliSecondNow();
        }

        return epochMilliSecond;
    }
}

//-----------------------------------------------------------------------------

std::string Time::getIso8601Time()
{
    Time::HubTime now = getCurrentTime();
    
    return getFormattedDateTime(now);
}

//-----------------------------------------------------------------------------

EpochDay Time::getDay( Time::HubTime time ){ return time / secondsInDay; }

//-----------------------------------------------------------------------------

DaySecond Time::getTimeOfDay( Time::HubTime time ){ return time % secondsInDay; }

//-----------------------------------------------------------------------------

EpochMilliSecond Time::getEpochMilliSecondNow()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(
                  std::chrono::system_clock::now().time_since_epoch() ).count();
}

//-----------------------------------------------------------------------------
