//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:  $
//-
//- $URL:  $
//-
//- DESCRIPTION:
//- NtpMonitor Class.  Monitors synchronisation with the NTP server.
//-
//-----------------------------------------------------------------------------

#ifndef NTP_MONITOR_H
#define NTP_MONITOR_H

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class NtpMonitor
{

    public:

        ///
        /// Constructor
        ///
        /// Initialises member data.
        ///
        NtpMonitor();


        ///
        /// isSynchronised()
        ///
        /// Returns true if the last monitor event found the ntpd service to be synchronised 
        /// with the NTP server.
        ///
        bool isSynchronised();
        
        ///
        /// isAvailable()
        ///
        /// Returns true if the last monitor event found the ntpd service to be synchronised 
        /// or unsynchronised (i.e. the server is contactable) with the NTP server.
        ///
        bool isAvailable();
        
        ///
        /// hasBeenSynced()
        ///
        /// Returns true if the virtual machine has successfully synced with the NTP server at least once since power-up.
        ///
        bool hasBeenSynced();

        ///
        /// tick1s()
        ///
        /// This should be called every second. When intervalSeconds has elapsed it queries the 
        /// operating system for the NTP synchronisation status. If this has not changed then 
        /// it does nothing. Otherwise it writes to the Event Log if now synchronised, or to 
        /// the Alert Log if synchroisation has been lost.
        ///
        /// This operation also checks that all calls beyond the first have +1s difference in
        /// current Hub time compared with the last. A "Time anomaly" warning is logged should
        /// the check fail.
        /// 
        /// Use of member data:- 
        ///    intervalSeconds          R
        ///    secondsToNextCheck       R/W
        ///    currentSyncStatus        R/W
        ///    prevSyncStatus           R/W
        ///
        void tick1s();
		
		///
		/// StartupSync()
		///
		/// Function called by the Hub constructor to check the sync status of the ntp daemon
		/// on startup by the VM. If the VM sync was unsuccessful then it will wait until 
		/// synchronisation has been achieved.
		///
		void startupSync();

    private:

        // The interval in seconds between monitor events
        static const int intervalSeconds;

        // Remaining time (in seconds) to next monitor event
        int secondsToNextCheck;

        // Save Synchronisation Status - current and previous
        //    First define values returned by system calls to npstatus, plus 'unset'
        typedef enum { synchronised = 0, notSynchronised, indeterminant, unset } ntpStatusCodes;
        ntpStatusCodes currentSyncStatus;
        ntpStatusCodes prevSyncStatus;
        
        // Variable to indicate if ntp has been synced during the run.
        bool ntpHasBeenSynced;
        
        // Function to check NTP synchronisation
        void syncCheck();
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------


#endif // NTP_MONITOR_H
