//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id:$
//-
//- $URL:$
//-
//- DESCRIPTION:
//- Provides general time related utility functions
//-
//-----------------------------------------------------------------------------

#ifndef TIME_HPP
#define TIME_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <time.h>
#include <iostream>

#include "hithub_types.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

class Time
{

    public:

        /// Application-wide time type, a time in seconds since 1/1/1970
        typedef time_t HubTime;

        /// Time based constants
        static const HubTime secondsInDay = 86400;

        ///
        /// getCurrentTime
        ///
        /// Returns the test time or the current time in seconds since 1/1/1970
        ///
        enum GetCurrentTimeType { gctInitial, gctCurrentFrame, gctNextFrame, gctSnapShot };
        static HubTime getCurrentTime(GetCurrentTimeType currentTimeType = gctCurrentFrame);

        ///
        /// getEpochMilliSecond
        ///
        /// Returns the test time or the current time in milli seconds since 1/1/1970
        ///
        static EpochMilliSecond getEpochMilliSecond(GetCurrentTimeType currentTimeType = gctCurrentFrame);

        ////
        /// getIso8601Time
        ///
        /// Returns the current time in the format '2017-10-17T15:17:00Z'
        ///
        static std::string getIso8601Time();

        ///
        /// getDay
        ///
        /// Returns the number of days since 1/1/1970 for the given time
        ///
        static EpochDay getDay( HubTime time );

        ///
        /// getTimeOfDay
        ///
        /// Returns the time of day in seconds for the given time
        ///
        static DaySecond getTimeOfDay( HubTime time );

        ///
        /// getEpochMilliSecondNow
        ///
        /// generally, this should not be used - use getEpochMilliSecond instead
        ///
        /// Returns the current time in milli seconds since 1/1/1970
        ///
        static EpochMilliSecond getEpochMilliSecondNow();

};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

// this is only used by test functions, please do not use it
extern EpochMilliSecond (*get_time_milli_second_p)(Time::GetCurrentTimeType currentTimeType);

// determine whether the given time is during BST
extern bool timeIsSummerTime( const Time::HubTime utcTime );

// produce a formatted date time, depending on whether the given time is during BST
extern std::string getFormattedDateTime( const Time::HubTime utcTime );

#endif // TIME_HPP
