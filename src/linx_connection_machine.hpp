//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 531 2017-11-07 16:07:56Z jules $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules1/src/hub.hpp $
//-
//- DESCRIPTION:
//- Declaration of the LinxConnectionMachine Class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef LINX_CONNECTION_MACHINE_HPP
#define LINX_CONNECTION_MACHINE_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include "connection_machine.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

//-----------------------------------------------------------------------------

// Classes

//-----------------------------------------------------------------------------

class ConfigFile;

//-----------------------------------------------------------------------------

class LinxConnectionMachine {
    public:

        LinxConnectionMachine(const ConfigFile & configFile);

        ~LinxConnectionMachine();

        ///
        /// Determines whether the linx is highly available
        ///
        bool isHighlyAvailable(Time::HubTime time, bool availability);

    protected:
        // control
            ConnectionMachine * connectionMachine_p;

        // status
            bool previousAvailability;
};

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif // LINX_CONNECTION_MACHINE_HPP

//-----------------------------------------------------------------------------
