//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 4563 2020-01-08 14:00:18Z terry $
//-
//- $URL: file:///var/svn/repos/hithub/trunk/src/hub.hpp $
//-
//- DESCRIPTION:
//- Declaration of the Hub Class.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef HUB_HPP
#define HUB_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <stdint.h>

#include "ntp_monitor.hpp"
#include "xmlConverter.hpp"
#include "xmlValidator.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

//-----------------------------------------------------------------------------

// Classes

class FlowInterface;
class LinxConnectionMachine;
class LinxInterface;

//-----------------------------------------------------------------------------

class Hub
{

    public:

        ///
        /// Constructs the Hitachi Hub Application and tries to connect to both the Linx and
        /// the Tranista.
        ///
        /// To simplify testing, pre-constructed Linx and Tranista modules are passed in as parameters.
        ///
        /// Throws an unrecoverable HubException if any of the configuration data files cannot be read.
        ///
        Hub(LinxInterface* linxInterface);


        ///
        /// Destructor
        ///
        /// Destructs the Linx and Tranista interfaces
        ///
        /// @post   Linx and Tranista classes are deconstructed.
        ///
        ~Hub();

        ///
        /// run()
        ///
        /// The main scheduler of the Hitachi Hub application.  The optional parameter is used for testing purposes
        /// only.
        ///
        /// @param testIterations Test purposes only.
        ///        If not less than zero the scheduler will loop that many times then return.
        /// @param testDurationSeconds Test purposes only.
        ///        If not less than zero the scheduler will loop for that many seconds then return.
        /// @post does not return unless the testIterations or testDurationSeconds parameters are >= zero.
        ///
        void run(long testIterations = -1, long testDurationSeconds = -1 );

    private:
		
		/// The overall Hub Application health status, used for logging
        enum SoftwareStatus{ starting, healthy, compromised, failed };
        SoftwareStatus healthStatus;

        /// The external interface class objects
        LinxInterface & linx;

        // monitoring availability
        LinxConnectionMachine * linxConnectionMachine_p;

        bool tranistaErrorsDisabled;
        bool disableForecast;
        bool disableTJM;

        /// The NTP Monitor object
        NtpMonitor ntpMonitor;

        ///
        /// Updates the health status based on the status of the external links and logs any change
        ///
        void updateHealthStatus( bool linxAvailable, bool ttsAvailable );
		
		XMLConverter converter;
		XMLValidator xmlValidator;
};


//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#endif // HUB_HPP
