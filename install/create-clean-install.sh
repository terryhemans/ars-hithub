#!/bin/bash

if [[ "$1" -eq "" ]]; then
	echo "usage: create-clean-install.sh <releaseNumber>"
	exit 1
fi

installDir=../release-$1

echo =====================================================
echo create installation kit for $installDir

echo =====================================================
echo Ensure we are using the latest versions committed to Git

pushd ..
git checkout HEAD *
git checkout -- *
popd

echo =====================================================
echo Compiling binary
make -C ../src

echo =====================================================
echo copying binary and config files

mkdir -p $installDir/bin/

cp clean-install.sh $installDir
cp hithub-init-script-master $installDir
cp ../bin/hithub $installDir/bin
cp -r ../bin/xsdFiles $installDir/bin
cp ../bin/*.cfg $installDir/bin

echo =====================================================
echo creating application zip

tar -cf $installDir-cleanInstall.tar $installDir
gzip $installDir-cleanInstall.tar

rm -rf $installDir

ls -l $installDir-cleanInstall.tar.gz
tar -tvf $installDir-cleanInstall.tar.gz