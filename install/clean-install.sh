#!/bin/bash

# Carries out a clean install of hithub

if [ "$(id -u)" != "0" ]; then
	echo "Error - must be run as root"
	exit 1
fi

installDir=/opt/hithub
logDir=/var/log/hithub

echo =====================================================
echo Stopping the hithub service
service hithub stop

echo =====================================================
echo Removing any previous installs
rm -rf /opt/hithub*
rm -rf $logDir
rm -f /etc/init.d/hithub

echo =====================================================
echo Creating required directories
mkdir -p $installDir
mkdir -p $logDir
touch $logDir/operational.log
touch $logDir/alert.log
chown hithub:hithub $logDir/operational.log $logDir/alert.log

echo =====================================================
echo Copying files..

cp -r bin $installDir

echo =====================================================
echo Installing...
touch /etc/init.d/hithub
cat hithub-init-script-master > /etc/init.d/hithub
chmod 755 /etc/init.d/hithub

rm -f /etc/rc3.d/S70hithub
ln -s /etc/init.d/hithub /etc/rc3.d/S70hithub

chkconfig --add hithub

echo =====================================================
echo Verifying Install...
if [ ! -f $installDir/bin/hithub ]; then echo "Error copying executable"; exit 1; fi
if [ `diff hithub-init-script-master /etc/init.d/hithub | wc -l` != 0 ]; then echo "Error copying startup script"; fi
if [ ! -f /etc/rc3.d/S70hithub ]; then echo "Error changing hithub start order to S70"; fi

echo =====================================================
echo Starting Hithub service

service hithub start

echo =====================================================
echo Installation Complete