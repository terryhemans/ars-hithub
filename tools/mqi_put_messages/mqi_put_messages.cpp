#include <imqi.hpp> // WebSphere MQ MQI (Client)
#ifdef Q_OS_WIN32
#include <Windows.h>
#endif
#include <string>
#include <unistd.h>
#include <iterator>
#include <iostream>
#include <sstream>
#include "config_file.hpp"


using namespace std;


string queueName = "DS008";
string address = "77.68.85.175(1414)";
string channelName = "CHANNEL1";
string managerName = "QM_APPLE";
string username = "hithub";
string password = "hithub";
bool silent = true;


static void usage(const char * chars = 0);

void readConfigFile( string filename );

int handleParameters(int argc, char* argv[]);


int main(int argc, char *argv[])
{

    int start = handleParameters (argc, argv);

    if( ! silent ){
        cout << "address:  " << address << endl;
        cout << "manager:  " << managerName << endl;
        cout << "channel:  " << channelName << endl;
        cout << "queue:    " << queueName << endl;
        cout << "username: " << username << endl;
        cout << "password: " << password << endl;
    }

	const int HeartBeatInterval = 1;
	const int TransportType = MQXPT_TCP;


    //printf("Connecting to MQ...\n");

    ImqQueueManager* mgr = new ImqQueueManager();

    ImqQueue queue;
    queue.setName(queueName.c_str());

    ImqChannel* channel = new ImqChannel();
    channel->setHeartBeatInterval(HeartBeatInterval);
    channel->setTransportType(TransportType);

    channel->setChannelName(channelName.c_str());
    channel->setConnectionName(address.c_str());

    mgr->setName(managerName.c_str());
    mgr->setChannelReference(channel);
    mgr->setAuthenticationType(MQCSP_AUTH_USER_ID_AND_PWD);
    mgr->setUserId(username.c_str());
    mgr->setPassword(password.c_str());

    ImqBoolean result = mgr->connect();

    if (!result)
    {
        int reasonCode = mgr->reasonCode();
        printf("Failed to open test queue reason code %i\n",
            reasonCode);
    }
    else
    {
        // Associate queue with queue manager.
        queue.setConnectionReference(mgr);

        // Open the named message queue for output; exclusive or shared
        // use of the queue is controlled by the queue definition here
        queue.setOpenOptions( MQOO_OUTPUT );
            //MQOO_OUTPUT + MQOO_INQUIRE             );        /* but not if MQM stopping   */
        queue.open();

        /* report reason, if any; stop if failed      */
        if (queue.reasonCode())
        {
            printf("ImqQueue::open ended with reason code %ld\n",
                (long)queue.reasonCode());
        } else if (queue.completionCode() == MQCC_FAILED)
        {
            printf("unable to open queue for input\n");

		} else {

			// Add the messages
			for (int index = start; index < argc; index++)
			{
				ImqMessage testMessage;
				testMessage.useFullBuffer( argv[index] , strlen(argv[index]));
				testMessage.setFormat( MQFMT_STRING );

				// Place the message on the queue, using default put message
				// Options.
				// The queue will be automatically opened with an output option.
				if ( !queue.put( testMessage ) )
				{
					printf("Failed to add message to queue %ld\n",(long)queue.reasonCode());
				}
				else{
					if( ! silent ){
					    cout << "Success!" << endl;
					}
				}
			}

			// Close the source queue (if it was opened)
			if (!queue.close())
			{
				/* report reason, if any     */
				printf("ImqQueue::close failed with reason code %ld\n",
					(long)queue.reasonCode());
			}

			// Disconnect from MQM if not already connected (the
			// ImqQueueManager object handles this situation automatically)
			if (!mgr->disconnect())
			{
				/* report reason, if any     */
				printf("ImqQueueManager::disconnect failed with reason code %ld\n",
					(long)mgr->reasonCode());
			}
		}
    }

	// Tidy up the channel object
	mgr->setChannelReference();
	delete channel;
	delete mgr;

    return 0;
}



//-----------------------------------------------------------------------------

static void usage(const char * chars)
{
    if (chars) printf("\n" "mqi_put_messages error: %s\n", chars);

    cout << "usage: \n";
    cout << "  mqi_put_messages [-QueueName <queueName>] [-Address '<ip(port_nr)>'] raw_td_message   OR\n";
    cout << "  mqi_put_messages [-QueueName <queueName>] [-Address '<ip(port_nr)>'] $(< file)\n";
    cout << "  Default QueueName is Q1\n";
    cout << "  Default Address is 77.68.85.175(1414)\n\n";
    cout << "e.g.\n";
    cout << "  ./mqi_put_messages '<CA_MSG>URCA015501572B20095304</CA_MSG>'\n";
    cout << "  ./mqi_put_messages -QueueName H1 -Address '127.0.0.1(7768)' $(< schema_msgs)\n";

    exit(1);

}

//-----------------------------------------------------------------------------

int handleParameters(int argc, char* argv[])
{

    if (argc < 2) usage();

    bool explicitSilent = false;

    int i = 1;
    while ( ( i < argc ) && ( argv[i][0] == '-' ) )
    {

        switch (argv[i][1])
        {

        case 'Q':
            queueName = argv[i + 1];
            i += 2;
            break;

        case 'A':
            address = argv[i + 1];
            i += 2;
            break;

        case 'S':
            silent = true;
            explicitSilent = true;
            i += 1;
            break;

        case 'c':
            try{
                readConfigFile( argv[i + 1] );
            }
            catch( std::exception &e  ){
                cout << e.what() << endl;
                exit(1);
            };
            i += 2;
            if( ! explicitSilent ) silent = false;
            break;

        default:
            usage("Unknown option");
        }
    }

    return i;

}

void readConfigFile( string filename ){
    try{

        // Read and extract the configuration data
        ConfigFile configFile(filename, "=");

        address = configFile.getValue("linx.connection");
        managerName = configFile.getValue("linx.manager");
        username = configFile.getValue("linx.username");
        password = configFile.getValue("linx.password");
        channelName = configFile.getValue("linx.channel");

    }
    catch( FileReadException &e )
    {
        throw HubException( std::string("Error reading config file: ") + e.what(), false);
    }
    catch( InvalidConfigDataException &e )
    {
        throw HubException( std::string("config file is invalid: ") +  e.what(), false);
    }
    catch( ConfigFile::MissingKeyException &e )
    {
        throw HubException( std::string("Field '") + e.what() + "' is missing from hithub.cfg", false);
    }
    catch( std::exception &e )
    {
        throw HubException( std::string("Error parsing config file: ") + e.what(), false);
    }
}

//-----------------------------------------------------------------------------
