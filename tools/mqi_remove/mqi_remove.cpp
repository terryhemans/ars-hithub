#include <imqi.hpp> // WebSphere MQ MQI (Client)
#ifdef Q_OS_WIN32
#include <Windows.h>
#endif
#include <string>
#include <unistd.h>
#include <iterator>
#include <iostream>
#include <sstream>

using namespace std;

static void usage(const char * chars = 0);
int handleParameters(int argc, char* argv[], std::string &queueName , std::string &address);


int main(int argc, char *argv[])
{

    string queueName = "";
    string address = "";
    int numberToRemove = handleParameters (argc, argv, queueName, address);


    if (queueName == "")
    {
        queueName = "Q1";
    }

    if ( address == "" )
    {
        address = "77.68.85.175(1414)";
    }

        
	const int HeartBeatInterval = 1;
	const int TransportType = MQXPT_TCP;
	static const char channelName[] = "CHANNEL1";
	static const char queueManagerName[] = "QM_APPLE";
	static const char UserId[] = "hithub";
	static const char Password[] = "hithub";


    //printf("Connecting to MQ...\n");

    ImqQueueManager* mgr = new ImqQueueManager();

    ImqQueue queue;
    queue.setName(queueName.c_str());

    ImqChannel* channel = new ImqChannel();
    channel->setHeartBeatInterval(HeartBeatInterval);
    channel->setTransportType(TransportType);

    channel->setChannelName(channelName);
    channel->setConnectionName(address.c_str());

    mgr->setName(queueManagerName);
    mgr->setChannelReference(channel);
    mgr->setAuthenticationType(MQCSP_AUTH_USER_ID_AND_PWD);
    mgr->setUserId(UserId);
    mgr->setPassword(Password);

    ImqBoolean result = mgr->connect();

    long int queueDepth = 0;

    if (!result)
    {
        int reasonCode = mgr->reasonCode();
        printf("Failed to open queue (%s) reason code %i\n", queueName.c_str(),
            reasonCode);
    }
    else
    {
        // Associate queue with queue manager.
        queue.setConnectionReference(mgr);

        // Open the named message queue for input; exclusive or shared
        // use of the queue is controlled by the queue definition here
        queue.setOpenOptions( MQOO_INPUT_SHARED + MQOO_INQUIRE             );        /* but not if MQM stopping   */
        queue.open();

        /* report reason, if any; stop if failed      */
        if (queue.reasonCode())
        {
            printf("ImqQueue::open queue (%s) failed with reason code %ld\n", queueName.c_str(),
                (long)queue.reasonCode());
        } else if (queue.completionCode() == MQCC_FAILED)
        {
            printf("unable to open queue (%s) for input\n", queueName.c_str());
            
        } else {
            queueDepth = queue.currentDepth();
            //printf("Queue (%s) Depth = %ld\n", queueName, queueDepth);
            
            if (queueDepth < 1) {
                printf("queue (%s) is empty!\n", queueName.c_str());
                
            } else {
                do {
                    ImqMessage imqMsg;

                    // get the next message in an empty buffer, up to Max_Message_Length bytes
                    const int maxMessageLength = 100*1000;
                    static char buffer[maxMessageLength+1];
                    imqMsg.useEmptyBuffer(buffer, maxMessageLength);
                    queue.get(imqMsg);

                    // Handle the result depending on the completion code
                    if ( queue.completionCode() != MQCC_OK) {
                        printf("ImqQueue::get (%s) failed with reason code %d reason code %ld\n", 
                            queueName.c_str(), queue.completionCode(), (long)queue.reasonCode());
                        
                    } else {
                        buffer[imqMsg.dataLength()] = 0;  /* add null terminator */
                        std::string msgStr = imqMsg.bufferPointer();
                        printf("(%s) item: %s\n", queueName.c_str(), msgStr.c_str());
                    }
                } while (--numberToRemove > 0);
            }

            // Close the source queue (if it was opened)
            if (!queue.close())
            {
                /* report reason, if any     */
                printf("ImqQueue::close failed with reason code %ld\n",
                    (long)queue.reasonCode());
            }

            // Disconnect from MQM if not already connected (the
            // ImqQueueManager object handles this situation automatically)
            if (!mgr->disconnect())
            {
                /* report reason, if any     */
                printf("ImqQueueManager::disconnect failed with reason code %ld\n",
                    (long)mgr->reasonCode());
            }
        }
    }

    // Tidy up the channel object 
    mgr->setChannelReference();
    delete channel;
    delete mgr;
        
    return queueDepth;
}



//-----------------------------------------------------------------------------

static void usage(const char * chars)
{
    if (chars) printf("\n" "mqi_remove error: %s\n", chars);

    cout << "usage: \n";
    cout << "  mqi_remove [-QueueName <queueName>] [-Address '<ip(port_nr)>'] [-N numberOfItems]]\n";
    cout << "Removes messages from the queue \n";
    cout << "  Default QueueName is Q1\n";
    cout << "  Default Address is 77.68.85.175(1414)\n";
    cout << "  Default numberOfItems is 1\n\n";
    cout << "e.g.\n";
    cout << "  ./mqi_remove -QueueName H1 -Address '127.0.0.1(7768)' -N 3\n";

    exit(1);

}



//-----------------------------------------------------------------------------

int handleParameters(int argc, char* argv[], std::string &queueName , std::string &address)
{
    queueName = "";
    address = "";
    
    int numberToRemove = 1;

    if (argc < 2) usage();

    for (int i = 1; i < argc; i +=2) 
    {
        if (argv[i][0] != '-') usage();

        switch (argv[i][1]) 
        {

        case 'Q':
            queueName = argv[i + 1];
            cout << argv[0] << ": QueueName: " << queueName << endl;
            break;

        case 'A':
            address = argv[i + 1];
            cout << argv[0] << ": Address: " << address << endl;
            break;

        case 'N':
            numberToRemove = atoi(argv[i + 1]);
            cout << argv[0] << ": Number To Remove: " << numberToRemove << endl;
            break;

        default:
            usage("Unknown option");
        }
    }

    return numberToRemove;

}

//-----------------------------------------------------------------------------
