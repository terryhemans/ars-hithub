#include <imqi.hpp> // WebSphere MQ MQI (Client)
#ifdef Q_OS_WIN32
#include <Windows.h>
#endif
#include <string>
#include <unistd.h>
#include <iterator>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    if( argc > 2 ){
        cout << "usage: \n";
        cout << "  mqi_get_depth [queueName]\n";
        cout << "returns the current depth of the queueName (default Q1) queue.\n";
        exit(0);
    }

	const char * queueName = "Q1";
	const int HeartBeatInterval = 1;
	const int TransportType = MQXPT_TCP;
	static const char channelName[] = "CHANNEL1";
	static const char connectionName[] = "77.68.85.175(1414)";
	static const char queueManagerName[] = "QM_APPLE";
	const int AuthenticationType = MQCSP_AUTH_USER_ID_AND_PWD; 
	static const char UserId[] = "hithub";
	static const char Password[] = "hithub";
	
	if (argc == 2) {
		queueName = argv[1];
	}

    //printf("Connecting to MQ...\n");

    ImqQueueManager* mgr = new ImqQueueManager();

    ImqQueue queue;
	queue.setName(queueName);

    ImqChannel* channel = new ImqChannel();
    channel->setHeartBeatInterval(HeartBeatInterval);
    channel->setTransportType(TransportType);

    channel->setChannelName(channelName);
    channel->setConnectionName(connectionName);

    mgr->setName(queueManagerName);
    mgr->setChannelReference(channel);
    mgr->setAuthenticationType(AuthenticationType);
    mgr->setUserId(UserId);
    mgr->setPassword(Password);

    ImqBoolean result = mgr->connect();

    long int queueDepth = 0;

    if (!result)
    {
        int reasonCode = mgr->reasonCode();
        printf("Failed to open test queue (%s) reason code %i\n",
            queueName, reasonCode);
    }
    else
    {
        // Associate queue with queue manager.
        queue.setConnectionReference(mgr);

        // Open the named message queue for input; exclusive or shared
        // use of the queue is controlled by the queue definition here
        queue.setOpenOptions( MQOO_INQUIRE );
        queue.open();

        /* report reason, if any; stop if failed      */
        if (queue.reasonCode())
        {
            printf("ImqQueue::open ended with reason code %ld\n",
                (long)queue.reasonCode());
        } else if (queue.completionCode() == MQCC_FAILED)
        {
            printf("unable to open queue for input\n");
			
        } else {
			queueDepth = queue.currentDepth();
			long long int t = queueDepth;
			std::string s;
			s = std::to_string(t);
			printf("MQ (%s) Queue Depth = %s\n", queueName, s.c_str());

			// Close the source queue (if it was opened)
			if (!queue.close())
			{
				/* report reason, if any     */
				printf("ImqQueue::close failed with reason code %ld\n",
					(long)queue.reasonCode());
			}

			// Disconnect from MQM if not already connected (the
			// ImqQueueManager object handles this situation automatically)
			if (!mgr->disconnect())
			{
				/* report reason, if any     */
				printf("ImqQueueManager::disconnect failed with reason code %ld\n",
					(long)mgr->reasonCode());
			}
		}
    }

	// Tidy up the channel object 
	mgr->setChannelReference();
	delete channel;
	delete mgr;
		
    return queueDepth;
}
