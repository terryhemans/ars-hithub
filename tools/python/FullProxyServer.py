from datetime import datetime
import socket
import sys
import select
import threading
import time
try:
    import queue
except ImportError:
    import Queue as queue

linxLink = ( '77.68.85.175', 1414) 
tranistaLink = ( '77.68.85.175', 1521)
ctrl_server_port_offset = 4
linkNames = ['Linx Primary', 'Linx Secondary', 'Tranista Primary', 'Tranista Secondary']
delay = 0.01
logFile = open("PythonLog.txt", "w+")

"""
Port offsets from Baseport and index in proxyLinks List:
0 - Linx Primary
1 - Linx Secondary
2 - Tranista Primary
3 - Tranista Secondary
"""

def log(text):
    print(text)
    logFile.write(str(datetime.now()) + '\t' + text + '\n')

class ControlServer:
    def __init__(self):
        log(">>> Initialising Proxy Control Server")
        self.HOST = ''
        self.BASEPORT = 0
        self.buffer_size = 1024 * 48
        self.defaultTimeout = 180
        self.proxyLinks = [None] * 4
        self.isAvailable = True
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.settimeout(self.defaultTimeout)
        self.server.setblocking(True)
        log(">>> Proxy Control Server Initialised")

    def checkCommandLineInput(self, cmdArgs):
        if (len(cmdArgs) < 2):
            log(">>> Error. No Port Number Provided")
            return False, ""
        elif (len(cmdArgs) > 2):
            log(">>> Error. Too Many Arguments")
            return False, ""
        else:
            try:
                result = int(cmdArgs[1])
                if result in range(10000, 10055):
                    return True, result
                else:
                    log('>>> Error. Ports out of range.\n    All port numbers must be between 10000 and 10059.')
                    return False, ""
            except ValueError:
                log(">>> Error. Port Number passed is not an integer")
                return False, ""

    def runServer(self, args):
        log(">>> Running Proxy Control Server")
        cmdCheck, self.BASEPORT = self.checkCommandLineInput(args)        
        if (cmdCheck):
            try:
                self.server.bind((self.HOST, self.BASEPORT + ctrl_server_port_offset))
                self.server.listen(5)
                log('>>> Control Server Listening on Port {}'.format(self.BASEPORT + ctrl_server_port_offset))
                log('>>> Awaiting Client Connection...')
                while self.isAvailable:
                    try:
                        conn, addr = self.server.accept()
                        log('>>> Connected to {}'.format(addr))
                        while self.isAvailable:
                            data = conn.recv(self.buffer_size)
                            dataString = data.decode('utf-8')
                            if (dataString[0:4].upper() == "QUIT"):
                                conn.sendall("Quit")
                                conn.close()
                                self.isAvailable = False
                            elif (dataString == "Hello" or dataString == "Hello\n"):
                                conn.sendall('Hi'.encode('utf-8'))
                            elif (dataString == "Hello World!" or dataString == "Hello World!\n"):
                                conn.sendall(data)
                            elif data:
                                message = self.onReceive(dataString)
                                conn.sendall(message.encode('utf-8'))
                            else:
                                conn.close()
                                self.closeControlServer()                            
                    except socket.error as e:
                        if (str(e) == '[Errno 104] Connection reset by peer'):
                            log('>>> Client Disconnected')
                            log('>>> Control Server Listening on Port {}'.format(self.BASEPORT + ctrl_server_port_offset))
                            log('>>> Awaiting Client Connection...')
                        elif (str(e) == 'timed out'):
                            log('>>> Socket Timeout')
                            self.closeControlServer()
                        else:
                            raise e
                self.closeControlServer()
            except KeyboardInterrupt:
                self.closeControlServer()
            except Exception as e:
                log('>>> Error: {}'.format(e))
                self.closeControlServer()

    def onReceive(self, command):
        """
            Acceptable commands to the Control Server:
            L1_ON - Turn on Primary Linx connection
            L1_OFF - Turn off Primary Linx connection
            L2_ON - Turn on Secondary Linx connection
            L2_OFF - Turn off Secondary Linx connection
            T1_ON - Turn on Primary Tranista connection
            T1_OFF - Turn off Primary Tranista connection
            T2_ON - Turn on Secondary Tranista connection
            T2_OFF - Turn off Secondary Tranista connection
            [Link Preface]_I - Check Availability
        """
        try:
            proxyIndex = 50
            if (command[0] == 'L'):
                proxyIndex = 0
            elif (command[0] == 'T'):
                proxyIndex = 2
            if (command[1] == '2'):
                proxyIndex += 1
            if (proxyIndex < 50):
                if (command[3:5] == 'ON'):
                    self.switchOn(proxyIndex)
                    message = str(self.checkAvailable(proxyIndex))
                elif (command[3:6] == 'OFF'):
                    self.switchOff(proxyIndex)
                    message = str(self.checkAvailable(proxyIndex))
                elif (command[3] == 'I'):
                    message = str(self.checkAvailable(proxyIndex))
                else:
                    message = 'Unrecognised Command to Control Server'
            else:
                message = 'Unrecognised Command to Control Server'            
        except IndexError:
            message = 'Unrecognised Command to Control Server'
        return message
    
    def switchOn(self, index):
        try:
            if self.proxyLinks[index] is None: # No ProxyServer object therefore start new
                log('>>> Starting Proxy Server Index: {}'.format(index))
                if index < 2:
                    self.proxyLinks[index] = ProxyServer(self.HOST, self.BASEPORT + index, linxLink, linkNames[index])                    
                else:
                    self.proxyLinks[index] = ProxyServer(self.HOST, self.BASEPORT + index, tranistaLink, linkNames[index])                
                self.proxyLinks[index].start()
                #time.sleep(10 * delay) # Give time for ProxyServer to start up
            else: # ProxyServer object is running                    
                if (self.proxyLinks[index].isAvailable == False):
                    log('>>> Switching on index: {}'.format(index))
                    self.proxyLinks[index].isAvailable = True                    
        except Exception as e:
            log('>>> Error Switching on ProxyServer Index {}: {}'.format(index,e))

    def switchOff(self, index):
        if self.proxyLinks[index] is not None: # If index is None there is no Server which is what we want
            if (self.proxyLinks[index].isAvailable == True):
                log('>>> Switching off index: {}'.format(index))
                self.closeProxyServer(index)
            else:
                log('>>> Index already off: {}'.format(index))
        else:
            log('>>> Index already off: {}'.format(index))

    def checkAvailable(self, index):
        available = False
        if self.proxyLinks[index] is not None:
            available = self.proxyLinks[index].isAvailable
        return available

    def closeControlServer(self):
        log('>>> Closing Proxy Control Server...')
        for p in range(len(self.proxyLinks)):
            self.closeProxyServer(p)
        self.isAvailable = False
        self.server.close()
        log('>>> Proxy Control Server Closed')

    def closeProxyServer(self, proxyID):
        log('>>> Closing Proxy Server {}...'.format(proxyID))
        if (self.proxyLinks[proxyID] is not None):                                                                
            self.proxyLinks[proxyID].isAvailable = False
            time.sleep(10 * delay) # Needed so that the loop containing the select statement can break
            for i in self.proxyLinks[proxyID].inputs:
                i.close()
            for i in self.proxyLinks[proxyID].outputs:
                i.close()
            if self.proxyLinks[proxyID].forwardPort:
                self.proxyLinks[proxyID].forwardPort.close()
            self.proxyLinks[proxyID].inputs = []
            self.proxyLinks[proxyID].join()
            self.proxyLinks[proxyID] = None
        log('>>> Proxy Server Closed {}'.format(proxyID))    


class ProxyServer(threading.Thread):
    def __init__(self, host, port, forwardAddress, name):
        super(ProxyServer, self).__init__(name = name)
        self.HOST = host
        self.PORT = port
        self.buffer_size = 1024 * 48
        self.inputs = []
        self.outputs = []                       
        self.messageQueues = {}
        self.forwardAddress = forwardAddress
        self.serverName = name
        self.forwardPort = False
        self.hithubPort = False
        self.isAvailable = True # This variable is changed via the Control Server to simulate lost connections          

    def run(self):        
        try:
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server.setblocking(0)
            server.bind((self.HOST, self.PORT))            
            server.listen(5)
            self.inputs.append(server)                
            log('\t>>> {} Listening on Port: {}'.format(self.serverName, self.PORT))
            log('\t>>> {} Awaiting Client Connection...'.format(self.serverName))
            while self.inputs:                
                while self.isAvailable:                    
                    readable, writable, e = select.select(self.inputs, self.outputs, [], delay)
                    
                    for s in writable:                        
                        self.onSend(s)

                    for s in readable:
                        if s is server: # When a new client attempts to connect
                            self.onConnection(s)
                        else: # When data or client disconnect is received                                
                            self.onReceive(s)                    
            self.closeServer
        except KeyboardInterrupt:
            log('\t>>> {} Shutting Down Server'.format(self.serverName))
            self.closeServer
        except Exception as e:
            log('\t>>> {} Main loop error: {}'.format(self.serverName,e))
            self.closeServer

    def onConnection(self, s):
        try:
            self.hithubPort, addr = s.accept()
            self.hithubPort.setblocking(0)                    
            self.inputs.append(self.hithubPort)
            self.messageQueues[self.hithubPort] = queue.Queue()
            log('\t>>> {} Connected to {}'.format(self.serverName, addr))        
            if not self.forwardPort:
                self.forwardConnect(self.forwardAddress)                
        except Exception as e:
            log('\t>>> {} onConnection error: {}'.format(self.serverName, e))
            raise e

    def onReceive(self, s):
        try:
            data = s.recv(self.buffer_size)
            #dataString = data.decode('utf-8')
            if data:                                                        
                try:
                    if s == self.hithubPort: # If received from Hithub then place in message queue for forwardPort
                        if not self.forwardPort: # If forwardPort is False it means that either the initial connection or subsequent reconnections failed. Therefore Retry.
                            self.forwardConnect(self.forwardAddress)                                    
                                            
                        if self.forwardPort: # Necessary to add forwardPort to messageQueues and outputs if not False and not already present.
                            if self.forwardPort not in self.messageQueues:
                                self.messageQueues[self.forwardPort] = queue.Queue()
                
                            self.messageQueues[self.forwardPort].put(data)
                    
                            if self.forwardPort not in self.outputs:
                                self.outputs.append(self.forwardPort)
                
                    else: # Received from ForwardPort therefore place in queue for hithubPort
                        self.messageQueues[self.hithubPort].put(data) # Put message data to be echoed back into queue
                        if self.hithubPort not in self.outputs:
                            self.outputs.append(self.hithubPort)
                except Exception as e:
                    log('\t>>> {} onReceive when data error: {}'.format(self.serverName, e))
                    raise e
            else: # Disconnect
                try:
                    if s in self.outputs:
                        self.outputs.remove(s)
                    self.inputs.remove(s)                
                    s.close()                
                    del self.messageQueues[s] 
                    if s == self.forwardPort:
                        self.forwardPort = False                 

                    if s == self.hithubPort:
                        log('\t>>> {} HitHub Disconnected'.format(self.serverName))
                    else:
                        log('\t>>> {} Linx/Tranista Disconnected'.format(self.serverName))
                except Exception as e:
                    log('\t>>> {} onReceive when disconnect error: {}'.format(self.serverName, e))
                    raise e
        except Exception as e:
            log('\t>>> {} onReceive error: {}'.format(self.serverName, e))
            raise e
        
    def onSend(self, s):
        try:
            msg = self.messageQueues[s].get_nowait()
        except queue.Empty:
            self.outputs.remove(s)                            
        except KeyError:
            self.outputs.remove(s)
        else:
            try:
                s.sendall(msg)
                """if s is self.hithubPort:                    
                    log('\t>>> {} Data sent to HitHub: '.format(self.serverName, msg))
                else:
                    log('\t>>> {} Data sent to Linx/Tranista: '.format(self.serverName, msg))"""
            except socket.error as ex:
                if s is self.forwardPort: # Means that connection to the forwarding port has been lost
                    log('\t>>> {} Error: Connection to Forwarding Port Lost'.format(self.serverName))
                    log('\t>>> {} Attempting Reconnect...'.format(self.serverName))
                    del self.messageQueues[s] # Necessary to ensure that the dict doesn't get too large with repeated disconnects
                    self.forwardPort = self.forwardConnect(self.forwardAddress) # Attempt to reconnect                    
            except Exception as ex:
                log('\t>>> {} onSend error: {}'.format(self.serverName, ex))
                raise ex

    def forwardConnect(self, addr):
        try:
            self.forwardPort = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.forwardPort.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.forwardPort.connect(addr)
            self.inputs.append(self.forwardPort)
            self.messageQueues[self.forwardPort] = queue.Queue()
            log('\t>>> {} Proxy Server connected to Forwarding Port {}'.format(self.serverName, self.forwardAddress))                        
        except Exception as e:
            log('\t>>> {} Error: Unable to connect to forwarding port'.format(self.serverName))
            log('\t    {} {}'.format(self.serverName, e))
            return False

    def closeServer(self):                                            
        log('\t>>> Closing Proxy Server: {}...'.format(self.serverName))
        for i in self.inputs:
            i.close()
        self.inputs = []
        for o in self.outputs:
            o.close()
        self.outputs = []
        self.messageQueues.clear()
        self.isAvailable = False         

Server = ControlServer().runServer(sys.argv)
logFile.close()