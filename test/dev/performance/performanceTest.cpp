#include <iostream>
#include <sys/wait.h>

#include <chrono>
#include <time.h>

#include "TestScript.hpp"

using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION( class performanceTests );

class performanceTests : public TestScript
{
	CPPUNIT_TEST_SUITE( performanceTests );
	
	TEST( setupScript );
	TEST( performanceTest );
	TEST( cleanup );
	
	CPPUNIT_TEST_SUITE_END();
	
public:
		
	void setupScript()
	{
		linx.clearAllQueues();
		assertEqualMsg("Linx queues not empty", linx.sizeAllQueues(), 0);
		
		string S009message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PathDetailsMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>2003</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d9262cd303</MessageIdentifier><MessageDateTime>2017-06-29T08:59:42-00:00</MessageDateTime></MessageReference><SenderReference>9M21BF+s9SBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">0070</Recipient></MessageHeader><AdministrativeContactInformation><Name>Network Rail</Name></AdministrativeContactInformation><Identifiers><PlannedTransportIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--879M21MH29</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></PlannedTransportIdentifiers><PlannedTransportIdentifiers><ObjectType>PA</ObjectType><Company>0070</Company><Core>9M21L6796010</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></PlannedTransportIdentifiers></Identifiers><MessageStatus>1</MessageStatus><TypeOfRequest>3</TypeOfRequest><TypeOfInformation>50</TypeOfInformation><PathInformation><PlannedJourneyLocation JourneyLocationTypeCode=\"01\"><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>55950</LocationPrimaryCode><LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">CLPHMJN</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>10:59:00</Time><Offset>0</Offset></Timing></TimingAtLocation><ResponsibleRU>9930</ResponsibleRU><ResponsibleIM>0070</ResponsibleIM><TrainActivity><TrainActivityType>GBTB</TrainActivityType></TrainActivity><OperationalTrainNumber>9M21</OperationalTrainNumber><NetworkSpecificParameter><Name>ASM</Name><Value>0</Value></NetworkSpecificParameter><NetworkSpecificParameter><Name>UID</Name><Value>L67960</Value></NetworkSpecificParameter><NetworkSpecificParameter><Name>SER</Name><Value>22206000</Value></NetworkSpecificParameter></PlannedJourneyLocation><PlannedJourneyLocation JourneyLocationTypeCode=\"01\"><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>99999</LocationPrimaryCode></PlannedJourneyLocation><PlannedCalendar><BitmapDays>1</BitmapDays><ValidityPeriod><StartDateTime>2017-06-29T10:59:00-00:00</StartDateTime></ValidityPeriod></PlannedCalendar></PathInformation></PathDetailsMessage>";
		string S013message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d9262b6082</MessageIdentifier><MessageDateTime>2017-06-29T08:56:36-00:00</MessageDateTime></MessageReference><SenderReference>5C00Y6EkFRBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">9999</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--735C00MS28</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-28</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>5C00</OperationalTrainNumber></OperationalTrainNumberIdentifier><TrainJourneyModification><TrainJourneyModificationIndicator>91</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>30951</LocationPrimaryCode><LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">ROYAOSD</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification></Location><ModificationStatusIndicator>91</ModificationStatusIndicator><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>16:04:00</Time><Offset>0</Offset></Timing></TimingAtLocation></LocationModified></TrainJourneyModification><ModificationReason NationalDelayCode=\"OU\">39</ModificationReason><TrainJourneyModificationTime>2017-06-29T09:56:00-00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
		string S015message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d926106d13</MessageIdentifier><MessageDateTime>2017-06-29T08:01:57-00:00</MessageDateTime></MessageReference><SenderReference>5C00Y6EkFRBG,7V1872tuASBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">0070</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--736V18CI29</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>7V18</OperationalTrainNumber></OperationalTrainNumberIdentifier><ReferenceOTN><OperationalTrainNumberIdentifier><OperationalTrainNumber>5C00</OperationalTrainNumber></OperationalTrainNumberIdentifier></ReferenceOTN><TrainJourneyModification><TrainJourneyModificationIndicator>07</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>99999</LocationPrimaryCode></Location><ModificationStatusIndicator>07</ModificationStatusIndicator></LocationModified></TrainJourneyModification><TrainJourneyModificationTime>2017-06-29T09:01:55-00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
		
		int noMsgToProcess = 1000;
		
		for(int i = 0; i < noMsgToProcess; i++)
		{
			linx.S009_Queue.push(S009message);
			linx.S013_Queue.push(S013message);
			linx.S015_Queue.push(S015message);
		}
		
		assertEqualMsg("Linx queues not full", linx.sizeAllQueues(), 3 * noMsgToProcess);
	}
	
	void performanceTest()
	{	
		int noMsg = linx.sizeAllQueues();
		
		long long startTime = getEpochMilliSecondNow();
		
		Hub.start();
		
		while (!linx.allQueuesAreEmpty())
		{
			if(!Hub.isRunning())
			{
				assertMsg("Error. Hithub not running. Check logs", false);
			}
		}
		
		long long endTime = getEpochMilliSecondNow();
		Hub.stop();
		
		long long durationTime = endTime - startTime;
		
		int msgPerSecond = noMsg / (durationTime / 1000);
		
		std::cout << endl << msgPerSecond << endl;

		assertMsg("Performance below 40 msg per second", msgPerSecond >= 40);
	}
	
	void cleanup()
	{
		linx.clearAllQueues();
		assertEqualMsg("Linx queues not empty", linx.sizeAllQueues(), 0);
	}
		
private:
	
	long long getEpochMilliSecondNow()
	{
		return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	}
};