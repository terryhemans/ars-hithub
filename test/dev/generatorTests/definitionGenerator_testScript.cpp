#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../../../src/xmlDefinitionGenerator/xmlDefinitionGenerator.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION( class GeneratorDevTests );

class GeneratorDevTests : public CppUnit::TestFixture
{
	public:
		XMLDefinitionGenerator definitionGenerator;
		
		CPPUNIT_TEST_SUITE( GeneratorDevTests );
	
		CPPUNIT_TEST( nameElementNativeType_AirBrakedMass );
		CPPUNIT_TEST( nameElementNativeType_BogiePitch );
		CPPUNIT_TEST( nameElementSimpleDerivedType_ITU_Type );
		CPPUNIT_TEST( nameElementSimpleDerivedType_OriginCountry );
		CPPUNIT_TEST( typeElementNativeType_AcceptanceTimeAtInterchange );
		CPPUNIT_TEST( typeElementNativeType_AdditionalCerification );
		CPPUNIT_TEST( typeElementSimpleType_AdditionalInstruction );
		CPPUNIT_TEST( typeElementSimpleType_Company );
		CPPUNIT_TEST( complexTypeSimpleContent_CountryCodeISO );
		CPPUNIT_TEST( complexTypeSimpleContent_ConsignmentNumber );
		CPPUNIT_TEST( complexTypeSimpleContentWithAttribute_DelayCause );
		CPPUNIT_TEST( complexTypeSimpleContentWithAttribute_LocationSubsidiaryCode );
		CPPUNIT_TEST( complexTypeSequence_Registration );
		CPPUNIT_TEST( complexTypeSequence_RequestedPeriod );
		CPPUNIT_TEST( complexTypeComplexContent_Station );
		CPPUNIT_TEST( complexTypeComplexContentWithSequence_PlannedJourneyLocation );
		CPPUNIT_TEST( s009Message_PathDetailsMessage );
		CPPUNIT_TEST( s013_s015Message_TrainJourneyModificationMessage );

		CPPUNIT_TEST_SUITE_END();
	
		void nameElementNativeType_AirBrakedMass()
		{
			std::string rootNodeName = "AirBrakedMass";
			std::string expectedXML = "<xs:element name=\"AirBrakedMass\" type=\"xs:integer\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void nameElementNativeType_BogiePitch()
		{
			std::string rootNodeName = "BogiePitch";
			std::string expectedXML = "<xs:element name=\"BogiePitch\" type=\"xs:integer\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void nameElementSimpleDerivedType_ITU_Type()
		{
			std::string rootNodeName = "ITU_Type";
			std::string expectedXML = "<xs:element name=\"ITU_Type\" type=\"xs:token\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void nameElementSimpleDerivedType_OriginCountry()
		{
			std::string rootNodeName = "OriginCountry";
			std::string expectedXML = "<xs:element name=\"OriginCountry\" type=\"xs:string\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void typeElementNativeType_AcceptanceTimeAtInterchange()
		{
			std::string rootNodeName = "AcceptanceTimeAtInterchange";
			std::string expectedXML = "<xs:element name=\"AcceptanceTimeAtInterchange\" type=\"xs:dateTime\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void typeElementNativeType_AdditionalCerification()
		{
			std::string rootNodeName = "AdditionalCertification";
			std::string expectedXML = "<xs:element name=\"AdditionalCertification\" type=\"xs:string\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void typeElementSimpleType_AdditionalInstruction()
		{
			std::string rootNodeName = "AdditionalInstruction";
			std::string expectedXML = "<xs:element name=\"AdditionalInstruction\" type=\"xs:string\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void typeElementSimpleType_Company()
		{
			std::string rootNodeName = "Company";
			std::string expectedXML = "<xs:element name=\"Company\" type=\"xs:integer\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSimpleContent_CountryCodeISO()
		{
			std::string rootNodeName = "CountryCodeISO";
			std::string expectedXML = "<xs:element name=\"CountryCodeISO\" type=\"xs:string\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSimpleContent_ConsignmentNumber()
		{
			std::string rootNodeName = "ConsignmentNumber";
			std::string expectedXML = "<xs:element name=\"ConsignmentNumber\" type=\"xs:string\"/>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSimpleContentWithAttribute_DelayCause()
		{
			std::string rootNodeName = "DelayCause";
			std::string expectedXML = "<xs:element name=\"DelayCause\" type=\"xs:token\"><xs:attribute name=\"NationalDelayCode\" type=\"xs:string\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSimpleContentWithAttribute_LocationSubsidiaryCode()
		{
			std::string rootNodeName = "LocationSubsidiaryCode";
			std::string expectedXML = "<xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSequence_Registration()
		{
			std::string rootNodeName = "Registration";
			std::string expectedXML = "<xs:element name=\"Registration\"><xs:element name=\"NotifiedBody\" type=\"xs:integer\"/><xs:element name=\"RegistrationDate\" type=\"xs:date\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeSequence_RequestedPeriod()
		{
			std::string rootNodeName = "RequestedPeriod";
			std::string expectedXML = "<xs:element name=\"RequestedPeriod\"><xs:element name=\"StartDateTime\" type=\"xs:dateTime\"/><xs:element name=\"EndDateTime\" type=\"xs:dateTime\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeComplexContent_Station()
		{
			std::string rootNodeName = "Station";
			std::string expectedXML = "<xs:element name=\"Station\"><xs:element name=\"CountryCodeISO\" type=\"xs:string\"/><xs:element name=\"LocationPrimaryCode\" type=\"xs:positiveInteger\"/><xs:element name=\"PrimaryLocationName\" type=\"xs:string\"/><xs:element name=\"LocationSubsidiaryIdentification\"><xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"AllocationCompany\" type=\"xs:integer\"/><xs:element name=\"LocationSubsidiaryName\" type=\"xs:string\"/></xs:element></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void complexTypeComplexContentWithSequence_PlannedJourneyLocation()
		{
			std::string rootNodeName = "PlannedJourneyLocation";
			std::string expectedXML = "<xs:element name=\"PlannedJourneyLocation\"><xs:element name=\"CountryCodeISO\" type=\"xs:string\"/><xs:element name=\"LocationPrimaryCode\" type=\"xs:positiveInteger\"/><xs:element name=\"PrimaryLocationName\" type=\"xs:string\"/><xs:element name=\"LocationSubsidiaryIdentification\"><xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"AllocationCompany\" type=\"xs:integer\"/><xs:element name=\"LocationSubsidiaryName\" type=\"xs:string\"/></xs:element><xs:element name=\"TimingAtLocation\"><xs:element name=\"Timing\" maxOccurs=\"unbounded\"><xs:element name=\"Time\" type=\"xs:time\"/><xs:element name=\"Offset\" type=\"xs:integer\"/><xs:attribute name=\"TimingQualifierCode\" type=\"xs:token\"/></xs:element><xs:element name=\"DwellTime\" type=\"xs:decimal\"/></xs:element><xs:element name=\"FreeTextField\" type=\"xs:string\" maxOccurs=\"unbounded\"/><xs:element name=\"ResponsibleApplicant\" type=\"xs:integer\"/><xs:element name=\"ResponsibleRU\" type=\"xs:integer\"/><xs:element name=\"ResponsibleIM\" type=\"xs:integer\"/><xs:element name=\"PlannedTrainData\"><xs:element name=\"TrainType\" type=\"xs:integer\"/><xs:element name=\"TrafficType\" type=\"xs:string\"/><xs:element name=\"TypeofService\"><xs:element name=\"SpecialServiceDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/><xs:element name=\"FacilityTypeDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/><xs:element name=\"CharacteristicDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/></xs:element><xs:element name=\"CommercialTrafficType\" type=\"xs:integer\"/><xs:element name=\"PlannedTrainTechnicalData\"><xs:element name=\"TrainWeight\" type=\"xs:int\"/><xs:element name=\"TrainLength\" type=\"xs:integer\"/><xs:element name=\"WeightOfSetOfCarriages\" type=\"xs:int\"/><xs:element name=\"LengthOfSetOfCarriages\" type=\"xs:integer\"/><xs:element name=\"TractionDetails\" maxOccurs=\"unbounded\"><xs:element name=\"LocoTypeNumber\" type=\"xs:string\"/><xs:element name=\"TractionMode\" type=\"xs:integer\"/><xs:element name=\"TrainCC_System\" type=\"xs:token\"/><xs:element name=\"TrainRadioSystem\" type=\"xs:token\"/><xs:element name=\"TractionWeight\" type=\"xs:int\"/><xs:element name=\"Length\"><xs:element name=\"Value\" type=\"xs:decimal\"/><xs:element name=\"Measure\" type=\"xs:token\"/></xs:element></xs:element><xs:element name=\"TrainMaxSpeed\" type=\"xs:integer\"/><xs:element name=\"HighestPlannedSpeed\" type=\"xs:integer\"/><xs:element name=\"MaxAxleWeight\" type=\"xs:int\"/><xs:element name=\"RouteClass\" type=\"xs:string\"/><xs:element name=\"BrakeType\" type=\"xs:token\"/><xs:element name=\"EmergencyBrakeOverride\" type=\"xs:boolean\"/><xs:element name=\"BrakingRatio\" type=\"xs:integer\"/><xs:element name=\"MinBrakedWeightPercent\" type=\"xs:integer\"/><xs:element name=\"BrakeWeight\" type=\"xs:int\"/></xs:element><xs:element name=\"ExceptionalGaugingIdent\" maxOccurs=\"unbounded\"><xs:element name=\"IM_Partner\" type=\"xs:integer\"/><xs:element name=\"ExceptionalGaugingCode\" type=\"xs:string\"/></xs:element><xs:element name=\"DangerousGoodsIndication\" maxOccurs=\"unbounded\"><xs:element name=\"HazardIdentificationNumber\" type=\"xs:string\"/><xs:element name=\"UN_Number\" type=\"xs:string\"/><xs:element name=\"DangerLabel\" type=\"xs:token\" maxOccurs=\"5\"/><xs:element name=\"RID_Class\" type=\"xs:string\"/><xs:element name=\"PackingGroup\" type=\"xs:token\"/><xs:element name=\"DangerousGoodsWeight\" type=\"xs:integer\"/><xs:element name=\"DangerousGoodsVolume\" type=\"xs:float\"/><xs:element name=\"LimitedQuantityIndicator\" type=\"xs:boolean\"/></xs:element><xs:element name=\"CombinedTrafficLoadProfile\"><xs:element name=\"P1\" type=\"xs:integer\"/><xs:element name=\"P2\" type=\"xs:integer\"/><xs:element name=\"C1\" type=\"xs:integer\"/><xs:element name=\"C2\" type=\"xs:integer\"/></xs:element></xs:element><xs:element name=\"StatusOfHarmonization\"><xs:element name=\"HandoverHarmonized\" type=\"xs:boolean\"/><xs:element name=\"InterchangeHarmonized\" type=\"xs:boolean\"/></xs:element><xs:element name=\"TrainActivity\" maxOccurs=\"unbounded\"><xs:element name=\"TrainActivityType\" type=\"xs:string\"/><xs:element name=\"AssociatedAttachedTrainID\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element><xs:element name=\"AssociatedAttachedOTN\" type=\"xs:string\"/></xs:element><xs:element name=\"OnDemandPath\" type=\"xs:boolean\"/><xs:element name=\"PreArrangedPath\" type=\"xs:string\"/><xs:element name=\"OperationalTrainNumber\" type=\"xs:string\"/><xs:element name=\"NetworkSpecificParameter\" maxOccurs=\"unbounded\"><xs:element name=\"Name\" type=\"xs:string\"/><xs:element name=\"Value\" type=\"xs:string\"/></xs:element><xs:attribute name=\"JourneyLocationTypeCode\" type=\"xs:token\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void s009Message_PathDetailsMessage()
		{
			std::string rootNodeName = "PathDetailsMessage";
			std::string expectedXML = "<xs:element name=\"PathDetailsMessage\"><xs:element name=\"MessageHeader\"><xs:element name=\"MessageReference\"><xs:element name=\"MessageType\" type=\"xs:integer\"/><xs:element name=\"MessageTypeVersion\" type=\"xs:string\"/><xs:element name=\"MessageIdentifier\" type=\"xs:string\"/><xs:element name=\"MessageDateTime\" type=\"xs:dateTime\"/></xs:element><xs:element name=\"MessageRoutingID\" type=\"xs:integer\"/><xs:element name=\"SenderReference\" type=\"xs:string\"/><xs:element name=\"Sender\" type=\"xs:integer\"><xs:attribute name=\"CI_InstanceNumber\" type=\"xs:integer\"/></xs:element><xs:element name=\"Recipient\" type=\"xs:integer\"><xs:attribute name=\"CI_InstanceNumber\" type=\"xs:integer\"/></xs:element></xs:element><xs:element name=\"AdministrativeContactInformation\"><xs:element name=\"Name\" type=\"xs:string\"/><xs:element name=\"Address\" type=\"xs:string\"/><xs:element name=\"eMail\" type=\"xs:string\"/><xs:element name=\"PhoneNumber\" type=\"xs:string\"/><xs:element name=\"FaxNumber\" type=\"xs:string\"/><xs:element name=\"FreeTextField\" type=\"xs:string\"/></xs:element><xs:element name=\"Identifiers\"><xs:element name=\"PlannedTransportIdentifiers\" maxOccurs=\"unbounded\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element><xs:element name=\"RelatedPlannedTransportIdentifiers\" maxOccurs=\"unbounded\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element></xs:element><xs:element name=\"MessageStatus\" type=\"xs:token\"/><xs:element name=\"TypeOfRUHarmonization\" type=\"xs:string\"/><xs:element name=\"TypeOfIMHarmonization\" type=\"xs:string\"/><xs:element name=\"CoordinatingIM\" type=\"xs:integer\"/><xs:element name=\"LeadRU\" type=\"xs:integer\"/><xs:element name=\"TypeOfRequest\" type=\"xs:short\"/><xs:element name=\"TypeOfInformation\" type=\"xs:integer\"/><xs:element name=\"PathInformation\"><xs:element name=\"PlannedJourneyLocation\" maxOccurs=\"unbounded\"><xs:element name=\"CountryCodeISO\" type=\"xs:string\"/><xs:element name=\"LocationPrimaryCode\" type=\"xs:positiveInteger\"/><xs:element name=\"PrimaryLocationName\" type=\"xs:string\"/><xs:element name=\"LocationSubsidiaryIdentification\"><xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"AllocationCompany\" type=\"xs:integer\"/><xs:element name=\"LocationSubsidiaryName\" type=\"xs:string\"/></xs:element><xs:element name=\"TimingAtLocation\"><xs:element name=\"Timing\" maxOccurs=\"unbounded\"><xs:element name=\"Time\" type=\"xs:time\"/><xs:element name=\"Offset\" type=\"xs:integer\"/><xs:attribute name=\"TimingQualifierCode\" type=\"xs:token\"/></xs:element><xs:element name=\"DwellTime\" type=\"xs:decimal\"/></xs:element><xs:element name=\"FreeTextField\" type=\"xs:string\" maxOccurs=\"unbounded\"/><xs:element name=\"ResponsibleApplicant\" type=\"xs:integer\"/><xs:element name=\"ResponsibleRU\" type=\"xs:integer\"/><xs:element name=\"ResponsibleIM\" type=\"xs:integer\"/><xs:element name=\"PlannedTrainData\"><xs:element name=\"TrainType\" type=\"xs:integer\"/><xs:element name=\"TrafficType\" type=\"xs:string\"/><xs:element name=\"TypeofService\"><xs:element name=\"SpecialServiceDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/><xs:element name=\"FacilityTypeDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/><xs:element name=\"CharacteristicDescriptionCode\" type=\"xs:integer\" maxOccurs=\"unbounded\"/></xs:element><xs:element name=\"CommercialTrafficType\" type=\"xs:integer\"/><xs:element name=\"PlannedTrainTechnicalData\"><xs:element name=\"TrainWeight\" type=\"xs:int\"/><xs:element name=\"TrainLength\" type=\"xs:integer\"/><xs:element name=\"WeightOfSetOfCarriages\" type=\"xs:int\"/><xs:element name=\"LengthOfSetOfCarriages\" type=\"xs:integer\"/><xs:element name=\"TractionDetails\" maxOccurs=\"unbounded\"><xs:element name=\"LocoTypeNumber\" type=\"xs:string\"/><xs:element name=\"TractionMode\" type=\"xs:integer\"/><xs:element name=\"TrainCC_System\" type=\"xs:token\"/><xs:element name=\"TrainRadioSystem\" type=\"xs:token\"/><xs:element name=\"TractionWeight\" type=\"xs:int\"/><xs:element name=\"Length\"><xs:element name=\"Value\" type=\"xs:decimal\"/><xs:element name=\"Measure\" type=\"xs:token\"/></xs:element></xs:element><xs:element name=\"TrainMaxSpeed\" type=\"xs:integer\"/><xs:element name=\"HighestPlannedSpeed\" type=\"xs:integer\"/><xs:element name=\"MaxAxleWeight\" type=\"xs:int\"/><xs:element name=\"RouteClass\" type=\"xs:string\"/><xs:element name=\"BrakeType\" type=\"xs:token\"/><xs:element name=\"EmergencyBrakeOverride\" type=\"xs:boolean\"/><xs:element name=\"BrakingRatio\" type=\"xs:integer\"/><xs:element name=\"MinBrakedWeightPercent\" type=\"xs:integer\"/><xs:element name=\"BrakeWeight\" type=\"xs:int\"/></xs:element><xs:element name=\"ExceptionalGaugingIdent\" maxOccurs=\"unbounded\"><xs:element name=\"IM_Partner\" type=\"xs:integer\"/><xs:element name=\"ExceptionalGaugingCode\" type=\"xs:string\"/></xs:element><xs:element name=\"DangerousGoodsIndication\" maxOccurs=\"unbounded\"><xs:element name=\"HazardIdentificationNumber\" type=\"xs:string\"/><xs:element name=\"UN_Number\" type=\"xs:string\"/><xs:element name=\"DangerLabel\" type=\"xs:token\" maxOccurs=\"5\"/><xs:element name=\"RID_Class\" type=\"xs:string\"/><xs:element name=\"PackingGroup\" type=\"xs:token\"/><xs:element name=\"DangerousGoodsWeight\" type=\"xs:integer\"/><xs:element name=\"DangerousGoodsVolume\" type=\"xs:float\"/><xs:element name=\"LimitedQuantityIndicator\" type=\"xs:boolean\"/></xs:element><xs:element name=\"CombinedTrafficLoadProfile\"><xs:element name=\"P1\" type=\"xs:integer\"/><xs:element name=\"P2\" type=\"xs:integer\"/><xs:element name=\"C1\" type=\"xs:integer\"/><xs:element name=\"C2\" type=\"xs:integer\"/></xs:element></xs:element><xs:element name=\"StatusOfHarmonization\"><xs:element name=\"HandoverHarmonized\" type=\"xs:boolean\"/><xs:element name=\"InterchangeHarmonized\" type=\"xs:boolean\"/></xs:element><xs:element name=\"TrainActivity\" maxOccurs=\"unbounded\"><xs:element name=\"TrainActivityType\" type=\"xs:string\"/><xs:element name=\"AssociatedAttachedTrainID\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element><xs:element name=\"AssociatedAttachedOTN\" type=\"xs:string\"/></xs:element><xs:element name=\"OnDemandPath\" type=\"xs:boolean\"/><xs:element name=\"PreArrangedPath\" type=\"xs:string\"/><xs:element name=\"OperationalTrainNumber\" type=\"xs:string\"/><xs:element name=\"NetworkSpecificParameter\" maxOccurs=\"unbounded\"><xs:element name=\"Name\" type=\"xs:string\"/><xs:element name=\"Value\" type=\"xs:string\"/></xs:element><xs:attribute name=\"JourneyLocationTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"PlannedCalendar\"><xs:element name=\"BitmapDays\" type=\"xs:string\"/><xs:element name=\"ValidityPeriod\"><xs:element name=\"StartDateTime\" type=\"xs:dateTime\"/><xs:element name=\"EndDateTime\" type=\"xs:dateTime\"/></xs:element></xs:element></xs:element><xs:element name=\"NetworkSpecificParameter\" maxOccurs=\"unbounded\"><xs:element name=\"Name\" type=\"xs:string\"/><xs:element name=\"Value\" type=\"xs:string\"/></xs:element><xs:element name=\"FreeTextField\" type=\"xs:string\" maxOccurs=\"unbounded\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
		void s013_s015Message_TrainJourneyModificationMessage()
		{
			std::string rootNodeName = "TrainJourneyModificationMessage";
			std::string expectedXML = "<xs:element name=\"TrainJourneyModificationMessage\"><xs:element name=\"MessageHeader\"><xs:element name=\"MessageReference\"><xs:element name=\"MessageType\" type=\"xs:integer\"/><xs:element name=\"MessageTypeVersion\" type=\"xs:string\"/><xs:element name=\"MessageIdentifier\" type=\"xs:string\"/><xs:element name=\"MessageDateTime\" type=\"xs:dateTime\"/></xs:element><xs:element name=\"MessageRoutingID\" type=\"xs:integer\"/><xs:element name=\"SenderReference\" type=\"xs:string\"/><xs:element name=\"Sender\" type=\"xs:integer\"><xs:attribute name=\"CI_InstanceNumber\" type=\"xs:integer\"/></xs:element><xs:element name=\"Recipient\" type=\"xs:integer\"><xs:attribute name=\"CI_InstanceNumber\" type=\"xs:integer\"/></xs:element></xs:element><xs:element name=\"MessageStatus\" type=\"xs:token\"/><xs:element name=\"TrainOperationalIdentification\"><xs:element name=\"TransportOperationalIdentifiers\" maxOccurs=\"unbounded\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element></xs:element><xs:element name=\"OperationalTrainNumberIdentifier\"><xs:element name=\"OperationalTrainNumber\" type=\"xs:string\"/><xs:element name=\"ScheduledTimeAtHandover\" type=\"xs:dateTime\"/><xs:element name=\"ScheduledDateTimeAtTransfer\" type=\"xs:dateTime\"/></xs:element><xs:element name=\"ReferenceOTN\"><xs:element name=\"OperationalTrainNumberIdentifier\"><xs:element name=\"OperationalTrainNumber\" type=\"xs:string\"/><xs:element name=\"ScheduledTimeAtHandover\" type=\"xs:dateTime\"/><xs:element name=\"ScheduledDateTimeAtTransfer\" type=\"xs:dateTime\"/></xs:element></xs:element><xs:element name=\"TrainJourneyModification\" maxOccurs=\"unbounded\"><xs:element name=\"TrainJourneyModificationIndicator\" type=\"xs:integer\"/><xs:element name=\"LocationModified\" maxOccurs=\"unbounded\"><xs:element name=\"Location\"><xs:element name=\"CountryCodeISO\" type=\"xs:string\"/><xs:element name=\"LocationPrimaryCode\" type=\"xs:positiveInteger\"/><xs:element name=\"PrimaryLocationName\" type=\"xs:string\"/><xs:element name=\"LocationSubsidiaryIdentification\"><xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"AllocationCompany\" type=\"xs:integer\"/><xs:element name=\"LocationSubsidiaryName\" type=\"xs:string\"/></xs:element></xs:element><xs:element name=\"ModificationStatusIndicator\" type=\"xs:integer\"/><xs:element name=\"TrainLocationStatus\" type=\"xs:token\"/><xs:element name=\"TimingAtLocation\"><xs:element name=\"Timing\" maxOccurs=\"unbounded\"><xs:element name=\"Time\" type=\"xs:time\"/><xs:element name=\"Offset\" type=\"xs:integer\"/><xs:attribute name=\"TimingQualifierCode\" type=\"xs:token\"/></xs:element><xs:element name=\"DwellTime\" type=\"xs:decimal\"/></xs:element><xs:element name=\"TrainActivity\" maxOccurs=\"unbounded\"><xs:element name=\"TrainActivityType\" type=\"xs:string\"/><xs:element name=\"AssociatedAttachedTrainID\"><xs:element name=\"ObjectType\" type=\"xs:string\"/><xs:element name=\"Company\" type=\"xs:integer\"/><xs:element name=\"Core\" type=\"xs:string\"/><xs:element name=\"Variant\" type=\"xs:string\"/><xs:element name=\"TimetableYear\" type=\"xs:integer\"/><xs:element name=\"StartDate\" type=\"xs:date\"/></xs:element><xs:element name=\"AssociatedAttachedOTN\" type=\"xs:string\"/></xs:element><xs:element name=\"NetworkSpecificParameter\" maxOccurs=\"unbounded\"><xs:element name=\"Name\" type=\"xs:string\"/><xs:element name=\"Value\" type=\"xs:string\"/></xs:element></xs:element></xs:element><xs:element name=\"ModificationReason\" type=\"xs:token\"><xs:attribute name=\"NationalDelayCode\" type=\"xs:string\"/></xs:element><xs:element name=\"TrainJourneyModificationTime\" type=\"xs:dateTime\"/><xs:element name=\"Remarks\" type=\"xs:string\" maxOccurs=\"unbounded\"/><xs:element name=\"TransferPoint\"><xs:element name=\"CountryCodeISO\" type=\"xs:string\"/><xs:element name=\"LocationPrimaryCode\" type=\"xs:positiveInteger\"/><xs:element name=\"PrimaryLocationName\" type=\"xs:string\"/><xs:element name=\"LocationSubsidiaryIdentification\"><xs:element name=\"LocationSubsidiaryCode\" type=\"xs:string\"><xs:attribute name=\"LocationSubsidiaryTypeCode\" type=\"xs:token\"/></xs:element><xs:element name=\"AllocationCompany\" type=\"xs:integer\"/><xs:element name=\"LocationSubsidiaryName\" type=\"xs:string\"/></xs:element></xs:element><xs:element name=\"InternalReferenceIdentifier\" type=\"xs:string\"/><xs:element name=\"TransfereeIM\" type=\"xs:integer\"/></xs:element>";
			runGeneratorGetXML(rootNodeName, expectedXML);
		}
		
	private:
		void runGeneratorGetXML(std::string & rootNodeName, std::string & expectedXML)
		{
			expectedXML = expectedXML;
			std::string output = definitionGenerator.getRootNodeDefinition(rootNodeName, "bin/xsdFiles/GB_TAF_TAP_TSI_complete.xsd");
			if(output != expectedXML)
			{
				std::cout << std::endl;
				std::cout << "Expected: " << expectedXML << std::endl;
				std::cout << "Actual: " << output << std::endl;
			}
			CPPUNIT_ASSERT(output == expectedXML);
		}
};