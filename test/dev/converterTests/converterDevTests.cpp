#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <sstream>
#include <fstream>
#include "../../../src/xmlConverter.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION( class ConverterDevTests );

class ConverterDevTests : public CppUnit::TestFixture
{
	public:
		XMLConverter converter;
		
		CPPUNIT_TEST_SUITE( ConverterDevTests );
	
		CPPUNIT_TEST( emptyNode );
		CPPUNIT_TEST( basicXMLNode );
		CPPUNIT_TEST( basicNodeWithAttributes );
		CPPUNIT_TEST( basicNodeWithXMLNSAttributes );
		CPPUNIT_TEST( basicNodeWithXSIAttributes );
		CPPUNIT_TEST( basicNodeWithNamespaceAttributes );
		CPPUNIT_TEST( selfClosingWithAttributes );
		CPPUNIT_TEST( basicNodeWithComments );
		CPPUNIT_TEST( basicNodeWithXMLProlog );
		CPPUNIT_TEST( specialCharacters );
		CPPUNIT_TEST( oddWhitespace );
		CPPUNIT_TEST( nestedNode );
		CPPUNIT_TEST( nestedNodeWithEmptyChildNode );
		CPPUNIT_TEST( nestedNodeWithAttributes );
		CPPUNIT_TEST( nestedNodeAndValue );
		CPPUNIT_TEST( nestedNodeWithAttributesAndValue );
		CPPUNIT_TEST( s009Message );
		CPPUNIT_TEST( s013Message );
		CPPUNIT_TEST( s015Message );

		CPPUNIT_TEST_SUITE_END();
		
		/*
			An XML node essentially has up to 4 sections. A name, attributes, child nodes and a value.
			Only the name is essential.
			This leads to 8 different variants of node.
			1. Empty (i.e. no attributes, child nodes or value)
			2. Value only.
			3. Child Nodes only.
			4. Child Nodes and value.
			5. Attributes only.
			6. Attributes and value.
			7. Attributes and Child Nodes.
			8. Attributes, Child Nodes and Value
			Additionally the converter ignores xmlns and xsi attributes.
			Therefore if a node only has these type of attributes then essentially the node has no attributes.
		*/
		
		void emptyNode()
		{
			// ICD-012 - SG16
			std::string testXML = "<XMLNode/>";
			std::string expectedOutput = "{\"XMLNode\":null}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicXMLNode()
		{
			// ICD-012 - SG12
			//Value only
			std::string testXML = "<XMLNode>Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":\"Basic Node\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithAttributes()
		{
			// ICD-012 - SG4
			//Attributes and value
			std::string testXML = "<XMLNode attr=\"attr text\">Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":{\"@attr\":\"attr text\",\"content\":\"Basic Node\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithXMLNSAttributes()
		{
			// ICD-012 - SG6
			//XMLNS attribute therefore value only
			std::string testXML = "<XMLNode xmlns=\"namespace\">Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":\"Basic Node\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithXSIAttributes()
		{
			// ICD-012 - SG6
			//XSI attribute therefore value only
			std::string testXML = "<XMLNode xsi=\"namespace\">Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":\"Basic Node\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithNamespaceAttributes()
		{
			// ICD-012 - SG5
			//Attributes and value
			std::string testXML = "<XMLNode n1:attr=\"attr text\">Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":{\"@n1:attr\":\"attr text\",\"content\":\"Basic Node\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void selfClosingWithAttributes()
		{
			// ICD-012 - SG3
			//Attributes only
			std::string testXML = "<XMLNode attr=\"attr text\" />";
			std::string expectedOutput = "{\"XMLNode\":{\"@attr\":\"attr text\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithComments()
		{
			// ICD-012 - SG2
			std::string testXML = "<XMLNode>Basic Node<!--XML Comment--></XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":\"Basic Node\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void basicNodeWithXMLProlog()
		{
			// ICD-012 - SG1
			std::string testXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XMLNode>Basic Node</XMLNode>";
			std::string expectedOutput = "{\"XMLNode\":\"Basic Node\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void specialCharacters()
		{
			// ICD-012 - SG9
			std::string testXML = "<SpecialCharacterTest>&lt; &gt; &amp; &apos; &quot; \\  tab  line feed</SpecialCharacterTest>";
			std::string expectedOutput = "{\"SpecialCharacterTest\":\"< > & ' \\\" \\\\ \\t \\n\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void oddWhitespace()
		{
			// ICD-012 - SG7
			std::string testXML = "<OddWhitespaceTest>Message\twith\n	odd \n\n whitespace</OddWhitespaceTest>";
			std::string expectedOutput = "{\"OddWhitespaceTest\":\"Message\twith\n	odd \n\n whitespace\"}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void nestedNode()
		{
			//Child Node only
			std::string testXML = "<ParentNode><ChildNode>Child Node Text</ChildNode></ParentNode>";
			std::string expectedOutput = "{\"ParentNode\":{\"ChildNode\":\"Child Node Text\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
	
		void nestedNodeWithEmptyChildNode()
		{
			//Child Node only
			std::string testXML = "<ParentNode><ChildNode/></ParentNode>";
			std::string expectedOutput = "{\"ParentNode\":{\"ChildNode\":null}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void nestedNodeWithAttributes()
		{
			//Attributes and Child Nodes
			std::string testXML = "<ParentNode parentAttr=\"parentAttr Text\"><ChildNode childAttr=\"childAttr Text\">Child Node Text</ChildNode></ParentNode>";
			std::string expectedOutput = "{\"ParentNode\":{\"@parentAttr\":\"parentAttr Text\",\"ChildNode\":{\"@childAttr\":\"childAttr Text\",\"content\":\"Child Node Text\"}}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void nestedNodeAndValue()
		{
			//Child Node and value
			std::string testXML = "<ParentNode>ParentNode Text<ChildNode childAttr=\"childAttr Text\">Child Node Text</ChildNode></ParentNode>";
			std::string expectedOutput = "{\"ParentNode\":{\"ChildNode\":{\"@childAttr\":\"childAttr Text\",\"content\":\"Child Node Text\"},\"content\":\"ParentNode Text\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void nestedNodeWithAttributesAndValue()
		{
			//Attributes, Child Node and value
			std::string testXML = "<ParentNode parentAttr=\"parentAttr Text\">ParentNode Text<ChildNode childAttr=\"childAttr Text\">Child Node Text</ChildNode></ParentNode>";
			std::string expectedOutput = "{\"ParentNode\":{\"@parentAttr\":\"parentAttr Text\",\"ChildNode\":{\"@childAttr\":\"childAttr Text\",\"content\":\"Child Node Text\"},\"content\":\"ParentNode Text\"}}";
			runConverterGetJSON(testXML, expectedOutput);
		}
		
		void s009Message()
		{
			std::string testXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PathDetailsMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>2003</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d9262cd303</MessageIdentifier><MessageDateTime>2017-06-29T08:59:42-00:00</MessageDateTime></MessageReference><SenderReference>9M21BF+s9SBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">0070</Recipient></MessageHeader><AdministrativeContactInformation><Name>Network Rail</Name></AdministrativeContactInformation><Identifiers><PlannedTransportIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--879M21MH29</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></PlannedTransportIdentifiers><PlannedTransportIdentifiers><ObjectType>PA</ObjectType><Company>0070</Company><Core>9M21L6796010</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></PlannedTransportIdentifiers></Identifiers><MessageStatus>1</MessageStatus><TypeOfRequest>3</TypeOfRequest><TypeOfInformation>50</TypeOfInformation><PathInformation><PlannedJourneyLocation JourneyLocationTypeCode=\"01\"><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>55950</LocationPrimaryCode><LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">CLPHMJN</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>10:59:00</Time><Offset>0</Offset></Timing></TimingAtLocation><ResponsibleRU>9930</ResponsibleRU><ResponsibleIM>0070</ResponsibleIM><TrainActivity><TrainActivityType>GBTB</TrainActivityType></TrainActivity><OperationalTrainNumber>9M21</OperationalTrainNumber><NetworkSpecificParameter><Name>ASM</Name><Value>0</Value></NetworkSpecificParameter><NetworkSpecificParameter><Name>UID</Name><Value>L67960</Value></NetworkSpecificParameter><NetworkSpecificParameter><Name>SER</Name><Value>22206000</Value></NetworkSpecificParameter></PlannedJourneyLocation><PlannedJourneyLocation JourneyLocationTypeCode=\"01\"><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>99999</LocationPrimaryCode></PlannedJourneyLocation><PlannedCalendar><BitmapDays>1</BitmapDays><ValidityPeriod><StartDateTime>2017-06-29T10:59:00-00:00</StartDateTime></ValidityPeriod></PlannedCalendar></PathInformation></PathDetailsMessage>";
			std::string expectedJSON = "{\"PathDetailsMessage\":{\"AdministrativeContactInformation\":{\"Name\":\"Network Rail\"},\"Identifiers\":{\"PlannedTransportIdentifiers\":[{\"Company\":0070,\"Core\":\"--879M21MH29\",\"ObjectType\":\"TR\",\"StartDate\":\"2017-06-29\",\"TimetableYear\":2017,\"Variant\":\"01\"},{\"Company\":0070,\"Core\":\"9M21L6796010\",\"ObjectType\":\"PA\",\"StartDate\":\"2017-06-29\",\"TimetableYear\":2017,\"Variant\":\"01\"}]},\"MessageHeader\":{\"MessageReference\":{\"MessageDateTime\":\"2017-06-29T08:59:42-00:00\",\"MessageIdentifier\":\"414d51204e52504230303920202020205950d6d9262cd303\",\"MessageType\":2003,\"MessageTypeVersion\":\"5.3.1.GB\"},\"Recipient\":{\"@n1:CI_InstanceNumber\":99,\"content\":0070},\"Sender\":{\"@n1:CI_InstanceNumber\":01,\"content\":0070},\"SenderReference\":\"9M21BF+s9SBG\"},\"MessageStatus\":\"1\",\"PathInformation\":{\"PlannedCalendar\":{\"BitmapDays\":\"1\",\"ValidityPeriod\":{\"StartDateTime\":\"2017-06-29T10:59:00-00:00\"}},\"PlannedJourneyLocation\":[{\"@JourneyLocationTypeCode\":\"01\",\"CountryCodeISO\":\"GB\",\"LocationPrimaryCode\":55950,\"LocationSubsidiaryIdentification\":{\"AllocationCompany\":0070,\"LocationSubsidiaryCode\":{\"@n1:LocationSubsidiaryTypeCode\":\"0\",\"content\":\"CLPHMJN\"}},\"NetworkSpecificParameter\":[{\"Name\":\"ASM\",\"Value\":\"0\"},{\"Name\":\"UID\",\"Value\":\"L67960\"},{\"Name\":\"SER\",\"Value\":\"22206000\"}],\"OperationalTrainNumber\":\"9M21\",\"ResponsibleIM\":0070,\"ResponsibleRU\":9930,\"TimingAtLocation\":{\"Timing\":[{\"@TimingQualifierCode\":\"ALD\",\"Offset\":0,\"Time\":\"10:59:00\"}]},\"TrainActivity\":[{\"TrainActivityType\":\"GBTB\"}]},{\"@JourneyLocationTypeCode\":\"01\",\"CountryCodeISO\":\"GB\",\"LocationPrimaryCode\":99999}]},\"TypeOfInformation\":50,\"TypeOfRequest\":3}}";
			runConverterGetJSON(testXML, expectedJSON);
		}
		
		void s013Message()
		{
			std::string testXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d9262b6082</MessageIdentifier><MessageDateTime>2017-06-29T08:56:36-00:00</MessageDateTime></MessageReference><SenderReference>5C00Y6EkFRBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">9999</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--735C00MS28</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-28</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>5C00</OperationalTrainNumber></OperationalTrainNumberIdentifier><TrainJourneyModification><TrainJourneyModificationIndicator>91</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>30951</LocationPrimaryCode><LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">ROYAOSD</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification></Location><ModificationStatusIndicator>91</ModificationStatusIndicator><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>16:04:00</Time><Offset>0</Offset></Timing></TimingAtLocation></LocationModified></TrainJourneyModification><ModificationReason NationalDelayCode=\"OU\">39</ModificationReason><TrainJourneyModificationTime>2017-06-29T09:56:00-00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
			std::string expectedJSON = "{\"TrainJourneyModificationMessage\":{\"MessageHeader\":{\"MessageReference\":{\"MessageDateTime\":\"2017-06-29T08:56:36-00:00\",\"MessageIdentifier\":\"414d51204e52504230303920202020205950d6d9262b6082\",\"MessageType\":9004,\"MessageTypeVersion\":\"5.3.1.GB\"},\"Recipient\":{\"@n1:CI_InstanceNumber\":99,\"content\":9999},\"Sender\":{\"@n1:CI_InstanceNumber\":01,\"content\":0070},\"SenderReference\":\"5C00Y6EkFRBG\"},\"MessageStatus\":\"1\",\"ModificationReason\":{\"@NationalDelayCode\":\"OU\",\"content\":\"39\"},\"OperationalTrainNumberIdentifier\":{\"OperationalTrainNumber\":\"5C00\"},\"TrainJourneyModification\":[{\"LocationModified\":[{\"Location\":{\"CountryCodeISO\":\"GB\",\"LocationPrimaryCode\":30951,\"LocationSubsidiaryIdentification\":{\"AllocationCompany\":0070,\"LocationSubsidiaryCode\":{\"@n1:LocationSubsidiaryTypeCode\":\"0\",\"content\":\"ROYAOSD\"}}},\"ModificationStatusIndicator\":91,\"TimingAtLocation\":{\"Timing\":[{\"@TimingQualifierCode\":\"ALD\",\"Offset\":0,\"Time\":\"16:04:00\"}]}}],\"TrainJourneyModificationIndicator\":91}],\"TrainJourneyModificationTime\":\"2017-06-29T09:56:00-00:00\",\"TrainOperationalIdentification\":{\"TransportOperationalIdentifiers\":[{\"Company\":0070,\"Core\":\"--735C00MS28\",\"ObjectType\":\"TR\",\"StartDate\":\"2017-06-28\",\"TimetableYear\":2017,\"Variant\":\"01\"}]}}}";
			runConverterGetJSON(testXML, expectedJSON);
		}
		
		void s015Message()
		{
			std::string testXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 GB_TAF_TAP_TSI_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>414d51204e52504230303920202020205950d6d926106d13</MessageIdentifier><MessageDateTime>2017-06-29T08:01:57-00:00</MessageDateTime></MessageReference><SenderReference>5C00Y6EkFRBG,7V1872tuASBG</SenderReference><Sender n1:CI_InstanceNumber=\"01\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">0070</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>TR</ObjectType><Company>0070</Company><Core>--736V18CI29</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2017-06-29</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>7V18</OperationalTrainNumber></OperationalTrainNumberIdentifier><ReferenceOTN><OperationalTrainNumberIdentifier><OperationalTrainNumber>5C00</OperationalTrainNumber></OperationalTrainNumberIdentifier></ReferenceOTN><TrainJourneyModification><TrainJourneyModificationIndicator>07</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>99999</LocationPrimaryCode></Location><ModificationStatusIndicator>07</ModificationStatusIndicator></LocationModified></TrainJourneyModification><TrainJourneyModificationTime>2017-06-29T09:01:55-00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
			std::string expectedJSON = "{\"TrainJourneyModificationMessage\":{\"MessageHeader\":{\"MessageReference\":{\"MessageDateTime\":\"2017-06-29T08:01:57-00:00\",\"MessageIdentifier\":\"414d51204e52504230303920202020205950d6d926106d13\",\"MessageType\":9004,\"MessageTypeVersion\":\"5.3.1.GB\"},\"Recipient\":{\"@n1:CI_InstanceNumber\":99,\"content\":0070},\"Sender\":{\"@n1:CI_InstanceNumber\":01,\"content\":0070},\"SenderReference\":\"5C00Y6EkFRBG,7V1872tuASBG\"},\"MessageStatus\":\"1\",\"OperationalTrainNumberIdentifier\":{\"OperationalTrainNumber\":\"7V18\"},\"ReferenceOTN\":{\"OperationalTrainNumberIdentifier\":{\"OperationalTrainNumber\":\"5C00\"}},\"TrainJourneyModification\":[{\"LocationModified\":[{\"Location\":{\"CountryCodeISO\":\"GB\",\"LocationPrimaryCode\":99999},\"ModificationStatusIndicator\":07}],\"TrainJourneyModificationIndicator\":07}],\"TrainJourneyModificationTime\":\"2017-06-29T09:01:55-00:00\",\"TrainOperationalIdentification\":{\"TransportOperationalIdentifiers\":[{\"Company\":0070,\"Core\":\"--736V18CI29\",\"ObjectType\":\"TR\",\"StartDate\":\"2017-06-29\",\"TimetableYear\":2017,\"Variant\":\"01\"}]}}}";
			runConverterGetJSON(testXML, expectedJSON);
		}
		
	private:
		void runConverterGetJSON(std::string & xml, const std::string & expectedJSON)
		{
			CPPUNIT_ASSERT(xml != "");
			std::string output = converter.convertXMLtoJSON(xml);
			if(output != expectedJSON)
			{
				std::cout << std::endl;
				std::cout << "Expected: " << expectedJSON << std::endl;
				std::cout << "Actual: " << output << std::endl;
			}
			CPPUNIT_ASSERT(output == expectedJSON);
		}
};