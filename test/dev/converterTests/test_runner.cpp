#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <cppunit/TestListener.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TextTestRunner.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestFailure.h>

using namespace std;

void signal_callback_handler(int signum)
{
	printf("Caught signal %d\n", signum);
	exit(signum);
}

class TestListener : public CppUnit::TestListener
{
	private:
		bool failed;
		uint16_t testCount;
		uint16_t failureCount;
		
	public:
		void startTest(CppUnit::Test *test)
		{
			testCount++;
			failed = false;
			cout << "Test: " << test->getName() << "..\033[1;31m" << flush;
		}
		
		void endTest(CppUnit::Test *test)
		{
			if (failed)
			{
				cout << "AILED\033[0m" << std::endl;
			}
			else
			{
				cout << "\033[1;32mPASSED\033[0m" << std::endl;
			}
		}
		
		void addFailure(const CppUnit::TestFailure &failure)
		{
			failed = true;
		}
		
		void endTestRun(CppUnit::Test * test, CppUnit::TestResult *eventManager){}
};

int main(int argc, char* argv[])
{
	signal(SIGINT, signal_callback_handler);
	signal(SIGTERM, signal_callback_handler);
	
	CPPUNIT_NS::Test *suite = CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest();
	
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);
	
	CppUnit::TestResult & event_manager = runner.eventManager();
	event_manager.addListener(new TestListener());
	
	cout << endl;
	
	bool wasSuccessful = runner.run("", false, true, true);
	
	return wasSuccessful ? 0 : 1;
	
}