#include <iostream>
#include <sys/wait.h>

//#include "Constants_Valid.hpp"
#include "TestScript.hpp"

using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION( class TestHarness_Tests );  // register the test class with CppUnit

class TestHarness_Tests : public TestScript
{
    ///
    /// CppUnit: Add a CPPUNIT_TEST for each test case or it will not run!
    ///

    CPPUNIT_TEST_SUITE( TestHarness_Tests );

    TEST( setupScript );
    TEST( takedownScript );
    TEST( testUtils_Strings );
    TEST( testUtils_Dates );
    TEST( testUtils_UTC_of_LT );
    TEST( testUtils_LT_of_UTC );
    TEST( testDaySecondOfHms12 );
    TEST( testConfig );
	
	TEST( mqSizeAndIsEmptyWork );
    TEST( mqMessageCanBePushed );
    TEST( mqCanBeCleared );
    TEST( mqCanBePopped );
    TEST( mqSizeAndIsEmptyWorkVar );
    TEST( mqMessageCanBePushedVar );
    TEST( mqCanBeClearedVar );
    TEST( mqCanBePoppedVar );
    TEST( mqS002works );
    TEST( allQueuesCanBeCleared );
   
    TEST( hubCanBeStartedAndStoppedOnTheHost, SWRTS );
    TEST( hubCanBeStartedAndStoppedOnTheTarget, SATS );
    TEST( hubCanBePassedParameters );
    TEST( hubExplicitParametersAreParsedCorrectly );
    TEST( hubCanBePassedParametersExplicitly );
    TEST( hubCanBeRunUntilItCompletes );
    TEST( resetLogFileNames );
    
    TEST( thisTestIsMarkedSomePassing, SOME_PASS );
    TEST( thisTestIsMarkedFail, FAIL );
    TEST( thisTestIsMarkedTodo, TODO );
    TEST( thisTestIsMarkedManual, MANUAL );
    TEST( thisIsASwrtsTest, SWRTS );
    TEST( thisIsASatsTest, SATS );
    TEST( thisIsAManualSatsTest, SATS, MANUAL );
    //TEST( testProxyController );
    //TEST( isLinkAvailableCanBeReadForLinxAndTranista );
    //TEST( individualLinxLinkCanBeDisconnectedAndConnected );
    //TEST( individualTranistaLinkCanBeDisconnectedAndConnected );
    //TEST( overallLinxCanBeDisconnectedAndConnected );
    //TEST( overallTranistaCanBeDisconnectedAndConnected );
    //TEST( proxyControllerCloseProxies );
    TEST( logSessionReadsTheOperationalLogCorrectly );
    TEST( logSessionCanBeFilteredByOneTag );
    TEST( logSessionCanBeFilteredByTwoTags );
    TEST( testXMLValidation );

    CPPUNIT_TEST_SUITE_END();


public:

    ///
    /// Test script setup.
    ///

    //MessageStateFlowRecords messageStateFlowRecords;

    void setupScript()
    {
        //HithubCfg & hithubCfg = *testEnvironment.hithubCfg;

        //hithubCfg.addOrSet("S002.IsTrainInTmsArea", "true");

        //hub->installConfigFile( hithubCfg );
        //tranista.messageStateFlowTable.selectAll(messageStateFlowRecords);
    }

    void takedownScript()
    {
        //tranista.messageStateFlowTable.insertAll(messageStateFlowRecords);
    }
    
    ///
    /// Test Cases
    ///

    #undef assertEqual
    #define assertEqual( A, B ) CPPUNIT_ASSERT(B == A)

    #define ASSERT_EQUAL_STR(NAME, X, A) assertEqualMsg( (std::string)(NAME) + " wrong expected(" + X + ") actual(" + A + ")", X, A )
    
    #define CHECK_SIZE(TABLE, SIZE) \
        { \
            assertEqual( SIZE, TABLE.size() ); \
            assertEqual( (SIZE == 0), TABLE.isEmpty() ); \
        }

    /// util Functions

    void testUtils_Strings(){
        /* fails! {
            const string set = "empty";
            const string line = "";
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 0);
        }*/
        {
            const string set = "one";
            const string line = "one";
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 1);
            assertMsg("Unexpected strictSplit " + set + "B", fields[0] == "one");
        }
        {
            const string set = "oneTablE";
            const string line = "one\t";
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 2);
            assertMsg("Unexpected strictSplit " + set + "B", fields[0] == "one");
            assertMsg("Unexpected strictSplit " + set + "c", fields[1] == "");
        }
        {
            const string set = "tabOne";
            const string line = "\tone";
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 2);
            assertMsg("Unexpected strictSplit " + set + "B", fields[0] == "");
            assertMsg("Unexpected strictSplit " + set + "C", fields[1] == "one");
        }
        {
            const string set = "oneTwo";
            const string line = "one\ttwo";
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 2);
            assertMsg("Unexpected strictSplit " + set + "B", fields[0] == "one");
            assertMsg("Unexpected strictSplit " + set + "C", fields[1] == "two");
        }
        {
            const string set = "hithub";
            const string field0 = "2018-06-21 10:13:25.100 +0000";
            const string field1 = "Hitachi_Hub";
            const string field2 = "[info]";
            const string field3 = "Starting";
            const string field4 = "<s001>";
            const string field5 = "Info, One Tag A";
            const string line = field0 + "\t" + field1 + "\t" + field2 + "\t" + field3 + "\t" + field4 + "\t" + field5;
            const vector<string> fields = strictSplit(line, "\t");
            assertMsg("Unexpected strictSplit " + set + "A", fields.size() == 6);
            assertMsg("Unexpected strictSplit " + set + "B", fields[0] == field0);
            assertMsg("Unexpected strictSplit " + set + "C", fields[1] == field1);
            assertMsg("Unexpected strictSplit " + set + "D", fields[2] == field2);
            assertMsg("Unexpected strictSplit " + set + "E", fields[3] == field3);
            assertMsg("Unexpected strictSplit " + set + "F", fields[4] == field4);
            assertMsg("Unexpected strictSplit " + set + "G", fields[5] == field5);
        }
        {
            const string set = "empty ";
            string left;
            string right;
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "") && (right == ""));
        }
        {
            const string set = "diff ";
            string left = "left";
            string right = "right";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "left") && (right == "right"));
        }
        {
            const string set = "same1 ";
            string left = "one";
            string right = "one";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "") && (right == ""));
        }
        {
            const string set = "same2 ";
            string left = "one\ntwo";
            string right = "one\ntwo";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "") && (right == ""));
        }
        {
            const string set = "diff2 ";
            string left = "one\ntwo";
            string right = "one\nTwo";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "two") && (right == "Two"));
        }
        {
            const string set = "diff3 ";
            string left = "one\ntwo\nthree";
            string right = "one\nTwo\nthree";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "two") && (right == "Two"));
        }
        {
            const string set = "diff4 ";
            string left = "one\ntwo\nthree\nfour";
            string right = "one\nTwo\nthree";
            discardIdenticalLines( left, right );
            assertMsg("Unexpected discardIdenticalLines " + set + "A", (left == "two\nfour") && (right == "Two"));
        }
        
        {
            const string colonTime = "07:55:30";
            const string utcid1 = "2016-11-10T" + colonTime + "+00:00";
            const string ltid1 = utcid1;
            EpochSecond e1 = 1478764530;
            EpochSecond utc1 = utcEpochSecondOfIsoDat(utcid1);
            EpochSecond lt1 = ltOfUtcEpochSecond(utc1);
            assertEqualMsg("utcEpochSecondOfIsoDat 1", e1, utc1);
            assertEqualMsg("utcEpochSecondIsSummerTime 1", false, utcEpochSecondIsSummerTime(utc1));
            assertEqualMsg("ltEpochSecondIsSummerTime 1", false, ltEpochSecondIsSummerTime(utc1));
            assertEqualMsg("ltOfUtcEpochSecond 1", e1, lt1);
            assertEqualMsg("utcOfLtEpochSecond 1", e1, utcOfLtEpochSecond(utc1));
            assertEqualMsg("isoDatOfUtcEpochSecond 1", utcid1, isoDatOfUtcEpochSecond(utc1));
            assertEqualMsg("isoDatLtOfUtcEpochSecond 1", ltid1, isoDatLtOfUtcEpochSecond(utc1));
            assertEqualMsg("isoDatLtOfLtEpochSecond 1", ltid1, isoDatLtOfUtcEpochSecond(lt1));
            assertEqualMsg("colonTimeOfIsoDat 1", colonTime, colonTimeOfIsoDat(utcid1));
            assertEqualMsg("daySecondOfColonTime 1", (DaySecond)(7*3600 + 55*60 + 30), daySecondOfColonTime(colonTimeOfIsoDat(utcid1)));

            const string utcid2 = "2016-06-10T" "07" ":55:30+" "00" ":00";
            const string ltid2 =  "2016-06-10T" "08" ":55:30+" "01" ":00";
            //                                   ##             ##
            EpochSecond e2 = 1465545330;
            EpochSecond utc2 = utcEpochSecondOfIsoDat(utcid2);
            EpochSecond utc2a = utcEpochSecondOfIsoDat(ltid2);
            EpochSecond lt2 = ltOfUtcEpochSecond(utc2);
            assertEqualMsg("utcEpochSecondOfIsoDat 2", e2, utc2);
            assertEqualMsg("utcEpochSecondOfIsoDat 2a", e2, utc2a);
            assertEqualMsg("utcEpochSecondIsSummerTime 2", true, utcEpochSecondIsSummerTime(utc2));
            assertEqualMsg("ltEpochSecondIsSummerTime 2", true, ltEpochSecondIsSummerTime(utc2));
            assertEqualMsg("ltOfUtcEpochSecond 2", e2, lt2 - 3600);
            assertEqualMsg("utcOfLtEpochSecond 2", e2, utcOfLtEpochSecond(lt2));
            assertEqualMsg("isoDatOfUtcEpochSecond 2", utcid2, isoDatOfUtcEpochSecond(utc2));
            assertEqualMsg("isoDatLtOfUtcEpochSecond 2", ltid2, isoDatLtOfUtcEpochSecond(utc2));
            assertEqualMsg("isoDatLtOfLtEpochSecond 2", ltid2, isoDatLtOfLtEpochSecond(lt2));
            assertEqualMsg("colonTimeOfIsoDat 2", stringOf("07:55:30"), colonTimeOfIsoDat(utcid2));
            assertEqualMsg("daySecondOfColonTime 2", (DaySecond)(7*3600 + 55*60 + 30), daySecondOfColonTime(colonTimeOfIsoDat(utcid2)));
            assertEqualMsg("colonTimeOfIsoDat 2b", stringOf("08:55:30"), colonTimeOfIsoDat(ltid2));
            assertEqualMsg("daySecondOfColonTime 2b", (DaySecond)(8*3600 + 55*60 + 30), daySecondOfColonTime(colonTimeOfIsoDat(ltid2)));
        }
        {
            const string set = "1 ";
            string v = "12345";
            assertMsg("Unexpected intOf " + set + "A", stringOf(intOf(v)) == v);
        }
        {
            const string set = "1 ";
            const char * p = "12345";
            const string v(p);
            assertMsg("Unexpected stringOf " + set + "A", stringOf(p) == v);
        }
    }
    
    void testUtils_Dates()
    {
        {
            const string set = "1 ";
            const EpochDay epochDay = 365 + 31 + 28 + 15;
            const std::string cymd = cymdOfEpochday(epochDay);
            assertMsg("Unexpected cymdOfEpochday " + set + "A" + " " + cymd, cymd == "1971" "03" "16");
        }
        {
            const string set = "1 ";
            const std::string cymd = "1971" "03" "15";
            const size_t days = 3;
            const std::string cymd1 = bumpCymd(cymd, days);
            assertMsg("Unexpected bumpCymd " + set + "A" + " " + cymd1, cymd1 == "1971" "03" "18");
        }
        {
            const string set = "1 ";
            const DaySecond daySecond = (2 * 3600) + (3 * 60) + 4;
            const std::string hms = HmsOfDaySecond(daySecond);
            assertMsg("Unexpected HmsOfDaySecond " + set + "A" + " " + hms, hms == "02:03:04");
        }
        {
            const string set = "2 ";
            const DaySecond daySecond = (23 * 3600) + (59 * 60) + 58;
            const std::string hms = HmsOfDaySecond(daySecond);
            assertMsg("Unexpected HmsOfDaySecond " + set + "A" + " " + hms, hms == "23:59:58");
        }
        {
            const string set = "1 ";
            const DaySecond daySecond0 = (2 * 3600) + (3 * 60) + 4;
            const string daySecond = stringOf(daySecond0);
            const std::string hms = HmsOfTime(daySecond);
            assertMsg("Unexpected HmsOfTime " + set + "A" + " " + hms, hms == "02:03:04");
        }
        {
            const string set = "2 ";
            const DaySecond daySecond0 = (23 * 3600) + (59 * 60) + 58;
            const string daySecond = stringOf(daySecond0);
            const std::string hms = HmsOfTime(daySecond);
            assertMsg("Unexpected HmsOfTime " + set + "A" + " " + hms, hms == "23:59:58");
        }
        {
            const string set = "1 ";
            const DaySecond daySecond0 = (2 * 3600) + (3 * 60) + 4;
            const string hms = "02:03:04";
            const DaySecond daySecond1 = DaySecondOfHms(hms);
            assertMsg("Unexpected DaySecondOfHms " + set + "A" + " " + hms + " " + stringOf(daySecond1), daySecond1 == daySecond0);
        }
        {
            const string set = "2 ";
            const DaySecond daySecond0 = (23 * 3600) + (59 * 60) + 58;
            const string hms = "23:59:58";
            const DaySecond daySecond1 = DaySecondOfHms(hms);
            assertMsg("Unexpected DaySecondOfHms " + set + "A" + " " + hms, daySecond1 == daySecond0);
        }
        {
            const string set = "1 ";
            const string hms = "02:03:04";
            const std::string hour = hourOfHms(hms);
            assertMsg("Unexpected hourOfHms " + set + "A" + " " + hour, hour == "02");
        }
        {
            const string set = "2 ";
            const string hms = "23:59:58";
            const std::string hour = hourOfHms(hms);
            assertMsg("Unexpected hourOfHms " + set + "A" + " " + hour, hour == "23");
        }
        {
            const string set = "1 ";
            const std::string a = opeDateTimeOfCymdHms("20180709", "01:02:03");
            const std::string x = "09-JUL-18 01.02.03 AM";
            assertMsg("Unexpected opeDateTimeOfCymdHms " + set + "A" + " a(" + a + ") x(" + x + ")", a == x);
        }
    }
    
    void testUtils_UTC_of_LT(){ // utc of lt
        {
            const string set = "1 ";
            const CymdLt cymdLt = "20180101";
            const DaySecLt daySecLt = (2 * 3600) + (3 * 60) + 4;
            const DaySecUtc daySecUtc = UtcDaySecOfLt(cymdLt, daySecLt);
            assertMsg("Unexpected UtcDaySecOfLt " + set + "A" + " " + stringOf(daySecUtc), daySecUtc == daySecLt);
        }
        {
            const string set = "2 ";
            const CymdLt cymdLt = "20180701";
            const DaySecLt daySecLt = (2 * 3600) + (3 * 60) + 4;
            const DaySecUtc daySecUtc = UtcDaySecOfLt(cymdLt, daySecLt);
            assertMsg("Unexpected UtcDaySecOfLt " + set + "A" + " " + stringOf(daySecUtc), (daySecUtc + 3600) == daySecLt);
        }
        {
            const string set = "1 ";
            const CymdLt cymdLt = "20180101";
            const HmsLt hmsLt = "02:03:04";
            const HmsUtc hmsUtc = UtcHmsOfLt(cymdLt, hmsLt);
            assertMsg("Unexpected UtcHmsOfLt " + set + "A" + " " + hmsUtc, hmsUtc == hmsLt);
        }
        {
            const string set = "2 ";
            const CymdLt cymdLt = "20180701";
            const HmsLt hmsLt = "02:03:04";
            const HmsLt hmsLt1 = "01:03:04";
            const HmsUtc hmsUtc = UtcHmsOfLt(cymdLt, hmsLt);
            assertMsg("Unexpected UtcHmsOfLt " + set + "A" + " " + hmsUtc, hmsUtc == hmsLt1);
        }
        {
            const string set = "1 ";
            const CymdLt cymdLt = "20180101";
            const DaySecLt daySecLt = (23 * 3600) + (59 * 60) + 58;
            const CymdUtc cymdUtc = UtcCymdOfLt(cymdLt, daySecLt);
            assertMsg("Unexpected UtcCymdOfLt " + set + "A" + " " + cymdUtc, cymdUtc == cymdLt);
        }
        {
            const string set = "2 ";
            const CymdLt cymdLt = "20180703";
            const DaySecLt daySecLt = (01 * 3600) + (59 * 60) + 58;
            const CymdUtc cymdUtc = UtcCymdOfLt(cymdLt, daySecLt);
            const CymdUtc cymdUtc1 = "20180703";
            assertMsg("Unexpected UtcCymdOfLt " + set + "A" + " " + cymdUtc + " " + cymdUtc1, cymdUtc == cymdUtc1);
        }
        {
            const string set = "1 ";
            const CymdLt cymdLt = "20180701";
            const HmsLt hmsLt = "23:03:04";
            const CymdUtc cymdUtc = UtcCymdOfLt(cymdLt, hmsLt);
            assertMsg("Unexpected UtcCymdOfLt " + set + "A" + " " + cymdUtc, cymdUtc == cymdLt);
        }
        {
            const string set = "1 ";
            const CymdLt cymdLt = "20180709";
            const HmsLt hmsLt = "03:03:04";
            const CymdUtc cymdUtc = UtcCymdOfLt(cymdLt, hmsLt);
            const CymdUtc cymdUtc1 = "20180709";
            assertMsg("Unexpected UtcCymdOfLt " + set + "A" + " " + cymdUtc, cymdUtc == cymdUtc1);
        }
    }
    
    void testUtils_LT_of_UTC(){ // lt of utc
        {
            const string set = "1 ";
            const CymdUtc cymdUtc = "20180101";
            const DaySecUtc daySecUtc = (2 * 3600) + (3 * 60) + 4;
            const DaySecLt daySecLt = LtDaySecOfUtc(cymdUtc, daySecUtc);
            assertMsg("Unexpected LtDaySecOfUtc " + set + "A" + " " + stringOf(daySecLt), daySecUtc == daySecLt);
        }
        {
            const string set = "2 ";
            const CymdUtc cymdUtc = "20180701";
            const DaySecUtc daySecUtc = (2 * 3600) + (3 * 60) + 4;
            const DaySecLt daySecLt = LtDaySecOfUtc(cymdUtc, daySecUtc);
            assertMsg("Unexpected LtDaySecOfUtc " + set + "A" + " " + stringOf(daySecLt), (daySecUtc + 3600) == daySecLt);
        }
        {
            const string set = "1 ";
            const CymdUtc cymdUtc = "20180101";
            const HmsUtc hmsUtc = "02:03:04";
            const HmsLt hmsLt = LtHmsOfUtc(cymdUtc, hmsUtc);
            assertMsg("Unexpected LtHmsOfUtc " + set + "A" + " " + hmsLt, hmsLt == hmsUtc);
        }
        {
            const string set = "2 ";
            const CymdUtc cymdUtc = "20180701";
            const HmsUtc hmsUtc1 = "01:03:04";
            const HmsUtc hmsUtc2 = "02:03:04";
            const HmsLt hmsLt = LtHmsOfUtc(cymdUtc, hmsUtc1);
            assertMsg("Unexpected LtHmsOfUtc " + set + "A" + " " + hmsLt, hmsLt == hmsUtc2);
        }
        {
            const string set = "1 ";
            const CymdUtc cymdUtc = "20180101";
            const DaySecUtc daySecUtc = (23 * 3600) + (59 * 60) + 58;
            const CymdLt cymdLt = LtCymdOfUtc(cymdUtc, daySecUtc);
            assertMsg("Unexpected LtCymdOfUtc " + set + "A" + " " + cymdLt, cymdLt == cymdUtc);
        }
        {
            const string set = "2 ";
            const CymdUtc cymdUtc = "20180703";
            const DaySecUtc daySecUtc = (01 * 3600) + (59 * 60) + 58;
            const CymdLt cymdLt = LtCymdOfUtc(cymdUtc, daySecUtc);
            const CymdLt cymdLt1 = "20180703";
            assertMsg("Unexpected LtCymdOfUtc " + set + "A" + " " + cymdLt + " " + cymdLt1, cymdLt == cymdLt1);
        }
        {
            const string set = "1 ";
            const CymdUtc cymdUtc = "20180701";
            const HmsUtc hmsUtc = "22:03:04";
            const CymdLt cymdLt = LtCymdOfUtc(cymdUtc, hmsUtc);
            assertMsg("Unexpected LtCymdOfUtc " + set + "A" + " " + cymdLt, cymdLt == cymdUtc);
        }
        {
            const string set = "1 ";
            const CymdUtc cymdUtc = "20180709";
            const HmsUtc hmsUtc = "03:03:04";
            const CymdLt cymdLt = LtCymdOfUtc(cymdUtc, hmsUtc);
            const CymdLt cymdLt1 = "20180709";
            assertMsg("Unexpected LtCymdOfUtc " + set + "A" + " " + cymdLt, cymdLt == cymdLt1);
        }
    }
    
    void testDaySecondOfHms12()
    {
        assertMsg("00:00:00 am", DaySecondOfHms12("00:00:00 am") == (00 * 3600 + 00 * 60 + 00));
        assertMsg("01:02:03 AM", DaySecondOfHms12("01:02:03 AM") == (01 * 3600 + 02 * 60 + 03));
        assertMsg("12:00:00 am", DaySecondOfHms12("12:00:00 AM") == (00 * 3600 + 00 * 60 + 00));
        assertMsg("00:00:00 pm", DaySecondOfHms12("00:00:00 pm") == (12 * 3600 + 00 * 60 + 00));
        assertMsg("01:02:03 PM", DaySecondOfHms12("01:02:03 PM") == (13 * 3600 + 02 * 60 + 03));
        assertMsg("12:00:00 pm", DaySecondOfHms12("12:00:00 pm") == (12 * 3600 + 00 * 60 + 00));
    }

    void testXMLValidation()
    {
        {
            // XML validation against XSD
            std::string xml_valid = "<TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 taf_cat_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>c3e2d840c3e2d8d74040404040404040d493d1fd3f83ab08</MessageIdentifier><MessageDateTime>2018-07-04T14:59:32</MessageDateTime></MessageReference><SenderReference>A1232HjQ-SBG</SenderReference><Sender n1:CI_InstanceNumber=\"04\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">9999</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>PA</ObjectType><Company>0070</Company><Core>--831M93MO04</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>2018-01-01</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>A123</OperationalTrainNumber></OperationalTrainNumberIdentifier><TrainJourneyModification><TrainJourneyModificationIndicator>91</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>12345</LocationPrimaryCode><LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">STNDSHJ</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification></Location><ModificationStatusIndicator>91</ModificationStatusIndicator><TrainLocationStatus>02</TrainLocationStatus><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>23:59:30</Time><Offset>185</Offset></Timing></TimingAtLocation><NetworkSpecificParameter><Name>PLA</Name><Value>ABCDE12345</Value></NetworkSpecificParameter></LocationModified></TrainJourneyModification><ModificationReason NationalDelayCode=\"OZ\">11</ModificationReason><TrainJourneyModificationTime>2017-06-30T01:02:03+00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
            std::string xml_invalid = "<TrainJourneyModificationMessage xmlns=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:n1=\"http://www.era.europa.eu/schemes/TAFTSI/5.3\" xsi:schemaLocation=\"http://www.era.europa.eu/schemes/TAFTSI/5.3 taf_cat_complete.xsd\"><MessageHeader><MessageReference><MessageType>9004</MessageType><MessageTypeVersion>5.3.1.GB</MessageTypeVersion><MessageIdentifier>c3e2d840c3e2d8d74040404040404040d493d1fd3f83ab08</MessageIdentifier><MessageDateTime>2018-07-04T14:59:32</MessageDateTime></MessageReference><SenderReference>A1232HjQ-SBG</SenderReference><Sender n1:CI_InstanceNumber=\"04\">0070</Sender><Recipient n1:CI_InstanceNumber=\"99\">9999</Recipient></MessageHeader><MessageStatus>1</MessageStatus><TrainOperationalIdentification><TransportOperationalIdentifiers><ObjectType>PA</ObjectType><Company>070</Company><Core>--3MO04</Core><Variant>01</Variant><TimetableYear>2017</TimetableYear><StartDate>208-01-01</StartDate></TransportOperationalIdentifiers></TrainOperationalIdentification><OperationalTrainNumberIdentifier><OperationalTrainNumber>A123</OperationalTrainNumber></OperationalTrainNumberIdentifier><TrainJourneyModification><TrainJourneyModificationIndicator>91</TrainJourneyModificationIndicator><LocationModified><Location><CountryCodeISO>GB</CountryCodeISO><LocationPrimaryCode>12345<LocationSubsidiaryIdentification><LocationSubsidiaryCode n1:LocationSubsidiaryTypeCode=\"0\">STNDSHJ</LocationSubsidiaryCode><AllocationCompany>0070</AllocationCompany></LocationSubsidiaryIdentification></Location><ModificationStatusIndicator>91</ModificationStatusIndicator><TrainLocationStatus>02</TrainLocationStatus><TimingAtLocation><Timing TimingQualifierCode=\"ALD\"><Time>23:59:30</Time><Offset>185</Offset></Timing></TimingAtLocation><NetworkSpecificParameter><Name>PLA</Name><Value>ABCDE12345</Value></NetworkSpecificParameter></LocationModified></TrainJourneyModification><ModificationReason NationalDelayCode=\"OZ\">11</ModificationReason><TrainJourneyModificationTime>2017-06-30T01:02:03+00:00</TrainJourneyModificationTime></TrainJourneyModificationMessage>";
            
            assertMsg("Valid XML not passing validation", validateXmlAgainstXsd(xml_valid));
            assertMsg("Invalid XML successfully passed validation", !validateXmlAgainstXsd(xml_invalid));
        }
    }

    void testConfig()
    {
        //HithubCfg & hithubCfg = *testEnvironment.hithubCfg;
        assertMsg("testConfig 100", hithubCfg.exists("no_such_key") == false);
        hithubCfg.add("no_such_key", "a value");
        assertMsg("testConfig 200", hithubCfg.exists("no_such_key") == true);
        assertMsg("testConfig 300", hithubCfg.get("no_such_key") == "a value");
        hithubCfg.set("no_such_key", "another value");
        assertMsg("testConfig 400", hithubCfg.exists("no_such_key") == true);
        assertMsg("testConfig 500", hithubCfg.get("no_such_key") == "another value");
        hithubCfg.remove("no_such_key");
        assertMsg("testConfig 600", hithubCfg.exists("no_such_key") == false);
    }

    /// Basic Queue Functions

    void mqSizeAndIsEmptyWork(){
        linx.S002_Queue.clear();
        assertEqual( 0, linx.S002_Queue.size() );
        assertEqual( true, linx.S002_Queue.isEmpty() );
    }

    void mqMessageCanBePushed(){
        linx.S002_Queue.push("Hello World!");
        assertEqual( 1, linx.S002_Queue.size() );
        assertEqual( false, linx.S002_Queue.isEmpty() );
    }

    void mqCanBeCleared(){
        linx.S002_Queue.clear();
        assertEqual( 0, linx.S002_Queue.size() );
        assertEqual( true, linx.S002_Queue.isEmpty() );
    }

    void mqCanBePopped(){
        linx.S002_Queue.push("Hello World!");
        assertEqual( string("Hello World!"), linx.S002_Queue.pop() );
        assertEqual( 0, linx.S002_Queue.size() );
        assertEqual( true, linx.S002_Queue.isEmpty() );
    }

    void mqSizeAndIsEmptyWorkVar(){
        linx.queueRef(qeS002).clear();
        assertEqual( 0, linx.queueRef(qeS002).size() );
        assertEqual( true, linx.queueRef(qeS002).isEmpty() );
    }

    void mqMessageCanBePushedVar(){
        linx.queueRef(qeS002).push("Hello World!");
        assertEqual( 1, linx.queueRef(qeS002).size() );
        assertEqual( false, linx.queueRef(qeS002).isEmpty() );
    }

    void mqCanBeClearedVar(){
        linx.queueRef(qeS002).clear();
        assertEqual( 0, linx.queueRef(qeS002).size() );
        assertEqual( true, linx.queueRef(qeS002).isEmpty() );
    }

    void mqCanBePoppedVar(){
        linx.queueRef(qeS002).push("Hello World!");
        assertEqual( string("Hello World!"), linx.queueRef(qeS002).pop() );
        assertEqual( 0, linx.queueRef(qeS002).size() );
        assertEqual( true, linx.queueRef(qeS002).isEmpty() );
    }

    void mqS002works() {
        assertEqual( 0, linx.S002_Queue.size() );
        linx.queueRef(qeS002).push("ace");
        linx.queueRef(qeS002).push("wow");
        assertEqual( 2, linx.S002_Queue.size() );
        linx.queueRef(qeS002).clear();
        assertEqual( 0, linx.queueRef(qeS002).size() );
        linx.queueRef(qeS002).push("ace");
        linx.queueRef(qeS002).push("wow");
        assertEqual(linx.queueRef(qeS002).pop(), "ace");
        assertEqual(linx.queueRef(qeS002).pop(), "wow");
    }
    
    void allQueuesCanBeCleared(){
        linx.clearAllQueues();

        linx.S002_Queue.clear();
        linx.S002_Queue.push("Hello World!");
        assertEqual( 1, linx.S002_Queue.size() );

        linx.S013_Queue.clear();
        linx.S013_Queue.push("Hello World!");
        assertEqual( 1, linx.S013_Queue.size() );

        assertEqual( 2, linx.sizeAllQueues() );
        assertEqual( false, linx.allQueuesAreEmpty() );

        linx.clearAllQueues();

        assertEqual( 0, linx.S002_Queue.size() );
        assertEqual( 0, linx.S013_Queue.size() );
        assertEqual( 0, linx.sizeAllQueues() );
        assertEqual( true, linx.allQueuesAreEmpty() );

        #define CLEAR_ADD_CHECK_Q_REF(QUEUE) \
            linx.QUEUE.clear(); \
            linx.QUEUE.push("Hello World!"); \
            assertEqual( 1, linx.QUEUE.size());

        CLEAR_ADD_CHECK_Q_REF(S002_Queue);
        CLEAR_ADD_CHECK_Q_REF(S009_Queue);
        CLEAR_ADD_CHECK_Q_REF(S013_Queue);
        CLEAR_ADD_CHECK_Q_REF(S015_Queue);

        linx.clearAllQueues();
    }

    /// Hub Control

    void hubCanBeStartedAndStopped(long waitFactor){
        assert( !Hub.isRunning() );
        assertMsg( "hithub is already running pre-test", system("ps ux | grep \" [h]ithub\"") != 0 );
        Hub.go().start();
        for (int i = 0; i < (300*waitFactor); i++) {
            TestControl::usnooze(10000);
            if (Hub.isRunning()) break;
        }
        assertMsg( "hithub started but isn't running", Hub.isRunning() );
        Hub.stop();
        for (int i = 0; i < (100*waitFactor); i++) {
            TestControl::usnooze(10000);
            if (!Hub.isRunning()) break;
        }
        assertMsg( "hithub stopped but is still running", !Hub.isRunning() );
        string psCommand = "ps ux | grep \" [h]ithub\" >/dev/null 2>&1";
        if( Hub.getHost() != "localhost" ) psCommand = "ssh -t " + Hub.getHost() + " '" + psCommand + "' >/dev/null 2>&1";
        int status = system(psCommand.c_str());
        assertEqualMsg( "hithub process is still running", 1, WEXITSTATUS(status) );
    }

    void hubCanBeStartedAndStoppedOnTheHost(){
        hubCanBeStartedAndStopped(1);
    }

    void hubCanBeStartedAndStoppedOnTheTarget(){
        hubCanBeStartedAndStopped(10);
    }

    void hubCanBePassedParameters(){
        assert( !Hub.isRunning() );
        assertMsg( "hithub is already running pre-test", system("ps ux | grep \" [h]ithub\"") != 0 );
        Hub.start("-s 1 -t 1");
        TestControl::usnooze(100000);
        assertMsg( "hithub started but isn't running", Hub.isRunning() );
        TestControl::snooze(3);
        assertMsg( "hithub should have stopped after 2 seconds but is still running", !Hub.isRunning() );
   }


    void hubExplicitParametersAreParsedCorrectly (){
        SingleRun & runner = Hub.go();
        string params =
            runner.
                alerts("alerts"). // not in params
                events("events"). // not in params
                name("name"). // not in params
                setConfig("test.cfg").
                startTime("10-Oct-2016 03:41:07"). // this is 1476070867 seconds
                timeDelta(4).
                runTime(1).
                logLevel(3).
                iterations(7000).
                timeRate(60).
                toString();

        assertEqual( string("-c test.cfg -t 1476070867 -d 4 -s 1 -l 3 -i 7000 -r 60"), params );
        assertEqual( string("-t 100"), Hub.go().startTime(100).toString() );
    }


    static bool fexists(const string & filename)
    {
        ifstream ifile(filename.c_str());
        return ifile;
    }

    void hubCanBePassedParametersExplicitly(){
        assertEqual( string("-t 1476070867 -s 1"), Hub.go().startTime("10-Oct-2016 03:41:07").runTime(1).toString() );
        string path = "bin"; // external to the program
        Hub.go().startTime("10-Oct-2016 03:41:07").runTime(1).name("name").alerts("alerts").events("events").start();

        TestControl::usnooze(10000);
        assertMsg( "hithub started but isn't running", Hub.isRunning() );
        TestControl::snooze(3);
        assertMsg( "hithub should have stopped after 1 second but is still running", !Hub.isRunning() );
        string alertPathName = "results/name.alerts";
        assertMsg( "alert log (" + alertPathName + ") not found", fexists( alertPathName ) );
        string eventPathName = "results/name.events";
        assertMsg( "event log (" + eventPathName + ") not found", fexists( eventPathName ) );
    }


    void hubCanBeRunUntilItCompletes(){
        time_t before = time(0);
        Hub.go().runTime(2).run();
        TestControl::usnooze(10000);
        time_t after = time(0);
        assertMsg( "hithub should have taken >1s to execute but instead it took " + to_string((long long int)(after-before)), (after-before) >= 1 );
        assertMsg( "hithub should have stopped after 1 second but is still running", !Hub.isRunning() );
        string path = "bin"; // (default) external to the program
        string alertPathName = Hub.alertLogName;
        assertMsg( "alert log (" + alertPathName + ") not found", fexists( alertPathName ) );
        string eventPathName = Hub.eventLogName;
        assertMsg( "event log (" + eventPathName + ") not found", fexists( eventPathName ) );
    }

    void resetLogFileNames(){
        Hub.go().alerts("").events("").name("").iterations(1).run();
        string alertPathName = "results/resetLogFileNames.alert.log";
        assertMsg( "hub alert file name (" + Hub.alertLogName + ") not reset", Hub.alertLogName == alertPathName );
        string eventPathName = "results/resetLogFileNames.operational.log";
        assertMsg( "hub event file name (" + Hub.eventLogName + ") not reset", Hub.eventLogName == eventPathName );
    }

	void thisTestIsMarkedSomePassing(){
        assertMsg("This test case shouldn't run when the --exclude-some-pass option is used", !theTestEnvironment.excludeSomePassingTests );
    }


    void thisTestIsMarkedFail(){
        assertMsg("This test case shouldn't run when the --exclude-fail option is used", !theTestEnvironment.excludeFailingTests );
    }


    void thisTestIsMarkedTodo(){
        assertMsg("This test case shouldn't run when the --exclude-todo option is used", !theTestEnvironment.excludeTodoTests );
    }


    void thisTestIsMarkedManual(){
        assertMsg("This test case shouldn't run when the --exclude-manual option is used", !theTestEnvironment.excludeManualTests );
    }

    void thisIsASwrtsTest(){
        assertMsg("This test case shouldn't run on a remote host", Hub.getHost() == "localhost" );
    }

    void thisIsASatsTest(){
        assertMsg("This test case shouldn't run on the local host", Hub.getHost() != "localhost" );
    }

    void thisIsAManualSatsTest(){
        assertMsg("This test case shouldn't run on the local host", Hub.getHost() != "localhost" );
        assertMsg("This test case shouldn't run when the --exclude-manual option is used", !theTestEnvironment.excludeManualTests );
    }


    /// Proxy Server Tests
/*
    void testProxyController(){
        assertMsg("Expected proxy controller not to be running by default", ! proxyCtrl.isRunning() );
        proxyCtrl.start();
        assertMsg("Failed to start proxy controller server", proxyCtrl.isRunning() );
        string controllerResponse = proxyCtrl.issueCommand("L1_ON");
        assertEqualMsg("Unexpected response when switching on Linx Primary", controllerResponse, string("True"));
        controllerResponse = proxyCtrl.issueCommand("L2_ON");
        assertEqualMsg("Unexpected response when switching on Linx Secondary", controllerResponse, string("True"));
        controllerResponse = proxyCtrl.issueCommand("L1_OFF");
        assertEqualMsg("Unexpected response when switching off Linx Primary", controllerResponse, string("False"));
        controllerResponse = proxyCtrl.issueCommand("L2_OFF");
        assertEqualMsg("Unexpected response when switching off Linx Secondary", controllerResponse, string("False"));
        controllerResponse = proxyCtrl.issueCommand("L1_ON");
        assertEqualMsg("Unexpected response when switching on Linx Primary", controllerResponse, string("True"));
        controllerResponse = proxyCtrl.issueCommand("L2_ON");
        assertEqualMsg("Unexpected response when switching on Linx Secondary", controllerResponse, string("True"));
        controllerResponse = proxyCtrl.issueCommand("T1_ON");
        assertEqualMsg("Unexpected response when switching on Tranista Primary", controllerResponse, string("True"));
        controllerResponse = proxyCtrl.issueCommand("T2_ON");
        assertEqualMsg("Unexpected response when switching on Tranista Secondary", controllerResponse, string("True"));
        //proxyCtrl.stop();
        //assertMsg("Failed to stop proxy controller server", ! proxyCtrl.isRunning() );
    }

    void isLinkAvailableCanBeReadForLinxAndTranista(){
        //proxyCtrl.start();
        //assertMsg("Failed to start proxy controller server", proxyCtrl.isRunning() );
        assertMsg("LINX Link A should be connected but is not", linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be connected but is not", linx.isLinkAvailable(LINK_B) );
        assertMsg("Tranista Link A should be connected but is not", tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be connected but is not", tranista.isLinkAvailable(LINK_B) );
        assertMsg("LINX should be connected but is not", linx.isLinkAvailable() );
        assertMsg("Tranista should be connected but is not", tranista.isLinkAvailable() );
    }

    void individualLinxLinkCanBeDisconnectedAndConnected(){
        linx.simulateLinkFailure(LINK_A);
        assertMsg("LINX Link A should be disconnected but is not", !linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be connected but is not", linx.isLinkAvailable(LINK_B) );
        linx.simulateLinkRecovery(LINK_A);
        assertMsg("LINX Link A should be connected but is not", linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be connected but is not", linx.isLinkAvailable(LINK_B) );
    }

    void individualTranistaLinkCanBeDisconnectedAndConnected(){
        tranista.simulateLinkFailure(LINK_B);
        assertMsg("Tranista Link A should be connected but is not", tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be disconnected but is not", !tranista.isLinkAvailable(LINK_B) );
        tranista.simulateLinkRecovery(LINK_B);
        assertMsg("Tranista Link A should be connected but is not", tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be connected but is not", tranista.isLinkAvailable(LINK_B) );
    }

    void overallLinxCanBeDisconnectedAndConnected(){
        assertMsg("LINX should be connected but is not", linx.isLinkAvailable() );
        assertMsg("LINX Link A should be connected but is not", linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be connected but is not", linx.isLinkAvailable(LINK_B) );
        linx.simulateLinkFailure();
        assertMsg("LINX should be disconnected but is not", !linx.isLinkAvailable() );
        assertMsg("LINX Link A should be disconnected but is not", !linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be disconnected but is not", !linx.isLinkAvailable(LINK_B) );
        linx.simulateLinkRecovery();
        assertMsg("LINX should be connected but is not", linx.isLinkAvailable() );
        assertMsg("LINX Link A should be connected but is not", linx.isLinkAvailable(LINK_A) );
        assertMsg("LINX Link B should be connected but is not", linx.isLinkAvailable(LINK_B) );
    }

    void overallTranistaCanBeDisconnectedAndConnected(){
        assertMsg("Tranista should be connected but is not", tranista.isLinkAvailable() );
        assertMsg("Tranista Link A should be connected but is not", tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be connected but is not", tranista.isLinkAvailable(LINK_B) );
        tranista.simulateLinkFailure();
        assertMsg("Tranista should be disconnected but is not", !tranista.isLinkAvailable() );
        assertMsg("Tranista Link A should be disconnected but is not", !tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be disconnected but is not", !tranista.isLinkAvailable(LINK_B) );
        tranista.simulateLinkRecovery();
        assertMsg("Tranista should be connected but is not", tranista.isLinkAvailable() );
        assertMsg("Tranista Link A should be connected but is not", tranista.isLinkAvailable(LINK_A) );
        assertMsg("Tranista Link B should be connected but is not", tranista.isLinkAvailable(LINK_B) );
    }

    void proxyControllerCloseProxies(){
        string controllerResponse = proxyCtrl.issueCommand("L1_OFF");
        assertEqualMsg("Unexpected response when switching on Linx Primary", controllerResponse, string("False"));
        controllerResponse = proxyCtrl.issueCommand("L2_OFF");
        assertEqualMsg("Unexpected response when switching on Linx Secondary", controllerResponse, string("False"));
		
		controllerResponse = proxyCtrl.issueCommand("T1_OFF");
        assertEqualMsg("Unexpected response when switching on Tranista Primary", controllerResponse, string("False"));
        controllerResponse = proxyCtrl.issueCommand("T2_OFF");
        assertEqualMsg("Unexpected response when switching on Tranista Secondary", controllerResponse, string("False"));
    }
*/

    /// LogSession Tests

    void logSessionReadsTheOperationalLogCorrectly() {
        //hub->go()->startTime("10-Oct-2016 03:41:07")->iterations(1)->run();
        Hub.go().startTime("10-Oct-2016 03:41:07").iterations(1).run();
        //assertMsg("LogSession has no logs - it should have at least a few", hub->logSession->numLogs() > 0 );
        assertMsg("LogSession has no logs - it should have at least a few", Hub.logSession.numLogs() > 0 );
    }


    void logSessionCanBeFilteredByOneTag() {

        LogSession masterSession;
        Log log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting		Info, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting		Info, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<s001>	Info, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<p001>	Info, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<s001><p001>	Info, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<p001><p002>	Info, Two Tags B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting		Error, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<s001>	Error, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<p001>	Error, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<s001><p001>	Error, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<p001><p002>	Error, Two Tags B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting		Alert, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<s001>	Alert, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<p001>	Alert, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<s001><p001>	Alert, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<p001><p002>	Alert, Two Tags B");  masterSession.push(log);

        log = Log("dateTime	Hitachi_Hub	[debug]	myStatus	<s001>	a message");  masterSession.push(log);
        log = Log("dateTime	myProduct	[trace]	Starting	<s001>	a message");  masterSession.push(log);
        log = Log("dateTime	Hitachi_Hub	[warning]	Starting	<s001>	myMessage");  masterSession.push(log);

        //LogSession* filteredSession = masterSession.filter( (string[]){ "<s001>" }, 1 );
        LogSession & filteredSessioN = masterSession.filter( (string[]){ "<s001>" }, 1 );

        //cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
        //cout << filteredSessioN.toString();
        //cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;

        assertEqualMsg( "incorrect number of logs", 9u, filteredSessioN.numLogs() );
        assertEqualMsg( "incorrect number of infos", 2u, filteredSessioN.numInfos() );
        assertEqualMsg( "incorrect number of errors", (uint)2, filteredSessioN.numErrors() );
        assertEqualMsg( "incorrect number of alerts", (uint)2, filteredSessioN.numAlerts() );

        assertEqualMsg( "incorrect number of debugs", (uint)1, filteredSessioN.numDebugs() );
        assertEqualMsg( "incorrect number of traces", (uint)1, filteredSessioN.numTraces() );
        assertEqualMsg( "incorrect number of warnings", (uint)1, filteredSessioN.numWarnings() );

        //LogIterator* s001Infos = filteredSessioN.getInfos();
        LogIterator & s001Infos = filteredSessioN.getInfos();
        //assertMsg( "info 1 incorrect", s001Infos->next()->tags == "<s001>" );
        assertMsg( "info 1 incorrect", s001Infos.next().tags == "<s001>" );
        assertMsg( "info 2 incorrect", s001Infos.next().tags == "<s001><p001>" );
        LogIterator & s001Errors = filteredSessioN.getErrors();
        assertMsg( "error 1 incorrect", "<s001>" == s001Errors.next().tags );
        assertMsg( "error 2 incorrect", "<s001><p001>" == s001Errors.next().tags );
        LogIterator & s001Alerts = filteredSessioN.getAlerts();
        assertMsg( "alert 1 incorrect", "<s001>" == s001Alerts.next().tags );
        assertMsg( "alert 2 incorrect", "<s001><p001>" == s001Alerts.next().tags );

        LogIterator & s001Debugs = filteredSessioN.getDebugs();
        assertMsg( "debug 1 incorrect", "myStatus" == s001Debugs.next().status );
        LogIterator & s001Traces = filteredSessioN.getTraces();
        assertMsg( "debug 1 incorrect", "myProduct" == s001Traces.next().product );
        LogIterator & s001Warnings = filteredSessioN.getWarnings();
        assertMsg( "debug 1 incorrect", "myMessage" == s001Warnings.next().message );
    }

    void logSessionCanBeFilteredByTwoTags() {

        LogSession masterSession;
        Log log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting		Info, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting		Info, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<s001>	Info, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<p001>	Info, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<s001><p001>	Info, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[info]	Starting	<p001><p002>	Info, Two Tags B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting		Error, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<s001>	Error, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<p001>	Error, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<s001><p001>	Error, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[error]	Starting	<p001><p002>	Error, Two Tags B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting		Alert, No Tag");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<s001>	Alert, One Tag A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<p001>	Alert, One Tag B");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<s001><p001>	Alert, Two Tags A");  masterSession.push(log);
        log = Log("2018-06-21 10:13:25.100 +0000	Hitachi_Hub	[alert]	Starting	<p001><p002>	Alert, Two Tags B");  masterSession.push(log);

        //LogSession* filteredSession = masterSession.filter( (string[]){ "<p001>", "<s001>" }, 2 );
        LogSession & filteredSessioN = masterSession.filter( (string[]){ "<p001>", "<s001>" }, 2 );

        assertEqualMsg( "incorrect number of infos", (uint)1, filteredSessioN.numInfos() );
        assertEqualMsg( "incorrect number of errors", (uint)1, filteredSessioN.numErrors() );
        assertEqualMsg( "incorrect number of alerts", (uint)1, filteredSessioN.numAlerts() );
        LogIterator & s001Infos = filteredSessioN.getInfos();
        assertMsg( "info incorrect", s001Infos.next().tags == "<s001><p001>" );
        LogIterator & s001Errors = filteredSessioN.getErrors();
        assertMsg( "error incorrect", "<s001><p001>" == s001Errors.next().tags );
        LogIterator & s001Alerts = filteredSessioN.getAlerts();
        assertMsg( "alert incorrect", "<s001><p001>" == s001Alerts.next().tags );
    }


    ///
    /// Test case setup and clean up.  CppUnit runs setUp before each test case and tearDown after.
    /// Override if required.
    ///

    void setUp() {}

    void tearDown() {}
};

