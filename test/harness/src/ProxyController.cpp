#include "ProxyController.hpp"
#include <stdexcept>
#include <unistd.h>
#include <iostream>
#include <thread>
#include "logger.hpp"
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <signal.h>
#include <stdio.h>

//using namespace std;

int sysCmdStatus;  // Local variable for LINUX system calls


ProxyController::ProxyController( std::string ipAddress, int basePort )
{
    this->ipAddress = ipAddress;
    this->basePort = basePort;
    sock = -1;
    port = -1;
	isActive = false;
}


ProxyController::~ProxyController()
{
    stop();
}


/*
    Connect to a host on a certain port number
*/
bool ProxyController::conn(std::string address , int port)
{   
	this->port = port;
    this->address = address;

    memset(&server, '0', sizeof(server));

    server.sin_family = AF_INET;
    server.sin_port = htons( port );

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &server.sin_addr)<=0)
    {
        throw runtime_error("Failed to resolve proxy server hostname ad:" + address +  + " po:" + to_string((long long)(port)));
    }	
	
    // Connect to remote server
    for (size_t i = 0; (!socketConnected) && (i < 10); i++) {
        usleep(200000); //Occasionally the socket will exhaust all of its connection attempts before the python script has started. This'll give it a little extra time.
		socketConnected = connect(sock , (struct sockaddr *)&server , sizeof(server)) >= 0;
    }
	
    if (!socketConnected)
    {
        std::string errorStr(strerror(errno));
        throw runtime_error("Failed to connect to proxy server socket: " + errorStr + " ad:" + address + " po:" + to_string((long long)(port)));
    }
    return true;
}


std::string ProxyController::issueCommand(std::string theCommand)
{
    //Send the command	
    if( send(sock , theCommand.c_str() , strlen( theCommand.c_str() ) , 0) < 0)
    {
        std::string errorStr(strerror(errno));
        throw runtime_error("Failed to issue command to proxy server (command failed): " + errorStr );
    }
    LOG_TRACE <<"Data send" << endl;

    // Get the reply
    const size_t buffer_size = 1024;
    char buffer[buffer_size+1];  // TODO Define buffer size somewhere sensible
    std::string reply;

    //Receive a reply from the server
    int len = recv(sock, buffer, buffer_size, 0);
    if( len < 0)
    {
        throw runtime_error("Failed to issue command to proxy server (no reply received)");
    }
    else
    {
        buffer[len] = '\0';
        reply = buffer;
        LOG_TRACE << reply << endl;
    }

    return reply;


}


bool ProxyController::isRunning()
{
    
	if( sock < 0) return false;
    try{
       return issueCommand("Hello") == "Hi";
    }
    catch( const runtime_error& e ){
       return false;
    }
	
	//return isActive;
}

void ProxyController::startProxyServer()
{
	// Start the control server
    //      TODO Look for any other servers using the same port range
	
	std::string sysCmd("python bin/FullProxyServer.py ");
    sysCmd += to_string((long long)(basePort));
    sysCmd += " >/dev/null";
    LOG_DEBUG << sysCmd << std::endl;
	isActive = true;
	sysCmdStatus = system(sysCmd.c_str()); //This line starts the Python server via a shell command. The thread will then pause on this line while the Python server is running.
	isActive = false;
	if (!WIFEXITED(sysCmdStatus))
    {
        throw std::runtime_error("Error calling python proxy controller base:" + to_string((long long)(basePort)));
    }
}


void ProxyController::start()
{
    if( !isRunning() )
    {
        portCheck();
		if( sock < 0 )
        {            
			// Create thread to run the Proxy Server code
			ProxyControllerThread = std::thread(&ProxyController::startProxyServer, this);
            
            // Create socket
            for (size_t i = 0; (sock < 0) && (i < 10); i++) {
                usleep(200000); // Give it time to start up

                sock = socket(AF_INET , SOCK_STREAM , 0);
            }
            		
			
            if (sock < 0)
            {
                throw runtime_error("Could not create socket to proxy server base:" + to_string((long long)(basePort)));
            }
        }        		
		conn( ipAddress, basePort + controlPortOffset );
    }
}

void ProxyController::portCheck()
{
	// Sets up then closes a listening socket.
	// Failure to setup a socket means the port number is in use.
	for (int i = 0; i < 5; i++)
	{
		if (!checkPort(basePort + i))
		{
			if (basePort > 10055)
			{
				throw runtime_error("Error during port checking. BasePort is out of acceptable range.");
			}
			std::cout << "Error on port " << basePort + i << ". Rechecking." << std::endl;
			basePort = basePort + i + 1;
			i = -1;
		}
	}
}

bool ProxyController::checkPort(int port)
{
	// Checks if there any any listening ports
	int ln = socket(AF_INET, SOCK_STREAM, 0);
	if (ln < 0)
	{
		close(ln);
		return false;
	}

	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	if(inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr) <= 0)
	{
		close(ln);
		return false;
	}
	if(::bind(ln, (struct sockaddr*)&sa, sizeof(sa)) < 0)
	{
		close(ln);
		return false;
	} 
	close(ln);
	return true;
}

std::string ProxyController::stop()
{
    if( isRunning() )
    {
        std::string reply = issueCommand("QUIT\n");	
		ProxyControllerThread.join();
		return reply;
    }
	else
	{
		return "Quit";
	}
}


std::string ProxyController::getIpAddress(){ return ipAddress; }

int ProxyController::getBasePort() { return basePort; }

