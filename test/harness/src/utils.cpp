/* utils.cpp
*/

#include <algorithm> // transform
#include <cctype> // tolower
#include <chrono>
#include <stdexcept> // runtime_error
#include <time.h>

#include "utils.hpp"

using namespace std;

namespace utils {

    //-----------------------------------------------------------------------------

    // like split but does not treat multiple delimiters as one
    vector<string> strictSplit(string str, string sep){
        vector<std::string> arr;
        size_t i = 0;
        string s = "";
        while( i < str.length() ){
            bool sepFound = false;
            for( size_t j=0; j<sep.length() && !sepFound; j++ ){
                if( str[i] == sep[j] ) sepFound = true;
            }
            if( !sepFound){ s += str[i]; }
            else{
                arr.push_back(s);
                s = "";
            }
            i++;
        }
        arr.push_back(s);
        return arr;
    }


    //-----------------------------------------------------------------------------

    ///
    /// Removes the first token from the given string
    ///
    bool getToken(const string & s, string & left, string & right, const char * delimiters)
    {
        size_t p = s.find_first_of(delimiters);

        if (string::npos == p) {
            left = s;
            right = "";
            return false;

        } else {
            left = s.substr(0, p);
            right = s.substr(p + 1);
            trimWhitespace(left);
            trimWhitespace(right);
            //cout << "[left:" << left << ",right:" << right << "]";
            return true;
        }
    }


    //-----------------------------------------------------------------------------

    void addFilenameExtension( string & filename, const string & dotExtension )
    {
        filename = filename.substr(0,filename.find_last_of('.')) + dotExtension;
    }


    //-----------------------------------------------------------------------------

    bool stringReplaceToken(std::string& str, const string& token, const string& replacement)
    {
        size_t start_pos = str.find(token);

        if(start_pos == std::string::npos) {
            return false;
        }

        str.replace(start_pos, token.length(), replacement);
        return true;
    }


    //-----------------------------------------------------------------------------

    bool fileReplaceToken(const string & filename, const string & token, const string & replacement )
    {
        bool result = false;

        ifstream infile(filename);
        if (infile.is_open()) {

            const string tempfilename = filename + ".tmp";
            ofstream tempfile(tempfilename);

            if (tempfile.is_open()) {
                string oldline;

                while ( getline(infile, oldline) ) {
                    string newline = oldline;
                    if (stringReplaceToken(newline, token, replacement )) {
                        LOG_TRACE << "replaced:" << token << "=>" << replacement << " in:" << oldline << '\n';
                        result = true;
                    }
                    tempfile << newline << endl;
                }

                tempfile.close();
            }
            infile.close();

            if (result) {
                result = remove(filename.c_str()) == 0;
                result = result && (rename(tempfilename.c_str(), filename.c_str()) == 0);
            }
        }

        return result;
    }


    //-----------------------------------------------------------------------------

    bool copyFile( const string & inFileName, const string & outFileName )
    {
        bool result = false;
        ifstream inFile( inFileName, std::ios::binary );
        if (inFile.is_open()) {
            ofstream outFile( outFileName, std::ios::binary );
            if (outFile.is_open()) {
                outFile << inFile.rdbuf();
                result = true;
            }
        }

        return result;
    }


    //-----------------------------------------------------------------------------

    string to_string( bool x )
    { 
        return x ? "true" : "false"; 
    }


    //-----------------------------------------------------------------------------

    string zeroPadNumber(int num, int digits)
    {
        std::ostringstream ss;
        ss << std::setw( digits ) << std::setfill( '0' ) << num;
        return ss.str();
    }

    //-----------------------------------------------------------------------------

    void makeLowerCase(std::string & s)
    {
        std::transform(s.begin(), s.end(), s.begin(), ::tolower);
    }

    //-----------------------------------------------------------------------------

    typedef std::vector<std::string> StringList;

    void getLineList(const std::string & lines, StringList & lineList)
    {
        lineList.resize(0);
        size_t begin = 0;
        for (;;) {
            size_t end = lines.find('\n', begin);
            if (end == std::string::npos) {
                if (lines.substr(begin) != "") {
                    lineList.push_back(lines.substr(begin));
                }
                break;
            }
            lineList.push_back(lines.substr(begin, end-begin));
            begin = end + 1;
        }
    }
    
    //-----------------------------------------------------------------------------

    void concatLineList(const StringList & lineList, std::string & lines)
    {
        lines = "";
        size_t size = lineList.size();
        if (size > 0) {
            for (size_t i = 0; i < size-1; i++) {
                lines += lineList[i] + "\n";
            }
            lines += lineList[size-1];
        }
    }
    
    //-----------------------------------------------------------------------------

    bool discardIdenticalLines( std::string & left, std::string & right )
    {
        bool result = false;
        
        StringList leftList;
        getLineList(left, leftList);
        StringList rightList;
        getLineList(right, rightList);

        size_t leftSize = leftList.size();
        size_t rightSize = rightList.size();
        size_t sameSize = (leftSize < rightSize) ? leftSize : rightSize;
        
        StringList newLeftList;
        StringList newRightList;
        
        for (size_t i = 0; i < sameSize; i++) {
            if (leftList[i] == rightList[i]) {
                result = true; // discard these identical lines
            } else {
                newLeftList.push_back(leftList[i]);
                newRightList.push_back(rightList[i]);
            }
        }
        
        for (size_t i = sameSize; i < leftSize; i++) {
            newLeftList.push_back(leftList[i]);
        }

        for (size_t i = sameSize; i < rightSize; i++) {
            newRightList.push_back(rightList[i]);
        }

        concatLineList(newLeftList, left);
        concatLineList(newRightList, right);
        
        return result;
    }

    //-----------------------------------------------------------------------------

    static unsigned int alphaTo5BitNumeric(const char alpha)
    {
        unsigned int result = 0;
        if ( ( alpha >= 'A' ) && ( alpha <= 'Z' ) )
        {
            result = alpha - 64;
        }
        // else if ( alpha == ' ' )  // no need for this branch, result is ready.
        return result;
    }

    //-----------------------------------------------------------------------------

    static char base64ToXmlChar(const unsigned int base64)
    {
        char result = '\0';

        if (base64 <= 63)
        {
            if ( base64 == 0 )
            {
                result = '-';
            }
            else if ( base64 <= 10 )
            {
                result = base64 + 47;
            }
            else if ( base64 <= 36 )
            {
                result = base64 + 54;
            }
            else if ( base64 == 37 )
            {
                result = '+';
            }
            else
            {
                result = base64 + 59;
            }
        }
        return result;
    }

    //-----------------------------------------------------------------------------

    std::string generateSenderReference(
        const std::string &headCode,
        const std::string &trainUid,
        const std::string &departCymd,
        const std::string &departHour,
        std::string & result) // : error string
    {
        //
        // Validate inputs, converting string-encoded numerics along the way
        //

        // Validate headCode
        // Head Code must be four chars. Those receive no further range checking.
        if ( headCode.length() != 4 ) {
            return "head code(" + headCode + ") length not 4";
        }
        
        // Validate trainUid
        if (trainUid.length() != 6 ) {
            return "train uid(" + trainUid + ") length not 6";
        }

        char trainUidAlpha = trainUid[0];
        if ( ( trainUidAlpha < 'A' ) || ( trainUidAlpha > 'Z' ) ) {
            return "train uid(" + trainUid + ") [0] not A-Z";
        }
            
        std::string trainUidNumeric = trainUid.substr(1, 5);
        unsigned long trainUidNumValue ;
        try {
            trainUidNumValue = std::stoi(trainUidNumeric);
        }
        catch (std::invalid_argument) {
            return "train uid(" + trainUid + ") [1+] not numeric";
        }

        // Validate departCymd
        // Note that we ignore the first two digits of the four-digit year.
        if (departCymd.length() != 8 ) {
            return "depart cymd(" + departCymd + ") not length 8";
        }
            
        int year(0), month(0), day(0);
        try {
            year = std::stoi(departCymd.substr(2,2));
            month = std::stoi(departCymd.substr(4,2));
            day = std::stoi(departCymd.substr(6,2));
        }
        catch (std::invalid_argument) {
            return "depart cymd(" + departCymd + ") not a numeric date";
        }
        
        if ( ( ( year < 0 )  || ( year > 99  ) ) ||
             ( ( month < 1 ) || ( month > 12 ) ) ||
             ( ( day < 1 )   || ( day  > 31  ) )
           )
        {
            return "depart cymd(" + departCymd + ") invalid date";
        }

        // Validate departHour
        if (( ( departHour.length()) < 1 ) || ( departHour.length() > 2 )) {
            return "depart hour(" + departHour + ") invalid length";
        }
            
        int hourDepartValue = 0;
        try {
            hourDepartValue = std::stoi(departHour);
        }
        catch (std::invalid_argument) {
            return "depart hour(" + departHour + ") not numeric";
        }
        
        if ( ( hourDepartValue < 0 ) || ( hourDepartValue > 23 ) )
        {
            return "depart hour(" + departHour + ") invalid number";
        }

        // Generate last 8 chars
        char appendChar[8 + 1];
        //   1st char from left using Train UID Alpha and am/pm indicator
        unsigned int currentBits = alphaTo5BitNumeric(trainUidAlpha);
        if ( hourDepartValue > 11 )
        {
            currentBits += 0x20;  // Set 6th bit
        }
        appendChar[0] = base64ToXmlChar(currentBits);
        //   Populate 2nd to 4th chars from left using Train UID Numeric
        currentBits = trainUidNumValue & 0x3Fu;  // first 6 bits
        appendChar[3] = base64ToXmlChar(currentBits);
        currentBits = (trainUidNumValue & 0xFC0u) >> 6;  // next 6 bits
        appendChar[2] = base64ToXmlChar(currentBits);
        currentBits = (trainUidNumValue & 0x1F000u) >> 12;  // last 5 bits
        appendChar[1] = base64ToXmlChar(currentBits);
        //   Populate 5th and 6th chars from left using Hour and Day
        appendChar[4] = base64ToXmlChar(hourDepartValue);
        appendChar[5] = base64ToXmlChar(day);
        //   Populate 7th and 8th chars from left using Month and Year
        currentBits = (month << 1) + (year >> 6);
        appendChar[6] = base64ToXmlChar(currentBits);
        appendChar[7] = base64ToXmlChar(year & 0x3Fu);
        appendChar[8] = '\0';

        result = headCode + (std::string)(appendChar);

        return "";
    }

    //-----------------------------------------------------------------------------

    bool utcEpochSecondIsSummerTime( const EpochSecond utcEpochSecond )
    {
        const time_t a_time = static_cast<time_t>(utcEpochSecond);
        const struct tm * a_tm_p = gmtime( &a_time );
        const struct tm & a_tm = *a_tm_p;

        bool isSummerTime = false;

        enum {
            TM_MON_MAR = 3-1,
            TM_MON_OCT = 10-1,
            TM_WEEK_FROM_MON_END = 31-7+1,
            TM_WDAY_SUN = 0,
            TM_HOUR_SPRING_FORWARD = 1,
            TM_HOUR_FALL_BACKWARD = 1
        };

        if ( (a_tm.tm_mon > TM_MON_MAR) && (a_tm.tm_mon < TM_MON_OCT) ) {
            isSummerTime = true;

        } else if ( (a_tm.tm_mon < TM_MON_MAR) || (a_tm.tm_mon > TM_MON_OCT) ) {
            isSummerTime = false;

        } else if (a_tm.tm_mon == TM_MON_MAR) {
            if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
                isSummerTime = false;

            } else if (a_tm.tm_wday > TM_WDAY_SUN) {
                isSummerTime = true;

            } else {
                isSummerTime = a_tm.tm_hour >= TM_HOUR_SPRING_FORWARD;
            }

        } else { // TM_MON_OCT
            if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
                isSummerTime = true;

            } else if (a_tm.tm_wday > TM_WDAY_SUN) {
                isSummerTime = false;

            } else {
                isSummerTime = a_tm.tm_hour < TM_HOUR_FALL_BACKWARD;
            }
        }

        return isSummerTime;
    }

    //-----------------------------------------------------------------------------

    bool ltEpochSecondIsSummerTime( const EpochSecond ltEpochSecond )
    {
        // this won't work during Autumn change over time
        bool result = utcEpochSecondIsSummerTime( ltEpochSecond );
        if (result) {
            if (! utcEpochSecondIsSummerTime( ltEpochSecond + 3600 )) {
                throw runtime_error("ltEpochSecondIsSummerTime at autumn changeover");
            }
        }
        return result;
    }

    //-----------------------------------------------------------------------------

    EpochSecond ltOfUtcEpochSecond( const EpochSecond utcEpochSecond )
    {
        bool isSummerTime = utcEpochSecondIsSummerTime( utcEpochSecond );
        return isSummerTime ? (utcEpochSecond + 3600) : utcEpochSecond;
    }

    //-----------------------------------------------------------------------------

    EpochSecond utcOfLtEpochSecond( const EpochSecond ltEpochSecond )
    {
        bool isSummerTime = ltEpochSecondIsSummerTime( ltEpochSecond );
        return isSummerTime ? (ltEpochSecond - 3600) : ltEpochSecond;
    }

    //-----------------------------------------------------------------------------

    static std::string isoOffset(const EpochSecond epochSecond, const char * format)
    {
        char buf[sizeof "2011-10-08T07:07:09+00:00"];

        const time_t time = static_cast<time_t>(epochSecond);
        strftime(buf, sizeof buf, format, gmtime(&time));

        std::string result(buf);
        return result;
    }

    //-----------------------------------------------------------------------------

    static std::string isoOffset0(const EpochSecond epochSecond)
    {
        return isoOffset(epochSecond, "%FT%T+00:00");
    }

    //-----------------------------------------------------------------------------

    static std::string isoOffset1(const EpochSecond epochSecond)
    {
        return isoOffset(epochSecond, "%FT%T+01:00");
    }

    //-----------------------------------------------------------------------------

    std::string isoDatOfUtcEpochSecond(const EpochSecond utcEpochSecond)
    {
        return isoOffset0(utcEpochSecond);
    }

    //-----------------------------------------------------------------------------

    std::string isoDatLtOfUtcEpochSecond(const EpochSecond utcEpochSecond)
    {
        bool isSummerTime = utcEpochSecondIsSummerTime( utcEpochSecond );
        
        if (isSummerTime) {
            return isoOffset1(utcEpochSecond + 3600);
        } else {
            return isoOffset0(utcEpochSecond);
        }
    }

    //-----------------------------------------------------------------------------

    std::string isoDatLtOfLtEpochSecond(const EpochSecond ltEpochSecond)
    {
        bool isSummerTime = ltEpochSecondIsSummerTime( ltEpochSecond );
        
        if (isSummerTime) {
            return isoOffset1(ltEpochSecond);
        } else {
            return isoOffset0(ltEpochSecond);
        }
    }

    //-----------------------------------------------------------------------------

    EpochSecond utcEpochSecondOfIsoDat(const std::string & isoDateTime)
    {
        // format: 2016-06-10T07:55:30+01:00
        struct tm dateTime_tm = {0};
        bool isSummer = strstr(isoDateTime.c_str(), "+01:00") != 0;
        const char * formatOffset0 = "%Y-%m-%d" "T" "%H:%M:%S+00:00";
        const char * formatOffset1 = "%Y-%m-%d" "T" "%H:%M:%S+01:00";
        const char * format = isSummer ? formatOffset1 : formatOffset0;
        bool result = strptime(isoDateTime.c_str(), format, &dateTime_tm) != NULL;
        
        if (! result) throw runtime_error("utcEpochSecondOfIsoDat date time conversion error");

        time_t time = mktime(&dateTime_tm);
        EpochSecond epochSecond = static_cast<EpochSecond>(time - (isSummer ? 3600 : 0));
        return epochSecond;
    }
    
    //-----------------------------------------------------------------------------

    std::string colonTimeOfIsoDat(const std::string & isoDateTime)
    {
        // format: 2016-06-10T07:55:30
        // index:  0123456789012345678
        
        if (isoDateTime.length() < 18) throw runtime_error("colonTimeOfIsoDat IsoDat too short");
        
        return isoDateTime.substr(11, 8);
    }
    
    //-----------------------------------------------------------------------------

    DaySecond daySecondOfColonTime(const std::string & colonTime)
    {
        // format: 07:55:30
        // index:  01234567
        
        DaySecond daySecond = 
            (colonTime[0] - '0') * 10 * 60 * 60 +
            (colonTime[1] - '0') *  1 * 60 * 60 +
            (colonTime[3] - '0') *  1 * 10 * 60 +
            (colonTime[4] - '0') *  1 *  1 * 60 +
            (colonTime[6] - '0') *  1 *  1 * 10 +
            (colonTime[7] - '0') *  1 *  1 *  1;
            
        return daySecond;
    }
    
    //-----------------------------------------------------------------------------

    long intOf(const std::string & s)
    {
        try {
            return std::stol(s);
        }
        catch (const exception & e) {
            throw runtime_error("utils intOf failed: value (" + s + ") throws: " + e.what());
        }
    }

    //-----------------------------------------------------------------------------

    std::string stringOf(long i)
    {
        return std::to_string(static_cast<long long>(i));
    }

    //-----------------------------------------------------------------------------

    std::string stringOf(const char * s)
    {
        std::string ss(s);
        return ss;
    }

    //-----------------------------------------------------------------------------

    std::string cymdOfEpochday(const EpochDay epochDay)
    {
        char buf[sizeof "ccyymmdd"];

        const EpochSecond epochSecond = epochDay * 24 * 3600;
        const time_t a_time = static_cast<time_t>(epochSecond);
        strftime(buf, sizeof buf, "%Y%m%d", gmtime(&a_time));

        std::string result(buf);
        return result;
    }

    //-----------------------------------------------------------------------------

    std::string bumpCymd(const std::string & cymd, const int days) {
        // cymd format: 20180703
        // indices      01234567

        EpochDay epochDay = dayOfCymd(cymd);
        epochDay += days;
        std::string result = cymdOfEpochday(epochDay);

        return result;
    }

    //-----------------------------------------------------------------------------

    Hms HmsOfDaySecond(DaySecond daySecond)
    {
        const int hour = daySecond / 3600;
        const int minute = (daySecond / 60) % 60;
        const int second = daySecond % 60;
        char cs[100];
        sprintf(cs, "%02d:%02d:%02d", hour, minute, second);
        Hms result = cs;
        return result;
    }

    //-----------------------------------------------------------------------------

    Hms HmsOfTime(const std::string & daySecond)
    {
        if (daySecond == "") return daySecond;
        const DaySecond ds = intOf(daySecond);
        if (ds < 0) return daySecond;
        if (ds >= (24 * 3600)) return daySecond;
        return HmsOfDaySecond(ds);
    }

    //-----------------------------------------------------------------------------

    DaySecond DaySecondOfHms(const Hms & hms)
    {
        // hms format:  17:01:30
        // indices      01234567

        DaySecond result =
            (hms[7] - '0') * 1 +
            (hms[6] - '0') * 10 +
            (hms[4] - '0') * 1 * 60 +
            (hms[3] - '0') * 10 * 60 +
            (hms[1] - '0') * 1 * 60 * 60 +
            (hms[0] - '0') * 10 * 60 * 60;

        return result;
    }

    //-----------------------------------------------------------------------------

    std::string hourOfHms(const Hms & hms) {
        // hms format:  17:01:30
        // indices      01234567

        std::string result = "24";

        result[0] = hms[0];
        result[1] = hms[1];

        return result;
    }

    //-----------------------------------------------------------------------------

    std::string opeDateTimeOfCymdHms(const CymdUtc & cymd, const HmsUtc & hms) {
        // cymd format: 20180703
        // hms format:  17:01:30
        // indices      01234567

        std::string result = "03-JUL-18 01.02.03 AM";
        // indices        01-345-78.01.34.67.90

        // day
        result[0] = cymd[6];
        result[1] = cymd[7];

        // month
        static const char months[] = "--- JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC";
        int month = (cymd[4]-'0') * 10 + (cymd[5]-'0');
        result[3] = months[4 * month + 0];
        result[4] = months[4 * month + 1];
        result[5] = months[4 * month + 2];

        // year
        result[7] = cymd[2];
        result[8] = cymd[3];

        // hour
        int hour = (hms[0]-'0') * 10 + (hms[1]-'0');
        bool pm = hour > 11;
        hour %= 12;
        if (hour == 0) hour = 12;
        result[10] = '0' + (hour / 10);
        result[11] = '0' + (hour % 10);

        // minute
        result[13] = hms[3];
        result[14] = hms[4];

        // second
        result[16] = hms[6];
        result[17] = hms[7];

        // am/pm
        result[19] = pm ? 'P' : 'A';

        return result;
    }

    //-----------------------------------------------------------------------------

    void normaliseCymdDaySec(Cymd & cymd, DaySec & daySec) {
        if (daySec < 0) {
            //COUT( "normaliseCymdDaySec BEFORE cymd(" + cymd + ") daySec(" + stringOf(daySec) + ")");
            int days = 1 + (-daySec / secondsPerDay);
            cymd = bumpCymd(cymd, -days);
            daySec += days * secondsPerDay;
            //COUT( "normaliseCymdDaySec AFTER  cymd(" + cymd + ") daySec(" + stringOf(daySec) + ")");
        }
    }

    //-----------------------------------------------------------------------------

    DaySecUtc UtcDaySecOfLt(const CymdLt & cymdLt, const DaySecLt daySecLt) {
        if (daySecLt < 0) throw runtime_error("UtcDaySecOfLt daySecLt < 0");
        const std::string cymdhmsLt = slashDateOf(cymdLt) + " " + HmsOfDaySecond(daySecLt);
        Seconds secondLt = secondsOfDateTime(cymdhmsLt);
        DaySecUtc daySecUtc = (utcEpochSecondIsSummerTime(secondLt)) ? (daySecLt - 3600) : daySecLt;
        if (daySecUtc < 0) throw runtime_error("UtcDaySecOfLt: summer test time is yesterday");
        return daySecUtc;
    }

    //-----------------------------------------------------------------------------

    HmsUtc UtcHmsOfLt(const CymdLt & cymdLt, const HmsLt & hmsLt) {
        const DaySecLt daySecLt = DaySecondOfHms(hmsLt);
        return HmsOfDaySecond(UtcDaySecOfLt(cymdLt, daySecLt));
    }

    //-----------------------------------------------------------------------------

    CymdUtc UtcCymdOfLt(const CymdLt & cymdLt, const DaySecLt daySecLt) {
        UtcDaySecOfLt(cymdLt, daySecLt); // check hour in range
        return cymdLt; // or throw
    }

    //-----------------------------------------------------------------------------

    CymdUtc UtcCymdOfLt(const CymdLt & cymdLt, const HmsLt & hmsLt) {
        UtcHmsOfLt(cymdLt, hmsLt); // check hour in range
        return cymdLt; // or throw
    }

    //-----------------------------------------------------------------------------

    DaySecLt LtDaySecOfUtc(const CymdUtc & cymdUtc, const DaySecUtc daySecUtc)
    {
        const std::string cymdHmsUtc = slashDateOf(cymdUtc) + " " + HmsOfDaySecond(daySecUtc);
        Seconds secondUtc = secondsOfDateTime(cymdHmsUtc);
        DaySecLt daySecLt = (utcEpochSecondIsSummerTime(secondUtc)) ? (daySecUtc + 3600) : daySecUtc;
        if (daySecLt >= (24 * 3600)) throw runtime_error("LtDaySecOfUtc: summer test time is tomorrow");
        return daySecLt;
    }

    //-----------------------------------------------------------------------------

    HmsLt LtHmsOfUtc(const CymdUtc & cymdUtc, const HmsUtc & hmsUtc)
    {
        const DaySecUtc daySecUtc = DaySecondOfHms(hmsUtc);
        return HmsOfDaySecond(LtDaySecOfUtc(cymdUtc, daySecUtc));
    }

    //-----------------------------------------------------------------------------

    CymdLt LtCymdOfUtc(const CymdUtc & cymdUtc, const DaySecUtc daySecUtc)
    {
        LtDaySecOfUtc(cymdUtc, daySecUtc);
        return cymdUtc; // or throw
    }

    //-----------------------------------------------------------------------------

    CymdLt LtCymdOfUtc(const CymdUtc & cymdUtc, const HmsUtc & hmsUtc)
    {
        LtHmsOfUtc(cymdUtc, hmsUtc);
        return cymdUtc; // or throw
    }

    //-----------------------------------------------------------------------------

    std::string digits4(int x)
    {
        std::string str;

        if (x < 10)
        {
            str = "000" + std::to_string(static_cast<long long>(x));
        }
        else if (x < 100)
        {
            str = "00" + std::to_string(static_cast<long long>(x));
        }
        else if (x < 1000)
        {
            str = "0" + std::to_string(static_cast<long long>(x));
        }
        else
        {
            str = "" + std::to_string(static_cast<long long>(x));
        }

        return str;
    }

    //-----------------------------------------------------------------------------

    std::string concatStrs(const std::string & s, int count) {
        std::string result;
        
        while (count-- > 0) {
            result += s;
        }
        
        return result;
    }
    
    //-----------------------------------------------------------------------------

    std::string beautifyXml(const std::string & xml, const std::string & tab)
    {
        const std::string lines = replaceSubStrings(xml, "><", ">\n<");

        StringList stringList;
        separateStrings(lines, stringList);

        int indents = 0;
        for (size_t i = 0; i < stringList.size(); i++) {
            std::string & line = stringList[i];
            size_t pos = line.find("</");
            indents -= pos == 0;
            line = concatStrs(tab, indents) + line;
            indents += pos == std::string::npos;
        }

        return conflateLines(stringList);
    }
    
    bool validateXmlAgainstXsd(const std::string & xml, const std::string & reportFilename)
    {
        return validateXmlAgainstXsd(xml, "bin/xsdFiles/GB_TAF_TAP_TSI_complete.xsd", reportFilename);
    }
    
    bool validateXmlAgainstXsd(const std::string & xml, const std::string & xsdFilename, const std::string & reportFilename)
    {
        std::string xmllint = "xmllint --noout --schema ";
        std::string command = xmllint + xsdFilename + " - <<<'" + xml + "' 2>" + "/dev/null";

        int cmd = system(command.c_str());

        if (! WIFEXITED(cmd))
        {
            //cout << "[validateXmlAgainstXsd:throw]";
            throw std::runtime_error("Error calling xml validator");
        }

        bool result = (WEXITSTATUS(cmd) == 0);

        /*if (result) {
            cout << "[validateXmlAgainstXsd:ok]";
        } else {
            cout << "[validateXmlAgainstXsd:fail:" << command << "]";
        }*/

        if ((! result) && (reportFilename != "")) {
            std::string command = xmllint + xsdFilename + " - <<<'" + xml + "' 2>" + reportFilename;

            system(command.c_str());
        }

        return result;
    }

    DaySecond DaySecondOfHms12(const std::string & hms12)
    {
        // 212.00.00 AM"
        DaySecond ds = DaySecondOfHms(hms12);
        if (ds >= (secondsPerDay / 2)) {
            ds -= secondsPerDay / 2;
        }
        if (upperCaseOf(hms12).find("PM") != string::npos) {
            ds += secondsPerDay / 2;
        }
        if (ds >= secondsPerDay) {
            ds -= secondsPerDay;
        }
        if ((ds < 0) || (ds >= secondsPerDay)) {
            throw runtime_error("DaySecondOfHms12 (" + hms12 + ") out of range (" + stringOf(ds) + ")");
        }
        return ds;
    }

    //-----------------------------------------------------------------------------

    DaySecond daySecRound(const DaySecond daySecond, const Seconds roundSecs) {
        return daySecond - (daySecond % roundSecs);
    }

    //-----------------------------------------------------------------------------

    Hms HmsOfDaySecondRounded(const DaySecStr daySecStr, const Seconds roundSecs) {
        return HmsOfDaySecond(daySecRound(intOf(daySecStr), roundSecs));
    }

    //-----------------------------------------------------------------------------

    Hms HmsOfDaySecondRounded(const DaySec daySec, const Seconds roundSecs) {
        return HmsOfDaySecond(daySecRound(daySec, roundSecs));
    }

    //-----------------------------------------------------------------------------

    std::string cmydDiff( const Cymd cymd1, const Cymd cymd2) {
        return stringOf(dayOfCymd(cymd1) - dayOfCymd(cymd2));
    }

    //-----------------------------------------------------------------------------

    EpochSecond epochSecondsOfCymdDaySec(const Cymd & cymd, const DaySec daySec) {
        std::string cymdHms = slashDateOf(cymd) + " " + HmsOfDaySecond(daySec);
        EpochSecond epochSecond = secondsOfDateTime(cymdHms);
        return epochSecond;
    }

    //-----------------------------------------------------------------------------

    bool timeIsSummerTime( const EpochSecond utcTime )
    {
        const time_t a_time = static_cast<time_t>(utcTime);
        const struct tm * a_tm_p = gmtime( &a_time );
        const struct tm & a_tm = *a_tm_p;

        bool isSummerTime = false;

        enum {
            TM_MON_MAR = 3-1,
            TM_MON_OCT = 10-1,
            TM_WEEK_FROM_MON_END = 31-7+1,
            TM_WDAY_SUN = 0,
            TM_HOUR_SPRING_FORWARD = 1,
            TM_HOUR_FALL_BACKWARD = 1
        };

        if ( (a_tm.tm_mon > TM_MON_MAR) && (a_tm.tm_mon < TM_MON_OCT) ) {
            isSummerTime = true;

        } else if ( (a_tm.tm_mon < TM_MON_MAR) || (a_tm.tm_mon > TM_MON_OCT) ) {
            isSummerTime = false;

        } else if (a_tm.tm_mon == TM_MON_MAR) {
            if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
                isSummerTime = false;
                
            } else if (a_tm.tm_wday > TM_WDAY_SUN) {
                isSummerTime = true;
                
            } else {
                isSummerTime = a_tm.tm_hour >= TM_HOUR_SPRING_FORWARD;
            }

        } else { // TM_MON_OCT
            if (a_tm.tm_mday < (TM_WEEK_FROM_MON_END + a_tm.tm_wday)) {
                isSummerTime = true;
                
            } else if (a_tm.tm_wday > TM_WDAY_SUN) {
                isSummerTime = false;
                
            } else {
                isSummerTime = a_tm.tm_hour < TM_HOUR_FALL_BACKWARD;
            }
        }

        return isSummerTime;
    }

    //-----------------------------------------------------------------------------

    std::string getFormattedDateTime(const EpochSecond utcTime)
    {
        char buf[sizeof "2011-10-08T07:07:09+01:00"];
        //               0123456789.123456789.1234

        bool isSummerTime = timeIsSummerTime( utcTime );
        
        if (isSummerTime) {
            const time_t a_time = static_cast<time_t>(utcTime + 3600); // advance by 1 hour
            strftime(buf, sizeof buf, "%FT%T+01:00", gmtime(&a_time));
            
        } else {
            const time_t a_time = static_cast<time_t>(utcTime);
            strftime(buf, sizeof buf, "%FT%T+00:00", gmtime(&a_time));
        }

        std::string result(buf);
        return result;
    }

    //-----------------------------------------------------------------------------

    EpochSecond getCurrentTime()
    {
        return getEpochMilliSecond() / 1000;
    }

    //-----------------------------------------------------------------------------

    EpochMilliSecond getEpochMilliSecond()
    {
        return getEpochMilliSecondNow();
    }

    //-----------------------------------------------------------------------------

    EpochMilliSecond getEpochMilliSecondNow()
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>(
                      std::chrono::system_clock::now().time_since_epoch() ).count();
    }

    //-----------------------------------------------------------------------------

    std::string getIso8601Time()
    {
        EpochSecond now = getCurrentTime();
        
        return getFormattedDateTime(now);
    }

    //-----------------------------------------------------------------------------

}

//-----------------------------------------------------------------------------

// eof