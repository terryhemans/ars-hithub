/*  test_runner.cpp
 *
 */

//-----------------------------------------------------------------------------

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <cppunit/TestListener.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TextTestRunner.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestFailure.h>

//-----------------------------------------------------------------------------

#include "TestEnvironment.hpp"
#include "LocalHubController.hpp"
#include "RemoteHubController.hpp"
#include "ConfigFile.hpp"
#include "TestControl.hpp"
#include "utils.hpp"
#include "logger.hpp"

//-----------------------------------------------------------------------------

using namespace std;
using namespace utils;

//-----------------------------------------------------------------------------

// Global test structures
TestEnvironment theTestEnvironment;

//-----------------------------------------------------------------------------

class TestListener : public CppUnit::TestListener
{
    private:
        TestEnvironment & testEnvironment;
        CppUnit::TestResult & event_manager;
        HubController* hub;
        bool failed;

    public:

        TestListener(TestEnvironment & _testEnvironment, CppUnit::TestResult & _event_manager, HubController* h ) :
            testEnvironment(_testEnvironment),
            event_manager(_event_manager),
            hub(h),
            failed(false)
        {}

        void startTest( CppUnit::Test *test ){
            failed = false;
            string name = test->getName();
            const string time = getIso8601Time().substr(0, 19);
            string time1 = replaceSubString(time, "T", " ");
            LOG << time1 << " Test: " << name << ".." << flush;
            hub->testName = name.substr(name.find_last_of(":") + 1);
        }

        void endTest( CppUnit::Test * ){
            if( failed ){
                LOG << "\b\033[1;31m" "FAILED" "\033[0m\n";
            }
            else LOG << "\033[1;32m" "PASSED" "\033[0m\n";
        }

        void addFailure( const CppUnit::TestFailure & ){
            failed = true;
            
            if (theTestEnvironment.stopOnFailure) {
                event_manager.stop();
            }
        }

        void endTestrun( CppUnit::Test *, CppUnit::TestResult * ){}
};


//-----------------------------------------------------------------------------

static void usage(string msg="")
{
    if (msg!="") LOG_ERROR << msg << endl << endl;

    cout << "test [-c test harness config filename(.cfg)] \n"
            "     [-d default hithub config filename(.cfg)] \n"
            "     [-h hithub DB name/pw] \n"
            "     [-l linx UN/PW] \n"
            "     [-q linx queue base name] \n"
            "     [-v verbose test harness output] \n"
            "     [--host=<local|<[user@]ipAddress>>] \n"
            "     [--exclude-pass exclude passing tests] \n"
            "     [--exclude-fail exclude failing tests] \n"
            "     [--exclude-todo exclude todo tests] \n"
            "     [--exclude-manual exclude manual tests] \n"
            "     [--verbose same as -v] \n"
            "     [--debug turn on debug (overrides -v)] \n"
            "     [--trace turn on trace debug (overrides --debug and -v)] \n"
            "     [--quiet turn off all logging except for errors and warnings \n"
            "     [--silent turn off all logging including for errors and warnings \n"
            "     [--stop stop on failure\n"
            ;

    exit(1);
}

//-----------------------------------------------------------------------------

static void handleParameters(int argc, const char* argv[], TestEnvironment & testEnvironment)
{
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') usage();

        string arg = string(argv[i]);
        string value = "";

        #define INC(I) (++i < argc) ? i : (usage(), 0)

        switch (argv[i][1]) {

        case 'c': // test harness config
            testEnvironment.configFilename = argv[INC(i)];
            addFilenameExtension(testEnvironment.configFilename, ".cfg");
            break;

        case 'd': // default hithub config filename
            testEnvironment.configDefaultHithubConfigFilename = argv[INC(i)];
            addFilenameExtension(testEnvironment.configDefaultHithubConfigFilename, ".cfg");
            break;

        case 'h': // hithub DB name/pw
            testEnvironment.configHithub = argv[INC(i)];
            break;

        case 'l': // linx UN/PW
            testEnvironment.configLinx = argv[INC(i)];
            break;

        case 'q': // linx queue name
            testEnvironment.configQueue = argv[INC(i)];
            break;

        case 'v': // verbose
            if( logLevel < _verbose ) logLevel = _verbose;
            break;

        case '-':
            if (arg == "--stop"){ testEnvironment.stopOnFailure = true; }
            else if (arg == "--exclude-pass"){ testEnvironment.excludePassingTests = true; }
            else if (arg == "--exclude-some-pass"){ testEnvironment.excludeSomePassingTests = true; }
            else if (arg == "--exclude-fail"){ testEnvironment.excludeFailingTests = true; }
            else if (arg == "--exclude-todo"){ testEnvironment.excludeTodoTests = true; }
            else if (arg == "--exclude-manual"){ testEnvironment.excludeManualTests = true; }
            else if (arg == "--help"){ usage(); }
            else if (arg == "--verbose"){ if( logLevel < _verbose ) logLevel = _verbose; }
            else if (arg == "--debug"){ logLevel = _debug; LOG_DEBUG << "Debug On" << endl; }
            else if (arg == "--trace"){ logLevel = _trace; LOG_TRACE << "Trace On" << endl; }
            else if (arg == "--quiet"){ logLevel = _off; }
            else if (arg == "--silent"){ logLevel = _silent; }
            else if (arg.substr(0,7) == "--host="){
                value = arg.substr(7);
                if( value == "local" ){ testEnvironment.configHost = "localhost"; }
                else{ testEnvironment.configHost = value; }
            }
            else{
                usage("Unknown option '"+arg+"'" );
            }
            break;

        default:
            usage("Unknown option '"+arg+"'");
        }
    }
}


//-----------------------------------------------------------------------------

static bool setupEnvironment( TestEnvironment* environment, int argc, const char* argv[] )
{
    // defaults
    environment->hubHost = "localhost";

    environment->linxConnection = "77.68.85.175(1414)";
    environment->linxMgr = "QM_APPLE";
    environment->linxChannel = "CHANNEL1";
    environment->linxUser = "hithub";
    environment->linxPwd = "hithub";

    // these are invalid

    environment->linxQueues.S002 = "QS002";
    environment->linxQueues.S009 = "QS009";
    environment->linxQueues.S013 = "QS013";
    environment->linxQueues.S015 = "QS015";

    handleParameters(argc, argv, *environment);

    // apply config file
    if (environment->configFilename != "") {
        LOG_TRACE << "Reading " << environment->configFilename << endl;
        ConfigFile configFile;
        if (! configFile.LoadFile(environment->configFilename)) {
            LOG_ERROR << "Failed to load: " << environment->configFilename << endl;
            return false;
        }

        configFile.OverLoad(environment->linxConnection, "linx.connection.url");
        configFile.OverLoad(environment->linxMgr, "linx.manager");
        configFile.OverLoad(environment->linxChannel, "linx.channel");
        configFile.OverLoad(environment->linxUser, "linx.username");
        configFile.OverLoad(environment->linxPwd, "linx.password");
        configFile.OverLoad(environment->linxQueues.S002, "linx.queue.name.s002");
        configFile.OverLoad(environment->linxQueues.S009, "linx.queue.name.s009");
        configFile.OverLoad(environment->linxQueues.S013, "linx.queue.name.s013");
        configFile.OverLoad(environment->linxQueues.S015, "linx.queue.name.s015");
        configFile.OverLoad(environment->hubHost, "hub.host");
        configFile.OverLoad(environment->proxyHost, "proxy.host");
        configFile.OverLoad(environment->proxyBasePort, "proxy.basePort");
        configFile.OverLoad(environment->hithubConfigFilename, "hithub.config.filename");
    }

    LOG_TRACE << "Applying command line options..." << endl;
    // apply CLT
    if (environment->configLinx != "") {
        LOG_TRACE << "[CLT:overload:linx.username:value:" << environment->linxUser
                  << "=>" << environment->configHithub << "]" << endl;
        environment->linxUser = environment->configLinx;

        LOG_TRACE << "[CLT:overload:linx.password:value:" << environment->linxPwd
                  << "=>" << environment->configHithub << "]" << endl;
        environment->linxPwd = environment->configLinx;
    }

    if (environment->configQueue != "") {
        #define OVERLOAD_LINX( QUEUE ) \
            LOG_TRACE << "[CLT:overload:linx.queue.name." #QUEUE ":value:" << environment->linxQueues.QUEUE \
                      << "=>" << environment->configQueue + environment->linxQueues.QUEUE << "]" << endl; \
            environment->linxQueues.QUEUE = environment->configQueue + environment->linxQueues.QUEUE;

        OVERLOAD_LINX( S002 );
        OVERLOAD_LINX( S009 );
        OVERLOAD_LINX( S013 );
        OVERLOAD_LINX( S015 );
    }

    if (environment->configHost != "") {
        LOG_TRACE << "[CLT:overload:hub.host:value:" << environment->hubHost
                  << "=>" << environment->configHost << "]" << endl;
        environment->hubHost = environment->configHost;
    }

    if (environment->configDefaultHithubConfigFilename != "") {
        LOG_TRACE << "[CLT:overload:hithub.config.filename:" << environment->hithubConfigFilename
                  << "=>" << environment->configDefaultHithubConfigFilename << "]" << endl;
        environment->hithubConfigFilename = environment->configDefaultHithubConfigFilename;
    }

    environment->hithubCfg = new HithubCfg();

    if (environment->hithubConfigFilename == "") { environment->hithubConfigFilename = "hithub.cfg"; }
    LOG_TRACE << "Loading hithub.cfg from " << environment->hithubConfigFilename << endl;

    StringList stringList;
    environment->hithubCfg->loadList(environment->hithubConfigFilename, stringList);
    environment->hithubCfg->createKeyValues(stringList);

    environment->hithubCfg->ref("linx.s002.queue") = environment->linxQueues.S002;
    environment->hithubCfg->ref("linx.s009.queue") = environment->linxQueues.S009;
    environment->hithubCfg->ref("linx.s013.queue") = environment->linxQueues.S013;
    environment->hithubCfg->ref("linx.s015.queue") = environment->linxQueues.S015;
    
    return true;
}

//-----------------------------------------------------------------------------

static void signal_callback_handler(int signum)
{
    printf("Caught signal %d\n",signum);
    exit(signum);
}

//-----------------------------------------------------------------------------

int main(int argc, const char* argv[])
{

    //
    // CppUnit standard main
    //

    signal(SIGINT,  signal_callback_handler);
    signal(SIGTERM, signal_callback_handler);

    //
    // HITHUB specific stuff
    //

    bool wasSucessful = false;

    if (setupEnvironment( &theTestEnvironment, argc, argv )) {

        LOG_VERBOSE << HEADING("Configuration");
        LOG_VERBOSE << endl << theTestEnvironment.toString();
        LOG_VERBOSE << endl << DIVIDER << endl << endl;

        try
        {
            //
            // Initialise the Test Harness objects
            //

            // Construct the hub controller
            LOG_DEBUG << "Constructing the hub...";
            HubController* hub;
            LocalHubController localHub;
            RemoteHubController remoteHub(theTestEnvironment.hubHost);
            if( theTestEnvironment.hubHost == "localhost" ){ hub = &localHub; }
            else{ hub = &remoteHub; }
            hub->installConfigFile( *theTestEnvironment.hithubCfg, "bin/hithub.cfg");
            LOG_DEBUG << "OK" << endl;

            // Construct the Proxy Server Controller
            LOG_DEBUG << "Constructing the Proxy Server..." << endl;
            ProxyController proxy(theTestEnvironment.proxyHost, theTestEnvironment.proxyBasePort);
            LOG_DEBUG << "OK" << endl;

            // Construct the LINX
            LOG_DEBUG << "Constructing the Linx...";
            Linx linx(
                theTestEnvironment.linxConnection,
                theTestEnvironment.linxChannel,
                theTestEnvironment.linxMgr,
                theTestEnvironment.linxUser,
                theTestEnvironment.linxPwd,
                &theTestEnvironment.linxQueues,
                proxy );
            LOG_DEBUG << "OK" << endl;

            // Store objects in the Test Environment
            theTestEnvironment.hub = hub;
            theTestEnvironment.proxyControl = &proxy;
            theTestEnvironment.linx = &linx;

            LOG_VERBOSE << "Running on host " + theTestEnvironment.hub->getHost() << endl;


            //
            // Construct the Test Suite
            //

            LOG_DEBUG << "Constructing the test suite...";

            // Get the top level suite from the registry
            CPPUNIT_NS::Test *suite = CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest();

            CppUnit::TextUi::TestRunner runner;
            runner.addTest(suite);

            // Add our custom progress listener.
            CppUnit::TestResult & event_manager = runner.eventManager();
            event_manager.addListener( new TestListener(theTestEnvironment, event_manager, hub) );

            LOG_TRACE << "OK" << endl;


            //
            // Run the Tests
            //

            LOG_TRACE << "Running the tests..." << endl;

            NEWLINE;
            wasSucessful = runner.run("", false, true, true);

        }
        catch( const std::exception& e ){
            LOG_ERROR << "ERROR! " << e.what() << "\n";
        }

    }

    return wasSucessful ? 0 : 1;
}

//-----------------------------------------------------------------------------

