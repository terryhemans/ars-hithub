//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>
#include <vector>
#include <fstream>
#include "utils.hpp"
#include "logger.hpp"

//-----------------------------------------------------------------------------

using namespace utils;

//-----------------------------------------------------------------------------

class KeyValue {
public:
    string key;
    string value;
    
public:
    KeyValue() {}
    
    KeyValue( const string & _key, const string & _value ) {
        key = _key;
        value = _value;
    }
};

//-----------------------------------------------------------------------------

typedef std::vector<KeyValue> KeyValues;

//-----------------------------------------------------------------------------

class ConfigFile {

protected:

    KeyValues keyValues;

public:

    bool LoadFile( std::string filename )
    {
        ifstream file(filename);

        if(file)
        {
            int lineNum = 0;

            string rawLine;
            string line;

            while( getline(file, rawLine) )
            {
                lineNum++;
                line = rawLine.substr( 0, rawLine.find_first_of("#") ); // ignore comments
                trimWhitespace(line);
                if( line.length() > 0 )
                {
                    KeyValue keyValue;
                    getToken(line, keyValue.key, keyValue.value);

                    //cout << "[Add key:" << keyValue.key << ",value:" << keyValue.value << "]" << endl;
                    if (findKey(keyValue.key)) {
                        LOG_WARNING << "key:" << keyValue.key << " already exists" << endl;
                        return false;
                    }
                    keyValues.push_back( keyValue );
                }
            }
            return true;
        }
        return false;
    }


    bool findKey(const string & key) const
    {
        unsigned int size = keyValues.size();

        for( unsigned int i=0; i<size; i++ )
        {
            const KeyValue & keyValue = keyValues.at(i);
            if( upperCaseOf(keyValue.key) == upperCaseOf(key) )
            {
                //cout << "[found key:" << keyValue.key << ",value:" << keyValue.value << "]" << endl;
                return true;
            }
        }

        return false;
    }


    bool getValue(const string & key, string & value) const
    {
        unsigned int size = keyValues.size();

        for( unsigned int i=0; i<size; i++ )
        {
            const KeyValue & keyValue = keyValues.at(i);
            if( upperCaseOf(keyValue.key) == upperCaseOf(key) )
            {
                //cout << "[found key:" << keyValue.key << ",value:" << keyValue.value << "]" << endl;
                value = keyValue.value;
                return true;
            }
        }

        return false;
    }


    bool OverLoad( string & s, const string & key )
    {
        string value;

        if (getValue(key, value)) {
            LOG_TRACE << "[config:overload:key:" << key << ",value:" << s << "=>" << value << "]" << endl;
            s = value;
            return true;
        }

        return false;
    }

    bool OverLoad( int & i, const string & key )
    {
        string value;

        if (getValue(key, value)) {
            i = stoi(value);
            LOG_TRACE << "[config:overload:key:" << key << ",value:" << i << "=>" << value << "]" << endl;
            return true;
        }

        return false;
    }

};

//-----------------------------------------------------------------------------
