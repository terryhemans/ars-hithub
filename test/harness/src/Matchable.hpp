#pragma once

#include <iostream>
#include <string>

using namespace std;


///
/// Holds an array of field names.  When used in conjunction with a Matchable object's matches function, this class
/// holds the names of the fields to be excluded from a 'matches' comparison.
///
class Mask
{

protected:

    string* maskedFields;
    int numberOfMaskedFields;


public:

    ///
    /// constructor.  Pass an array of the field names to mask
    ///
    Mask( string* fieldsToMask, int size ) :
        numberOfMaskedFields(size)
    {
        maskedFields = new string[size];
        for( int i=0; i<numberOfMaskedFields; i++ ){
            maskedFields[i] = fieldsToMask[i];
        }
    }


    ///
    /// returns true if the given field name is masked
    ///
    bool isMasked( string field ) const
    {
        for( int i=0; i<numberOfMaskedFields; i++ ){
            if( maskedFields[i] == field ) return true;
        }
        return false;
    }

};


///
/// Interface: Matchable defines objects that can be compared with a field Mask
///
class Matchable
{

public:

    virtual bool matches( const Matchable& rhs, const Mask* mask ) const = 0;

    virtual string toString(const string & equal = "=> ", const string & comma = ",", const string & end = "\n") const = 0;

};

