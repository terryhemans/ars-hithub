/* HubController.cpp
*/

#include <unistd.h>
#include <fcntl.h>
#include <stdexcept>
#include <signal.h>
#include <sys/wait.h>
#include <iostream>
#include <vector>
#include <string.h>
#include <sstream>
#include <locale>
#include <iomanip>
#include <fstream>

#include "HubController.hpp"
#include "logger.hpp"
#include "utils.hpp"

using namespace utils;

void HubController::startProcess(const string &command, const string &args, const string &runInDir, const string &alertlog, const string &eventlog, const string &name)
{
	string resultsDir = "results/";

    char* argv[100+1];

    argv[0] = (char*)"hithub";

    char * cargs = strdup(args.c_str());

    int index = 1;

    char * current = strtok(cargs," " "\t");
    while ((index < 100) && (current != NULL)) {
        argv[index++] = current;
        current = strtok(NULL, " " "\t");
    }

    argv[index] = NULL;

    if( !running ){
        alertLogName = resultsDir;
        eventLogName = resultsDir;
        if (name != "") {
            alertLogName += name + ".";
            eventLogName += name + ".";
        }
        alertLogName += (alertlog != "") ? alertlog : testName + ".alert.log";
        eventLogName += (eventlog != "") ? eventlog : testName + ".operational.log";

        logSession.init( eventLogName, alertLogName );

        hithubPID = fork();

        switch( hithubPID ){
            case -1:  // must be in parent process
                throw runtime_error("Could not fork HitHub");

            case 0:   // must be in child process
                {
                    int fd_stdout = open(eventLogName.c_str(), O_APPEND | O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
                    if( fd_stdout < 0 ) throw runtime_error("Cannot open hithub's " + eventLogName + ": error code " + to_string((long long int)errno));
                    int fd_stderr = open(alertLogName.c_str(), O_APPEND | O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
                    if( fd_stderr < 0 ) throw runtime_error("Cannot open hithub's " + alertLogName + ": error code " + to_string((long long int)errno));
                    if( dup2(fd_stdout, 1) < 0 ) throw runtime_error("Cannot redirect hithub's stdout: error code " + to_string((long long int)errno));
                    if( dup2(fd_stderr, 2) < 0 ) throw runtime_error("Cannot redirect hithub's stderr: error code " + to_string((long long int)errno));

                    LOG_DEBUG << command << " " << args << endl;

                    if( runInDir != "" ) chdir(runInDir.c_str());
                    execvp(command.c_str(), argv);
                    exit(1);
                }

            default:  // must be in parent process
                running = true;
        }
    }
    else{
        throw runtime_error("hithub already running");
    }
}


void HubController::stop()
{
    if( running ){
        kill(hithubPID, 15);
        waitpid(hithubPID, NULL, 0);
        running = false;
    }
}


void HubController::run(const string &params, const string &alertlog, const string &eventlog, const string &name )
{
    start(params, alertlog, eventlog, name);
    int status = 0;
    waitpid(hithubPID, &status, 0);
    if( !WIFEXITED(status) ) throw runtime_error( "hithub process did not exit normally (WIFEXITED=false)" );
    if( WEXITSTATUS(status) != 0 ) throw runtime_error( "hithub generated exit code " + to_string((long long)WEXITSTATUS(status)) + ".  See alert log." );
    running = false;
}


bool HubController::isRunning()
{
    if( running && ( waitpid(hithubPID, NULL, WNOHANG) > 0 ) ) running = false;
    return running;
}


void HubController::installConfigFile( HithubConfigFile & cfg, string destinationFile ){
    string targetFile = (destinationFile != "") ? destinationFile : "bin/hithub.cfg";
    //LOG_TRACE << "installing " << targetFile << " with:\n" << cfg.toString() << "\n";
    string tempFilename = "_hithub.cfg";
    ofstream configFile;
    configFile.open (tempFilename);
    configFile << cfg.toString();
    configFile.close();
    installConfigFile( tempFilename, targetFile );
}



//-----------------------------------------------------------------------------

SingleRun & HubController::go()
{
    return SingleRun::getInstance(this); 
}


//-----------------------------------------------------------------------------

//  SingleRun

//-----------------------------------------------------------------------------

SingleRun & SingleRun::setConfig( const string & configfile )
{
    if( params.length() > 0 ) params += " ";
    params += "-c " + configfile;
    return instance;
}

//-----------------------------------------------------------------------------

SingleRun & SingleRun::areIdentities( const string & filename )
{
    if( params.length() > 0 ) params += " ";
    params += "-ca " + filename;
    return instance;
}

//-----------------------------------------------------------------------------

SingleRun & SingleRun::berthSteps( const string & filename )
{
    if( params.length() > 0 ) params += " ";
    params += "-cb " + filename;
    return instance;
}

//-----------------------------------------------------------------------------

SingleRun & SingleRun::transitionBerths( const string & filename )
{
    if( params.length() > 0 ) params += " ";
    params += "-ct " + filename;
    return instance;
}

//-----------------------------------------------------------------------------

SingleRun & SingleRun::iterations( int iterations )
{
    if( params.length() > 0 ) params += " ";
    params += "-i " + to_string((long long int)iterations);
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::startTime( const string & time )
{
    tm t = {0,0,0,0,0,0,0,0,0};
    string utcTime = time + " UTC";
    if( strptime(utcTime.c_str(), "%d-%b-%Y %H:%M:%S %Z", &t) == NULL ){
        throw runtime_error("Time string must be formatted like '21-Nov-2017 05:37:21'");
    }
    
    time_t secondsSinceEpoch = mktime(&t);
    if( params.length() > 0 ) params += " ";
    params += "-t " + to_string((long long int)secondsSinceEpoch);
    
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::startTime( int seconds )
{
    if( params.length() > 0 ) params += " ";
    params += "-t " + to_string((long long int)seconds);
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::logLevel( int level ){
    if( params.length() > 0 ) params += " ";
    params += "-l " + to_string((long long int)level);
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::runTime( int seconds )
{
    if( params.length() > 0 ) params += " ";
    params += "-s " + to_string((long long int)seconds);
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::timeDelta( int seconds )
{
    if( params.length() > 0 ) params += " ";
    params += "-d " + to_string((long long int)seconds);
    return instance;
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::timeRate( int rate )
{
    if( params.length() > 0 ) params += " ";
    params += "-r " + to_string((long long int)rate);
    return instance;
}

//-----------------------------------------------------------------------------

static int fileFindKeys(const string & filename, const string & key1, const string & key2 = "", const string & key3 = "")
{
    int result = 0;

    ifstream infile(filename);
    if (infile.is_open()) {

        const string KEY1 = upperCaseOf(key1);
        const string KEY2 = upperCaseOf(key2);
        const string KEY3 = upperCaseOf(key3);
        
        //cout << "fileFindKeys KEY1(" << KEY1 << ") KEY2(" << KEY2 << ") KEY3(" << KEY3 << ")" << endl;
        
        string line;

        while ( getline(infile, line) ) {
            makeUpperCase(line);
            
            //cout << "fileFindKeys line(" << line << ")" << endl;

            size_t key1_pos = line.find(KEY1);

            if (key1_pos != std::string::npos) {
                //cout << "fileFindKeys found: KEY1(" << KEY1 << ")" << endl;

                if (key2 == "") {
                    result++;
                    
                } else {
                    size_t key2_pos = line.find(KEY2, key1_pos + KEY1.length());

                    if (key2_pos != std::string::npos) {
                        //cout << "fileFindKeys found: KEY2(" << KEY2 << ")" << endl;

                        if (key3 == "") {
                            result++;
                            
                        } else {
                            size_t key3_pos = line.find(KEY3, key1_pos + KEY2.length());

                            if (key3_pos != std::string::npos) {
                                //cout << "fileFindKeys found: KEY3(" << KEY3 << ")" << endl;
                                
                                result++;
                            }
                        }
                    }
                }
            }
        }
        infile.close();
    }

    return result;
}

//-----------------------------------------------------------------------------

int SingleRun::searchAlerts( const string & key1, const string & key2, const string & key3 )
{
    const string & filename = hub->alertLogName;
    return fileFindKeys(filename, key1, key2, key3);
}

//-----------------------------------------------------------------------------

int SingleRun::searchEvents( const string & key1, const string & key2, const string & key3 )
{
    string filename = hub->eventLogName;
    return fileFindKeys(filename, key1, key2, key3);
}

//-----------------------------------------------------------------------------

string SingleRun::toString(){ return params; }


//-----------------------------------------------------------------------------

void SingleRun::start()
{
    hub->start(params, m_alertLog, m_eventLog, m_name);
}


//-----------------------------------------------------------------------------

void SingleRun::run()
{
    hub->run(params, m_alertLog, m_eventLog, m_name);
}


//-----------------------------------------------------------------------------

SingleRun & SingleRun::getInstance( HubController* hub )
{
    instance.hub = hub;
    instance.params = "";

    return instance;
}

//-----------------------------------------------------------------------------

SingleRun & SingleRun::instance( * new SingleRun() );
