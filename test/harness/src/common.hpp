//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- (C) 2017 Hitachi, Ltd.  All rights reserved.
//-
//- $Id: hub.hpp 237 2017-06-08 09:58:55Z david $
//-
//- $URL: svn+ssh://jules@77.68.12.143/var/svn/repos/hithub/branches/jules/src/forecast.hpp $
//-
//- DESCRIPTION:
//- Declaration of common functions.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef COMMON_HPP
#define COMMON_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <stdexcept>
#include <string>

#include "hithub_types.hpp"

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

/// return whether the string just contains digits
///
extern bool isDigits(const std::string & str);
extern bool isDigits(const char * p);

/// return whether the string is valid cymd
///
//not implemented extern bool cymdIsValid( const std::string & cymd);

/// return a ccyymmdd string of dd-mon-yy, ignoring separator character
///
/// input:  "dd-mon-yy"
/// output: "ccyymmdd"
///
extern std::string cymdOfDdMonYy(const std::string & ddMonYy);

/// return a ccyymmdd string of ccyy/mm/dd, ignoring separator character
///
/// input:  "ccyy-mm-dd"
/// output: "ccyymmdd"
///
extern std::string cymdOfNiceDate(const std::string & niceDate);

/// return a ccyy-mm-dd string of ccyymmdd, using separator character
///
/// input:  "ccyymmdd"
/// output: "ccyy-mm-dd"
///
extern std::string niceDateOf(const std::string & ccyymmdd, char separator);

/// return a ccyy-mm-dd string of ccyymmdd
///
/// input:  "ccyymmdd"
/// output: "ccyy-mm-dd"
///
extern std::string dashDateOf(const std::string & ccyymmdd);

/// return a ccyy/mm/dd string of ccyymmdd
///
/// input:  "ccyymmdd"
/// output: "ccyy/mm/dd"
///
extern std::string slashDateOf(const std::string & ccyymmdd);

/// return a HH:MM:SS string of daySecondStr
///
extern std::string colonTimeOfDaySecond(const std::string & daySecondStr);

/// return a HH:MM:SS string of daySecond
///
extern std::string colonTimeOfDaySecond(DaySecond daySecond);

/// return whether the conversion of dateTime to epoch seconds succeeded
/// formatted expected: 2018/12/31 23:59:59
///
extern bool secondsOfDateTime(const std::string & dateTime, Seconds & seconds );

/// return epoch seconds of dateTime, formatted expected: 2018/12/31 23:59:59
/// 0 on conversion error
///
extern Seconds secondsOfDateTime(const std::string & dateTime );

/// return whether the conversion of ccyymmdd to epoch day succeeded
/// formatted expected: 20181231
///
extern bool dayOfCymd(const std::string & ccyymmdd, EpochDay & epochDay );

/// return epoch day of ccyymmdd, formatted expected: 20181231
/// 0 on conversion error
///
extern EpochDay dayOfCymd(const std::string & ccyymmdd );

/// return a string of length, filled with fill_ch
///
extern std::string leftOf(const std::string & s, size_t length, char fill_ch = ' ');

/// return a string of at least 2 digits for the given value e.g. 01
///
extern std::string digits2(int x);

/// return a string of at least 3 digits for the given value e.g. 007
///
extern std::string digits3(int x);

/// trim the set of characters from the beginning of the given string
///
extern void trimFirstSet(std::string& s, const char set[]);

/// return the given string with the given set trimmed from the beginning
///
extern std::string trimFirstSetFrom(const std::string& s, const char set[]);

/// return the given string with the leading zeros trimmed from the beginning
///
extern std::string trimLeadingZerosFrom(const std::string& s);

/// trim the set of characters from the beginning and the end of the given string
///
extern void trimSet(std::string& s, const char set[]);

/// return the given string with the given set trimmed from the beginning and the end
///
extern std::string trimSetFrom(const std::string& s, const char set[]);

/// trim whitespace from the beginning and the end of the given string
///
extern void trimWhitespace(std::string& s);

/// return the given string with whitespace trimmed from the beginning and the end
///
extern std::string trimWhitespaceFrom(const std::string& s);

/// make the given string lower case
///
extern void makeLowerCase(std::string & s);

/// return a lower case version of the given string
///
extern std::string lowerCaseOf(const std::string & s);

/// make the given string upper case
///
extern void makeUpperCase(std::string & s);

/// return an upper case version of the given string
///
extern std::string upperCaseOf(const std::string & s);

/// return whether string is a boolean, and the resultant value
///
extern bool boolOfString(const std::string& boolString, bool & returnBool);

/// return whether string is a number, and the resultant value
///
extern bool longOfString(const std::string& numericString, long & value);

/// return skip past whitespace
///
extern const char * skipWhitespace(const char * p);

/// return the first token from the text, increment the text pointer
///
/// a token does not contain leading and trailing whitespace
///
/// tokens are separated by any one char in separator set
///
/// text format: A, B ,C
///
extern std::string removeToken(const char *& text_p, const char separatorSet[] = "!\"£$%^&*()-=+{}[]:@~;'#<>?,./");

/// return the first whitespace-token from the text, increment the text pointer
///
/// whitespace-token is a token that may contain leading and trailing whitespace
///
/// tokens are separated by any one char in separator set
///
/// text format: A, B ,C
///
extern std::string removeWhitespaceToken(const char *& text_p, const char separatorSet[] = "!\"£$%^&*()-=+{}[]:@~;'#<>?,./");

/// return whether the token was found and skipped in the text, increment the text pointer
///
/// text format: data-token-data
///
extern bool skipToken(const char *& text_p, const char token[]);

/// return whether the whitespace-token was found and skipped in the text, increment the text pointer
///
/// text format: data-token-data
///
extern bool skipWhitespaceToken(const char *& text_p, const char token[]);

/// replace all occurrences of findstr with replacestr in str
///
/// example: replaceSubStrings(str, "\r", "<CR>");
///
extern void replaceSubStrings(std::string & str, const std::string & findStr, const std::string & replaceStr = "");

/// return all occurrences of findstr in orgStr replaced with replacestr
///
/// example: str = replaceSubStrings(str, "\r", "<CR>");
///
extern std::string replaceSubStrings(const std::string & orgStr, const std::string & findStr, const std::string & replaceStr = "");

/// replace the first occurrence of findstr with replacestr in str : did
///
/// example: replaceSubString(str, "\r", "<CR>");
///
extern bool replaceSubString(std::string & str, const std::string & findStr, const std::string & replaceStr = "");

/// return the first occurrence of findstr in orgStr replaced with replacestr
///
/// example: str = replaceSubString(str, "\r", "<CR>");
///
extern std::string replaceSubString(const std::string & orgStr, const std::string & findStr, const std::string & replaceStr = "");

/// replace the tail of str containing findstr with replacestr in str : did
///
/// example: replaceTailString(str, " +01:00");
///
extern bool replaceTailString(std::string & str, const std::string & findStr, const std::string & replaceStr = "");

/// return a string with control characters of orgStr replaced with <CR>, <NL> etc
///
/// example: str = visibleControlsOf("\n" "ace" "\r") returns "<NL>ace<CR>"
///
extern std::string visibleControlsOf(const std::string & orgStr);

/// return the conflated strings with optional separator
///
/// example: StringList sl; sl.push_back("ace"); sl.push_back("zip"); s = conflateStrings(sl, "\n");
/// s contains "ace" "\n" "wow"
///
extern std::string conflateStrings(const StringList & stringList, const std::string & separator = "");

/// return the conflated lines
///
/// example: StringList sl; sl.push_back("ace"); sl.push_back("zip"); s = conflateLines(sl);
/// s contains "ace" "\n" "wow"
///
extern std::string conflateLines(const StringList & lineList);

/// return the strings separated into a list with optional separator
///
/// example: s = "ace" "\n" "wow"; StringList sl; separateStrings(s, sl, "\n");
/// sl[0] contains "ace"  sl[1] contains "wow"
///
extern void separateStrings(const std::string & strings, StringList & stringList, const std::string & separator = "\n");

/// return the strings separated into a list with optional separator
///
/// example: s = "ace" "\n" "wow"; StringList sl; separateLines(s, sl);
/// sl[0] contains "ace"  sl[1] contains "wow"
///
extern void separateLines(const std::string & lines, StringList & lineList);

//-----------------------------------------------------------------------------

#endif // COMMON_HPP
