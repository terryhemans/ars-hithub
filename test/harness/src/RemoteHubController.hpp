#pragma once

#include "HubController.hpp"
#include <stdexcept>
#include "logger.hpp"

using namespace std;


///
/// HubControl
///
class RemoteHubController : public HubController
{

public:


    RemoteHubController( string ipAddr ) : HubController(), ipAddress(ipAddr) {}
    ~RemoteHubController() { stop(); }

    ///
    /// starts the hub in a background process.  Takes an optional parameter line.
    /// Hub is left running until it ends or stop() is called.
    ///
    void start( const string &params = "" ,
                const string &alertlog = "",
                const string &eventlog = "",
                const string &name = "" )
    {
        string hubCommandline = "./hithub " + params;
        string sshArgs = "" + ipAddress + " cd bin; export LD_LIBRARY_PATH=/usr/local/lib:/opt/mqm/lib64:/opt/hithub/lib/oracle; " + hubCommandline;
        startProcess( "ssh", sshArgs, "", alertlog, eventlog, name );
    }


    ///
    /// returns whether the Hub executable is on the local host or a remote host (e.g. VM)
    ///
    string getHost() { return ipAddress; }


    ///
    /// copies the given source file to the destination on the remote host ready for testing
    ///
    void installConfigFile( string sourceFile, string destinationFile = "" )
    {
        if( destinationFile == "" ) destinationFile = "~/bin";
        LOG_DEBUG << "Installing config file: " << sourceFile << endl;
        string command = "scp " + sourceFile + " " + ipAddress + ":" + destinationFile + " >/dev/null";
        LOG_TRACE << command << endl;
        int exitCode = system(command.c_str());
        if( exitCode != 0 ) throw runtime_error("failed to install config file: '" + command + "' failed with exit code " + to_string((long long)exitCode) );
    }


private:

    string ipAddress;

};


