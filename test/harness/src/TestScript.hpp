#pragma once

#include <cppunit/extensions/HelperMacros.h>

#include "TestEnvironment.hpp"
//#include "Tranista.hpp"
#include "Linx.hpp"
#include "LocalHubController.hpp"
#include "TestControl.hpp"
//#include "TrainJourney.hpp"
#include "logger.hpp"
#include "utils.hpp"
#include "assertions.hpp"


using namespace std;
using namespace utils;


extern TestEnvironment theTestEnvironment;


class TestScript : public CppUnit::TestFixture
{

protected:

    TestEnvironment & testEnvironment;
    HubController & Hub;
    ProxyController & proxyCtrl;
    //Tranista & tranista;
    Linx & linx;
    HithubCfg & hithubCfg;

    bool firstTestCase;

public:


    TestScript() :
        testEnvironment(theTestEnvironment),
        Hub(* theTestEnvironment.hub),
        proxyCtrl(* theTestEnvironment.proxyControl),
        //tranista(* theTestEnvironment.tranista),
        linx(* theTestEnvironment.linx),
        hithubCfg(* theTestEnvironment.hithubCfg),
        firstTestCase(true)
    {}

};
