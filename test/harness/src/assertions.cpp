#include "assertions.hpp"

//extern template <class T>
//void assertNotEqual( const T& expected, const T& actual ){
//    assert( expected != actual );
//}

void assertMatchesMsg( string message, const Matchable& lhs, const Matchable& rhs, const Mask* mask )
{
    if( message != "" ) message += ": ";
    bool matches = lhs.matches(rhs, mask);
    if (!matches) {
        string left = lhs.toString();
        string right = rhs.toString();
        utils::discardIdenticalLines( left, right );
        assertMsg( message + "expected: " + left + "\nactual: " + right, lhs.matches(rhs, mask) );
    }
}

void assertMatches( const Matchable& lhs, const Matchable& rhs, const Mask* mask )
{
    assertMatchesMsg( "", lhs, rhs, mask );
}

void assertNotMatchesMsg( string message, const Matchable& lhs, const Matchable& rhs, const Mask* mask )
{
    if( message != "" ) message += ": ";
    assertMsg( message + "expected objects to be different: " +lhs.toString(), !lhs.matches(rhs, mask) );
}

void assertNotMatches( const Matchable& lhs, const Matchable& rhs, const Mask* mask )
{
    assertNotMatchesMsg( "", lhs, rhs, mask );
}

