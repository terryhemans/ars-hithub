#pragma once

#include <imqi.hpp>
#include <string>

//#include "XmlMessage.hpp"

using namespace std;

class LinxMQ
{

protected:

    ImqQueue queue;

public:

    const string name;


    LinxMQ( ImqQueueManager* mgr, string queueName );


    ///
    /// initialise the queue
    ///
    void initialise();


    ///
    /// returns the number of messages on the queue
    ///
    int size();


    ///
    /// returns true if the queue is empty
    ///
    bool isEmpty();


    ///
    /// Deletes all messages on the queue
    ///
    void clear();


    ///
    /// adds the given message to the queue
    ///
    void push( const string & msg );


    ///
    /// returns the next message on the queue
    ///
    string pop();


    ///
    /// returns the next message on the queue as an XmlMessage
    ///
    //XmlMessage* popXML( XmlMessage* msg );

    ///
    /// provides the name of the table
    ///
    const string & getName() { return name; }
};
