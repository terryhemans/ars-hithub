#pragma once

#include <vector>
#include <string.h>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "common.hpp"
#include "logger.hpp"

using namespace std;

namespace utils {

    //-----------------------------------------------------------------------------


    // like split but does not treat multiple delimiters as one
    vector<string> strictSplit(string str, string sep);

    ///
    /// Removes the first token from the given string
    ///
    bool getToken(const string & s, string & left, string & right, const char * delimiters = ",=");

    void addFilenameExtension( string & filename, const string & dotExtension );

    bool stringReplaceToken(std::string& str, const string& token, const string& replacement);

    bool fileReplaceToken(const string & filename, const string & token, const string & replacement );

    bool copyFile( const string & inFileName, const string & outFileName );

    string to_string( bool x );

    string zeroPadNumber(int num, int digits);

    bool discardIdenticalLines( std::string & left, std::string & right );

    std::string generateSenderReference(
        const std::string &headCode,
        const std::string &trainUid,
        const std::string &departCymd,
        const std::string &departHour,
        std::string & result); // : error string

    bool utcEpochSecondIsSummerTime( const EpochSecond utcEpochSecond );

    bool ltEpochSecondIsSummerTime( const EpochSecond ltEpochSecond ); // can throw

    EpochSecond ltOfUtcEpochSecond( const EpochSecond utcEpochSecond );

    EpochSecond utcOfLtEpochSecond( const EpochSecond ltEpochSecond );

    std::string isoDatOfUtcEpochSecond(const EpochSecond utcEpochSecond);

    std::string isoDatLtOfUtcEpochSecond(const EpochSecond utcEpochSecond);

    std::string isoDatLtOfLtEpochSecond(const EpochSecond ltEpochSecond);

    EpochSecond utcEpochSecondOfIsoDat(const std::string & isoDateTime);

    std::string colonTimeOfIsoDat(const std::string & isoDateTime);

    DaySecond daySecondOfColonTime(const std::string & colonTime);

    long intOf(const std::string & s);

    std::string stringOf(long i);

    std::string stringOf(const char * s);

    std::string cymdOfEpochday(const EpochDay epochDay);

    std::string bumpCymd(const std::string & cymd, const int days);

    Hms HmsOfDaySecond(DaySecond daySecond);

    Hms HmsOfTime(const std::string & daySecond);

    DaySecond DaySecondOfHms(const Hms & hms);

    std::string hourOfHms(const Hms & hms);

    std::string opeDateTimeOfCymdHms(const CymdUtc & cymd, const HmsUtc & hms);

    void normaliseCymdDaySec(Cymd & cymd, DaySec & daySec);

    // utc of lt
    DaySecUtc UtcDaySecOfLt(const CymdLt & cymdLt, const DaySecLt daySecLt);

    HmsUtc UtcHmsOfLt(const CymdLt & cymdLt, const HmsLt & hmsLt);

    CymdUtc UtcCymdOfLt(const CymdLt & cymdLt, const DaySecLt daySecLt);

    CymdUtc UtcCymdOfLt(const CymdLt & cymdLt, const HmsLt & hmsLt);

    // lt of utc
        DaySecLt LtDaySecOfUtc(const CymdUtc & cymdUtc, const DaySecUtc daySecUtc);

        HmsLt LtHmsOfUtc(const CymdUtc & cymdUtc, const HmsUtc & hmsUtc);

        CymdLt LtCymdOfUtc(const CymdUtc & cymdUtc, const DaySecUtc daySecUtc);

        CymdLt LtCymdOfUtc(const CymdUtc & cymdUtc, const HmsUtc & hmsUtc);

    std::string digits4(int x);
    
    std::string beautifyXml(const std::string & xml, const std::string & tab = "    ");
    
    bool validateXmlAgainstXsd(const std::string & xml, const std::string & reportFilename = "");

    bool validateXmlAgainstXsd(const std::string & xml, const std::string & xsdFilename, const std::string & reportFilename);

    DaySecond DaySecondOfHms12(const std::string & hms12); // "12.00.00 AM" etc

    DaySecond daySecRound(const DaySecond daySecond, const Seconds roundSecs);

    Hms HmsOfDaySecondRounded(const DaySecStr daySecStr, const Seconds roundSecs);

    Hms HmsOfDaySecondRounded(const DaySec daySec, const Seconds roundSecs);

    std::string cmydDiff( const Cymd cymd1, const Cymd cymd2);
    
    EpochSecond epochSecondsOfCymdDaySec(const Cymd & cymd, const DaySec daySec);

    bool timeIsSummerTime( const EpochSecond utcTime );

    std::string getFormattedDateTime(const EpochSecond utcTime);

    EpochSecond getCurrentTime();

    EpochMilliSecond getEpochMilliSecond();

    EpochMilliSecond getEpochMilliSecondNow();

    std::string getIso8601Time();
}
