//-----------------------------------------------------------------------------

#include <algorithm>
#include <cctype>
#include <cstring>
#include <stdexcept>
#include <fstream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

//-----------------------------------------------------------------------------

#include "HithubCfg.hpp"

#include "logger.hpp"
#include "utils.hpp"

//-----------------------------------------------------------------------------

using namespace std;

using namespace utils;

//-----------------------------------------------------------------------------

void HithubCfg::loadList(const std::string & filename, StringList & stringList)
{
    std::ifstream file(filename);

    if(!file)
    {
        throw runtime_error( std::strerror(errno) );
    }

    std::string line;
    while( std::getline(file, line) )
    {
        stringList.push_back( line );
    }
}

//-----------------------------------------------------------------------------

void HithubCfg::createKeyValues(const StringList & stringList)
{
    keyValues.resize(0);
    
    for (size_t index = 0; index < stringList.size(); index++)
    {
        const std::string & line = stringList[index];

        size_t commentPos = line.find_first_of("#");
        size_t equalPos = line.find_first_of("=");
        
        if ((equalPos == std::string::npos) || (commentPos < equalPos)) {
            //LOG_TRACE << "adding comment: " << line << "\n";
            KeyValue keyValue(line, "");
            keyValues.push_back( keyValue );
            
        } else {
            string key = trimWhitespaceFrom(line.substr(0, equalPos));
            string value = trimWhitespaceFrom(line.substr(equalPos+1));
            //LOG_TRACE << "adding attribute key: " << key << " value: " << value << "\n";
            int index = getKeyIndex(key);
            bool found = index >= 0;
            if (found) {
                std::string exceptionString = "Config data key " + key + " already exists";
                throw runtime_error(exceptionString);
            }
            KeyValue keyValue(key, value);
            keyValues.push_back( keyValue );
        }
    }
}

//-----------------------------------------------------------------------------

int HithubCfg::getKeyIndex(const std::string & key) const
{
    const std::string upperKey = upperCaseOf(key);
    
    unsigned int size = keyValues.size();

    for( unsigned int i=0; i<size; i++ )
    {
        const KeyValue & keyValue = keyValues.at(i);

        if( upperCaseOf(keyValue.key) == upperKey )
        {
            //LOG_TRACE << "ConfigFile::getKeyIndex:" << key << " at index:" << i << "\n";

            return i;
        }
    }

    LOG_TRACE << "ConfigFile::getKeyIndex:" << key << ",not found\n";

    return -1;
}

//-----------------------------------------------------------------------------

std::string & HithubCfg::ref( const std::string & key )
{
    int index = getKeyIndex(key);

    bool found = index >= 0;

    if (! found) {
        std::string exceptionString = "Cannot find key " + key;
        throw runtime_error(exceptionString);
    }
    
    KeyValue & keyValue = keyValues.at(index);

    return keyValue.value;
}

//-----------------------------------------------------------------------------

void HithubCfg::add( const std::string & key, const std::string & value )
{
    int index = getKeyIndex(key);

    bool found = index >= 0;
    
    if (found) {
        std::string exceptionString = "Item already exists for key " + key;
        throw runtime_error(exceptionString);
    }

    KeyValue keyValue(key, value);
    keyValues.push_back( keyValue );
}

//-----------------------------------------------------------------------------

void HithubCfg::remove( const std::string & key )
{
    int index = getKeyIndex(key);

    bool found = index >= 0;
    
    if (! found) {
        std::string exceptionString = "Cannot find key " + key;
        throw runtime_error(exceptionString);
    }

    keyValues.erase(keyValues.begin() + index);
}

//-----------------------------------------------------------------------------

string HithubCfg::toString(){
    string result;
    
    for (size_t index = 0; index < keyValues.size(); index++) {
        const KeyValue & keyValue = keyValues.at(index);
        if (keyValue.value == "") {
            result += keyValue.key + "\n";
        } else {
            result += keyValue.key + " = " + keyValue.value + "\n";
        }
    }
    
    return result;
}

//-----------------------------------------------------------------------------
