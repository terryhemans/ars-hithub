#pragma once

#include <imqi.hpp>
#include <string>

#include "LinxQueues.hpp"
#include "LinxMQ.hpp"
#include "DualRedundantInterface.hpp"

using namespace std;

enum QueueEnum {
    
    qeS002,
    qeS009,
    qeS013,
    qeS015,
    
    qeCount
};

class Linx : public DualRedundantInterface
{

protected:

    ImqQueueManager* mgr;
    ImqChannel* channel;

    LinxMQ * queues[ qeCount ];

    ///
    /// registers a linx into queues
    ///
    void Register(QueueEnum qe, LinxMQ & linx)
    {
        queues[ qe ] = &linx;
    }

public:
    
    LinxMQ & S002_Queue;
    LinxMQ & S009_Queue;
    LinxMQ & S013_Queue;
    LinxMQ & S015_Queue;
    int _unused;

    string ipAddress;

    ///
    /// Constructor.  Pass the connection parameters and a list of all queue names you are interested in.
    /// Throws an exception if it cannot connect to any of the queues
    ///
    Linx( string ipAddress, string channelName, string managerName, string username, string password,
          LinxQueues* queueNames, ProxyController & proxy );


    ///
    /// return the size of all queues
    ///
    int sizeAllQueues(string * report_p = 0);

    ///
    /// return whether all queues are empty
    ///
    bool allQueuesAreEmpty(string * report_p = 0);

    ///
    /// Clears all queues
    ///
    void clearAllQueues();
    
    LinxMQ & queueRef(QueueEnum qe) {
        return *queues[qe];
    }
};
