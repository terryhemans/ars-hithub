#pragma once

#include "LinxQueues.hpp"
#include "HubController.hpp"
#include "Linx.hpp"
#include "HithubCfg.hpp"

#include <string>

using namespace std;

struct TestEnvironment
{

    // control
    string configFilename;
    string configDefaultHithubConfigFilename;
    string configHithub;
    string configLinx;
    string configQueue;
    string configHost;

    // Hub
    HubController* hub;
    ProxyController* proxyControl;
    Linx* linx;
    string hubHost;

    // Hub Config
    string hithubConfigFilename;
    HithubCfg* hithubCfg;

    // Proxy
    string proxyHost;
    int proxyBasePort;  // Five ports will be used starting with this one and
                        // ending with that of the Proxy Control Server

    // LINX
    string linxConnection;
    string linxUser;
    string linxPwd;
    string linxMgr;
    string linxChannel;
    LinxQueues linxQueues;

    // Test Control
    bool excludePassingTests;
    bool excludeSomePassingTests;
    bool excludeFailingTests;
    bool excludeTodoTests;
    bool excludeManualTests;
    bool stopOnFailure;
    
    // Filesystem
    static string projectDir;

    string toString();

    void useProxyServer( bool useIt );
	
	void commandToProxy( string command );

};

