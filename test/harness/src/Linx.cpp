#include "Linx.hpp"
#include <stdexcept>

Linx::Linx( string ipAddress, string channelName, string managerName, string username, string password,
            LinxQueues* queueNames, ProxyController & proxy )
    : 
        DualRedundantInterface(proxy, "L1", "L2"),
        mgr(new ImqQueueManager()),
        
        S002_Queue(* new LinxMQ( mgr, queueNames->S002 )),
        S009_Queue(* new LinxMQ( mgr, queueNames->S009 )),
        S013_Queue(* new LinxMQ( mgr, queueNames->S013 )),
        S015_Queue(* new LinxMQ( mgr, queueNames->S015 )),
        _unused(0),
        ipAddress(ipAddress)
{
    const int HeartBeatInterval = 1;
    const int TransportType = MQXPT_TCP;
    channel = new ImqChannel();
    channel->setHeartBeatInterval(HeartBeatInterval);
    channel->setTransportType(TransportType);
    channel->setChannelName(channelName.c_str());
    channel->setConnectionName(ipAddress.c_str());

    mgr->setName(managerName.c_str());
    mgr->setChannelReference(channel);
    mgr->setAuthenticationType(MQCSP_AUTH_USER_ID_AND_PWD);
    mgr->setUserId(username.c_str());
    mgr->setPassword(password.c_str());

    ImqBoolean result = mgr->connect();

    if (!result){
        int reasonCode = mgr->reasonCode();
        throw runtime_error( "Could not connect to LINX: reason code " + to_string((long long int)reasonCode) );
    }
    
    Register(qeS002, S002_Queue);
    Register(qeS009, S009_Queue);
    Register(qeS013, S013_Queue);
    Register(qeS015, S015_Queue);

    for (uint u = 0; u < qeCount; u++) {
        queues[u]->initialise();
    }
}


int Linx::sizeAllQueues(string * report_p)
{
    int result = 0;
    
    for (uint u = 0; u < qeCount; u++) {
        int size = queues[u]->size();
        if (size > 0) {
            result += size;
            if (report_p != 0) {
                *report_p += "queue(" + queues[u]->getName() + ") size(" + to_string(static_cast<long long>(size)) + ");";
            }
        }
    }
    
    return result;
}

bool Linx::allQueuesAreEmpty(string * report_p)
{
    bool result = true;
    
    for (uint u = 0; u < qeCount; u++) {
        bool isEmpty = queues[u]->isEmpty();
        
        if (! isEmpty) {
            result = false;
            if (report_p != 0) {
                *report_p += "queue(" + queues[u]->getName() + ") not empty;";
            }
        }
    }
    
    return result;
}

void Linx::clearAllQueues()
{
    for (uint u = 0; u < qeCount; u++) {
        queues[u]->clear();
    }
}
