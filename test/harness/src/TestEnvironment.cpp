#include "TestEnvironment.hpp"
#include "assertions.hpp"

string getenvString( const char* var ){
    char* value = getenv(var);
    if( value == NULL ){
        return "";
    }
    else{
        string strValue(value);
        return strValue;
    }
}


string TestEnvironment::projectDir = getenvString("PROJECT_DIR");


string bool2String( bool b ){
    if(b) return "true";
    else return "false";
}

string TestEnvironment::toString()
{
    const string INDENT = "   ";
    const string endl = "\n";
    //string indent = "";
    string output = "";

    // Command Line Options

    output += "Command Line Options" + endl;
    if (configFilename != "") output += INDENT + "config filename: " + configFilename + endl;
    if (configHithub != "") output += INDENT + "HitHub DB UN/PW: " + configHithub + endl;
    if (configLinx != "") output += INDENT + "linx UN/PW: " + configLinx + endl;
    if (configQueue != "") output += INDENT + "linx queue name: " + configQueue + endl;

    output += endl + "Hub" + endl;
    output += INDENT + "host: " + hubHost + endl;

    output += endl + "Proxy" + endl;
    output += INDENT + "host: " + proxyHost + endl;
    output += INDENT + "base port: " + to_string((long long)proxyBasePort) + endl;

    output += endl + "LINX" + endl;
    output += INDENT + "linxConnection: " + linxConnection + endl;
    output += INDENT + "linxUser: " + linxUser + endl;
    output += INDENT + "linxPwd: " + linxPwd + endl;
    output += INDENT + "linxMgr: " + linxMgr + endl;
    output += INDENT + "linxChannel: " + linxChannel + endl;
    output += INDENT + "Queues: " + endl;
    output += INDENT + INDENT + "S002: " + linxQueues.S002 + endl;
    output += INDENT + INDENT + "S009: " + linxQueues.S009 + endl;
    output += INDENT + INDENT + "S013: " + linxQueues.S013 + endl;
    output += INDENT + INDENT + "S015: " + linxQueues.S015 + endl;
    
    if (stopOnFailure) output += INDENT + "stop on Failure: " + bool2String(stopOnFailure) + endl;

    // Test Exclusions
    if (excludePassingTests || excludeFailingTests || excludeTodoTests || excludeManualTests) {
        output += endl + "EXCLUSIONS" + endl;
        if (excludePassingTests) output += INDENT + "exclude passing tests: " + bool2String(excludePassingTests) + endl;
        if (excludeSomePassingTests) output += INDENT + "exclude some passing tests: " + bool2String(excludeSomePassingTests) + endl;
        if (excludeFailingTests) output += INDENT + "exclude failing tests: " + bool2String(excludeFailingTests) + endl;
        if (excludeTodoTests) output += INDENT + "exclude todo tests: " + bool2String(excludeTodoTests) + endl;
        if (excludeManualTests) output += INDENT + "exclude manual tests: " + bool2String(excludeManualTests) + endl;
    }

    output += endl + "projectDir: " + projectDir + endl;

    return output;
}


void TestEnvironment::useProxyServer( bool useIt ){
    if( useIt ){
        proxyControl->start();
        assertMsg("Failed to start proxy controller server", proxyControl->isRunning() );
		proxyBasePort = proxyControl->getBasePort();
        string controllerResponse = proxyControl->issueCommand("L1_ON");
        assertEqualMsg("Unexpected response when switching on Linx Primary", controllerResponse, string("True"));
        controllerResponse = proxyControl->issueCommand("L2_ON");
        assertEqualMsg("Unexpected response when switching on Linx Secondary", controllerResponse, string("True"));
        hithubCfg->set("linx.connection", proxyHost + "(" + to_string((long long)proxyBasePort) + ")");
    	hithubCfg->set("linx.secondary.connection", proxyHost + "(" + to_string((long long)(proxyBasePort+1)) + ")");
		
		//*
		controllerResponse = proxyControl->issueCommand("T1_ON");
        assertEqualMsg("Unexpected response when switching on Tranista Primary", controllerResponse, string("True"));
        controllerResponse = proxyControl->issueCommand("T2_ON");
        assertEqualMsg("Unexpected response when switching on Tranista Secondary", controllerResponse, string("True"));
        hithubCfg->set("tranista.connection", proxyHost + ":" + to_string((long long)(proxyBasePort + 2)));
    	hithubCfg->set("tranista.secondary.connection", proxyHost + ":" + to_string((long long)(proxyBasePort + 3)));
		//*/
        hub->installConfigFile( *hithubCfg );
    }
    else{
        //proxyControl->stop();
        //assertMsg("Failed to stop proxy controller server", !proxyControl->isRunning() );
    	
		string controllerResponse = proxyControl->issueCommand("L1_OFF");
        assertEqualMsg("Unexpected response when switching on Linx Primary", controllerResponse, string("False"));
        controllerResponse = proxyControl->issueCommand("L2_OFF");
        assertEqualMsg("Unexpected response when switching on Linx Secondary", controllerResponse, string("False"));
		
		controllerResponse = proxyControl->issueCommand("T1_OFF");
        assertEqualMsg("Unexpected response when switching on Tranista Primary", controllerResponse, string("False"));
        controllerResponse = proxyControl->issueCommand("T2_OFF");
        assertEqualMsg("Unexpected response when switching on Tranista Secondary", controllerResponse, string("False"));
		
		hithubCfg->set("linx.connection", linx->ipAddress);
    	hithubCfg->set("linx.secondary.connection", "linx.secondary.url");
		hithubCfg->set("tranista.secondary.connection", "tranista.secondary.url");
        hub->installConfigFile( *hithubCfg );
    }
}

void TestEnvironment::commandToProxy(string command)
{
	if(proxyControl->isRunning())
	{
		string ControllerResponse = proxyControl->issueCommand(command);
		if (ControllerResponse == "Unrecognised Command to Control Server")
		{
			assertMsg("Error: Unrecognised command sent to Proxy Control Server - " + command, false);		
		}
	}
	else
	{
		assertMsg("Error: Proxy Control Server Not Running", false);
	}
}


