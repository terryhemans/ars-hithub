#pragma once

#include "HubController.hpp"
#include "logger.hpp"
#include <stdexcept>
#include <iostream>

using namespace std;


///
/// HubControl
///
class LocalHubController : public HubController
{

public:


    LocalHubController() : HubController() {}
    ~LocalHubController() { stop(); }

    ///
    /// starts the hub in a background process.  Takes an optional parameter line.
    /// Hub is left running until it ends or stop() is called.
    ///
    void start(const string &params = "" ,
               const string &alertlog = "",
               const string &eventlog = "",
               const string &name = "")
    {
        startProcess( "./hithub", params, "bin", alertlog, eventlog, name );
    }


    ///
    /// returns whether the Hub executable is on the local host or a remote host (e.g. VM)
    ///
    string getHost() { return "localhost"; }


    ///
    /// copies the given source file to the destination ready for testing
    ///
    void installConfigFile( string sourceFile, string destinationFile = "" )
    {
        if( destinationFile == "" ) destinationFile = "bin";
        LOG_DEBUG << "Installing config file: " << sourceFile << endl;
        string command = "cp --remove-destination " + sourceFile + " " + destinationFile;
        LOG_TRACE << command << endl;
        int exitCode = system(command.c_str());
        if( exitCode != 0 ) throw runtime_error("failed to install config file: '" + command + "' failed with exit code " + to_string((long long)exitCode) );
    }


};


