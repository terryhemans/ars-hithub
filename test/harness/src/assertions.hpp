#pragma once

#include <cppunit/TestAssert.h>
#include <string>

#include "Matchable.hpp"
#include "utils.hpp"

#define assertEqual CPPUNIT_ASSERT_EQUAL
#define assertEqualMsg CPPUNIT_ASSERT_EQUAL_MESSAGE
#define assert CPPUNIT_ASSERT
#define assertMsg CPPUNIT_ASSERT_MESSAGE

template <class T>
void assertNotEqual( const T& expected, const T& actual ){
    assert( expected != actual );
}


void assertMatchesMsg( string message, const Matchable& lhs, const Matchable& rhs, const Mask* mask );

void assertMatches( const Matchable& lhs, const Matchable& rhs, const Mask* mask );

void assertNotMatchesMsg( string message, const Matchable& lhs, const Matchable& rhs, const Mask* mask );

void assertNotMatches( const Matchable& lhs, const Matchable& rhs, const Mask* mask );

