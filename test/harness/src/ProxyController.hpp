#pragma once

#include <string.h>    //strlen
#include <string>  //string
#include <sys/socket.h>    //socket
#include <arpa/inet.h> //inet_addr
#include <netdb.h> //hostent
#include <netinet/in.h>
#include <unistd.h>
#include <thread>

using namespace std;


static const int controlPortOffset = 4;

class ProxyController
{

public:

    ProxyController( string ipAddress, int basePort );
    ~ProxyController();

    // start the proxy server
	void start();

    // stop the proxy server
    string stop();

    string issueCommand(string theCommand);

    bool isRunning();

    string getIpAddress();
    int getBasePort();

private:

    bool isActive;
	
	string ipAddress;
    int basePort;

    int sock;
    string address;
    int port;
    struct sockaddr_in server;
    bool socketConnected;

	void startProxyServer();
    bool conn(string address, int port);	
	void portCheck();
	bool checkPort(int port);
	
	std::thread ProxyControllerThread;

};

