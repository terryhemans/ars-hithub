//-----------------------------------------------------------------------------
//- RESERVATION OF RIGHTS:
//- Copyright (c) 2017 Hitachi Limited. All rights reserved.
//-
//- $Id: $
//-
//- $URL: $
//-
//- DESCRIPTION: Declares the Hitachi Hub Tranista types.
//-
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#ifndef HITHUB_TYPES_HPP
#define HITHUB_TYPES_HPP

//-----------------------------------------------------------------------------
//- INCLUDES
//-----------------------------------------------------------------------------

#include <string>
#include <vector>

//-----------------------------------------------------------------------------
//- MACRO DEFINITIONS
//-----------------------------------------------------------------------------

#define COUT(X) cout << endl << "[[ " << X << " ]] "

#define NOUT(X) // nothing

#if 1
    #define OUT(X) NOUT(X)
#else
    #define OUT(X) COUT(X)
#endif

#define BIT_MASK(n) (1 << (n))

//-----------------------------------------------------------------------------
//- CONSTANT DEFINITIONS
//-----------------------------------------------------------------------------

const long secondsPerDay = 24 * 60 * 60;

//-----------------------------------------------------------------------------
//- TYPEDEFS
//-----------------------------------------------------------------------------

// general

typedef std::vector<std::string> StringList;

typedef std::vector<StringList> StringLists;

// times

// the Epoch started at 00:00 in 1970/01/01

typedef long DaySecond; // the second in a day: 0 .. (24 * 60 * 60) - 1
typedef DaySecond DaySec;

typedef std::string DaySecondStr;
typedef DaySecondStr DaySecStr;

typedef long EpochDay; // the day in the Epoch
typedef long long EpochSecond; // the second in the Epoch
typedef long long EpochMilliSecond; // the millisecond in the Epoch

// durations

typedef long Days; // a number of days
typedef long Seconds; // a number of seconds
typedef long long MilliSeconds; // a number of milliseconds

//-----------------------------------------------------------------------------

// time and date strings

// unknown time zone
typedef std::string Cymd; // "20191231"
typedef std::string CymdHms; // "20191231 23:59:59"
typedef std::string HourStr; // "00" - "23"
typedef std::string Hms; // "23:59:59"
typedef std::string MmSs; // "2359"

// LocalTime (Lt)
typedef Cymd CymdLt;
typedef CymdHms CymdHmsLt;
typedef HourStr HourStrLt;
typedef Hms HmsLt;
typedef MmSs MmSsLt;
typedef DaySecStr DaySecStrLt;
typedef DaySec DaySecLt;

// UTC (Utc)
typedef Cymd CymdUtc;
typedef CymdHms CymdHmsUtc;
typedef HourStr HourStrUtc;
typedef Hms HmsUtc;
typedef MmSs MmSsUtc;
typedef DaySecStr DaySecStrUtc;
typedef DaySec DaySecUtc;

//-----------------------------------------------------------------------------

// database

typedef std::vector<std::string> Record;

typedef std::vector<Record> Table;

//-----------------------------------------------------------------------------
//- DATA DECLARATIONS
//-----------------------------------------------------------------------------

// Constants

// Enumerations

// Structures

// Variables

// Templates

// Classes

//-----------------------------------------------------------------------------
//- FUNCTION DECLARATIONS
//-----------------------------------------------------------------------------

#ifdef __GNUG__
    // not provided in GCC 6.4.0, but useful to enable compilation
    extern char *strptime(const char *, const char *, struct tm *);
#endif

//-----------------------------------------------------------------------------

#endif  // HITHUB_TYPES_HPP
