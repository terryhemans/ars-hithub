//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>
#include <vector>

//-----------------------------------------------------------------------------

#include "ConfigFile.hpp"
#include "HithubConfigFile.hpp"

//-----------------------------------------------------------------------------

using namespace std;

typedef vector<string> StringList;

//-----------------------------------------------------------------------------

class HithubCfg : public HithubConfigFile
{
protected:
    
    int getKeyIndex(const std::string & key) const;

    KeyValues keyValues;
    
public:

    HithubCfg() : HithubConfigFile("hithub.cfg") {
    }
    
    void loadList(const std::string & filename, StringList & stringList);
    
    void createKeyValues(const StringList & stringList);

    bool exists( const std::string & key ) { return getKeyIndex(key) >= 0; }

    void add( const std::string & key, const std::string & value );
    
    void addOrSet( const std::string & key, const std::string & value )
        { if (exists(key)) set(key, value); else add(key, value); }
    
    const std::string & get( const std::string & key ) { return ref(key); }

    std::string & ref( const std::string & key );

    void remove( const std::string & key );
    
    void set( const std::string & key, const std::string & value ) { ref(key) = value; }
    
    string toString();

};
