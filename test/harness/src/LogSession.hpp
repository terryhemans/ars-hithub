#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;


class Log;           // defined below
class LogIterator;   // defined below


class LogSession{

    private:

        string operationalLogName;
        string alertLogName;
        bool loaded;

        vector<Log> logs;

        struct Counters{
            uint numLogs;
            uint numInfos;
            uint numDebugs;
            uint numTraces;
            uint numAlerts;
            uint numErrors;
            uint numWarnings;
        };
        Counters counters;

        void update();

    public:

        LogSession(){ init("",""); }

        void init( const string & operationalLogName, const string & alertLogName );
        
        uint numInfos();
        uint numDebugs();
        uint numTraces();
        uint numLogs();
        uint numAlerts();
        uint numErrors();
        uint numWarnings();

        bool hasAlerts();
        bool hasErrors();
        bool hasWarnings();
        bool hasInfos();
        bool hasDebugs();
        bool hasTraces();
        bool hasErrorsOrWarnings();
        bool hasNoAlerts();
        bool hasNoErrors();
        bool hasNoWarnings();
        bool hasNoInfos();
        bool hasNoDebugs();
        bool hasNoTraces();
        bool hasNoErrorsOrWarnings();

        LogIterator & getLogs();
        LogIterator & getInfos(const string & filter = "");
        LogIterator & getDebugs();
        LogIterator & getTraces();
        LogIterator & getAlerts();
        LogIterator & getErrors();
        LogIterator & getWarnings();
        LogIterator & getErrorsAndWarnings();

        Log* push( const Log & log );

        // Returns a new LogSession with only logs that contain all the given tags
    
        LogSession & filter( const string tags[], int numberOfTags );

        string toString();
};


//-----------------------------------------------------------------------------
// Log class
//
// Holds a single log entry split into its fields
//-----------------------------------------------------------------------------
class Log{

    public:

        enum LogLevel{ trace, debug, info, warning, error, alert, fatal };

        LogLevel level;
        string raw, time, product, type, status, tags, message;

        Log() {}
        
        Log(const string & line, bool isAlert = false);
        
        string toString();
};


//-----------------------------------------------------------------------------
// LogIterator class
//
// Virtual class.  Allows iteration over a set of Log objects.
//-----------------------------------------------------------------------------
class LogIterator{

    protected:

        vector<Log>* allLogs;
        uint ptr;
        uint step;
        uint count;

    public:

        LogIterator( int begin = 0 ){ ptr = begin; step = begin; count = 0; }
        bool hasNext(){ return step < count; }
    
        virtual const Log & next() = 0;
        uint size(){ return count; }
        
        void rewind() { ptr = 0; step = 0; }

        string toString(){
            string s = "";
            for( uint i=0; i< allLogs->size(); i++ ){
                s += allLogs->at(i).toString() + "\n";
            }
            return s;
        }

};
