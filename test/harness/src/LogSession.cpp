#include "LogSession.hpp"
#include "logger.hpp"
#include "utils.hpp"
#include <stdexcept>

using namespace std;
using namespace utils;


//-----------------------------------------------------------------------------
// LogIterator class functions
//-----------------------------------------------------------------------------

class AllLogIterator : public LogIterator{

    public:

        AllLogIterator( vector<Log>* logs ){
            allLogs = logs;
            count = logs->size();
        }

    
        
        const Log & next(){ 
            if( hasNext() ){
                step++;
                return allLogs->at(ptr++);
            }
            else throw runtime_error("TEST FAILURE: AllLogIterator.next() called with nothing left");
        }
};


//-----------------------------------------------------------------------------

class ErrorLogIterator : public LogIterator{

    public:

        ErrorLogIterator( vector<Log>* logs ){
            allLogs = logs;
            for( size_t i=0; i<logs->size(); i++ ){
                const Log & log = logs->at(i);
                if( log.level == Log::error || log.level == Log::fatal ) count++;
                if( log.level == Log::fatal ) std::cout << "fatal it";
            }
        }

    
        
        const Log & next(){
            if( hasNext() ){
                step++;
                for( size_t i=ptr; i<allLogs->size(); i++ ){
                    const Log & log = allLogs->at(ptr++);
                    if( log.level == Log::error || log.level == Log::fatal ) return log;
                }
            }
            throw runtime_error("TEST FAILURE: ErrorLogIterator.next() called with nothing left");
        }
};


//-----------------------------------------------------------------------------

class SingleLevelLogIterator : public LogIterator{

    private:

        Log::LogLevel level;
        string filter;

    public:

        SingleLevelLogIterator( vector<Log>* logs, Log::LogLevel level, const string & filter = "" ){
            this->level = level;
            this->filter = filter;
            allLogs = logs;
            for( size_t i=0; i<logs->size(); i++ ){
                const Log & log = logs->at(i);
                if( log.level == level ) {
                    if ((filter == "") || (log.message.find(filter) != string::npos)) {
                        count++;
                    }
                }
            }
        }

    
        
        const Log & next(){ 
            if( hasNext() ){
                step++;
                for( size_t i=ptr; i<allLogs->size(); i++ ){
                    const Log & log = allLogs->at(ptr++);
                    if( log.level == level ) {
                        if ((filter == "") || (log.message.find(filter) != string::npos)) {
                            return log;
                        }
                    }
                }
            }
            throw runtime_error("TEST FAILURE: LogIterator.next() called with nothing left");
        }
};


//-----------------------------------------------------------------------------

class WarningAndErrorLogIterator : public LogIterator{

    public:

        WarningAndErrorLogIterator( vector<Log>* logs ){
            allLogs = logs;
            for( size_t i=0; i<logs->size(); i++ ){
                const Log & log = logs->at(i);
                if( log.level == Log::warning || log.level == Log::error || log.level == Log::fatal ) count++;
            }
        }

    
        
        const Log & next(){ 
            if( hasNext() ){
                step++;
                for( size_t i=ptr; i<allLogs->size(); i++ ){
                    const Log & log = allLogs->at(ptr++);
                    if( log.level == Log::warning || log.level == Log::error || log.level == Log::fatal ) return log;
                }
            }
            throw runtime_error("TEST FAILURE: WarningAndErrorLogIterator.next() called with nothing left");
        }
};


//-----------------------------------------------------------------------------
// LogSession class functions
//-----------------------------------------------------------------------------

void LogSession::init( const string & operationalLogName, const string & alertLogName )
{
    this->operationalLogName = operationalLogName;
    this->alertLogName = alertLogName;
    loaded = false;

    logs.resize(0);
    
    counters.numLogs = 0;
    counters.numInfos = 0;
    counters.numDebugs = 0;
    counters.numTraces = 0;
    counters.numAlerts = 0;
    counters.numErrors = 0;
    counters.numWarnings = 0;
};


 
uint LogSession::numLogs(){ update(); return counters.numLogs; }
uint LogSession::numInfos(){ update(); return counters.numInfos; }
uint LogSession::numDebugs(){ update(); return counters.numDebugs; }
uint LogSession::numTraces(){ update(); return counters.numTraces; }
uint LogSession::numAlerts(){ update(); return counters.numAlerts; }
uint LogSession::numErrors(){ update(); return counters.numErrors; }
uint LogSession::numWarnings(){ update(); return counters.numWarnings; }

//-----------------------------------------------------------------------------

bool LogSession::hasAlerts(){ update(); return counters.numAlerts > 0; }
bool LogSession::hasErrors(){ update(); return counters.numErrors > 0; }
bool LogSession::hasWarnings(){ update(); return counters.numWarnings > 0; }
bool LogSession::hasInfos(){ update(); return counters.numInfos > 0; }
bool LogSession::hasDebugs(){ update(); return counters.numDebugs > 0; }
bool LogSession::hasTraces(){ update(); return counters.numTraces > 0; }
bool LogSession::hasErrorsOrWarnings(){ update(); return counters.numErrors + counters.numWarnings > 0; }
bool LogSession::hasNoAlerts(){ update(); return !hasAlerts(); }
bool LogSession::hasNoErrors(){ update(); return !hasErrors(); }
bool LogSession::hasNoWarnings(){ update(); return !hasWarnings(); }
bool LogSession::hasNoInfos(){ update(); return !hasInfos(); }
bool LogSession::hasNoDebugs(){ update(); return !hasDebugs(); }
bool LogSession::hasNoTraces(){ update(); return !hasTraces(); }
bool LogSession::hasNoErrorsOrWarnings(){ update(); return !hasErrorsOrWarnings(); }


//-----------------------------------------------------------------------------

LogIterator & LogSession::getAlerts(){ update(); return * new SingleLevelLogIterator(&logs, Log::LogLevel::alert); }
LogIterator & LogSession::getLogs(){ update(); return * new AllLogIterator(&logs); }
LogIterator & LogSession::getErrors(){ update(); return * new ErrorLogIterator(&logs); }
LogIterator & LogSession::getWarnings(){ update(); return * new SingleLevelLogIterator(&logs, Log::LogLevel::warning); }
LogIterator & LogSession::getInfos(const string & filter){ update(); return * new SingleLevelLogIterator(&logs, Log::LogLevel::info, filter); }
LogIterator & LogSession::getDebugs(){ update(); return * new SingleLevelLogIterator(&logs, Log::LogLevel::debug); }
LogIterator & LogSession::getTraces(){ update(); return * new SingleLevelLogIterator(&logs, Log::LogLevel::trace); }
LogIterator & LogSession::getErrorsAndWarnings(){ update(); return * new WarningAndErrorLogIterator(&logs); }


//-----------------------------------------------------------------------------

Log* LogSession::push( const Log & log ){
    counters.numLogs++;
    switch(log.level)
    {
        case Log::trace:
            counters.numTraces++;
            break;
        case Log::debug:
            counters.numDebugs++;
            break;
        case Log::info:
            counters.numInfos++;
            break;
        case Log::warning:
            counters.numWarnings++;
            break;
        case Log::alert:
            counters.numAlerts++;
            break;
        case Log::error:
        case Log::fatal:
            counters.numErrors++;
            break;
        default:
            throw runtime_error("LogSession::push:unknown log.level: " + to_string((long long int)(log.level)) );
    }
    logs.push_back(log);
    return &logs.back();

};


//-----------------------------------------------------------------------------

void LogSession::update(){

    if( ! loaded ){

        string line;

        if( operationalLogName != "" ){
            LOG_TRACE << "LogSession: reading " << operationalLogName << endl;
            ifstream operationalLogStream;
            operationalLogStream.open(operationalLogName);
            if ( ! operationalLogStream.is_open() ) {
                string errorStr(strerror(errno));
                throw runtime_error("LogSession failed to open the operational log file log file: " + errorStr );
            }
            while( getline( operationalLogStream, line ) ){
                try{
                    if( line.length() == 0 ){
                        LOG_TRACE << "ignoring blank line" << endl;
                    }
                    else if( line.length() >= 6 && line.substr(0,6) == "hithub" ){
                        LOG_TRACE << "ignoring hithub comment line: " << line << endl;
                    }
                    else{
                        Log* log = new Log(line);
                        push( *log );
                    }
                }
                catch( const exception& e ){
                    LOG_WARNING << "ignored line in log file with unexpected format: \"" << line << "\"" << endl;
                }
            }
            operationalLogStream.close();
        }

        if( alertLogName != "" ){
            LOG_TRACE << "LogSession: reading " << alertLogName << endl;
            ifstream alertLogStream;
            alertLogStream.open(alertLogName);
            if ( ! alertLogStream.is_open() ) {
                string errorStr(strerror(errno));
                throw runtime_error("LogSession failed to open the alert log file: " + errorStr );
            }
            while( getline( alertLogStream, line ) ){
                try{
                    Log* log = new Log(line, true);
                    push( *log );
                }
                catch( const exception& e ){
                    LOG_DEBUG << "ignored line in log file with unexpected format: \"" << line << "\"" << endl;
                }
            }
            alertLogStream.close();
        }

        loaded = true;
    }
}


//-----------------------------------------------------------------------------

LogSession::LogSession & LogSession::filter( const string tags[], int numberOfTags ){
    LogSession* filteredSession = new LogSession();
    LogIterator & it = getLogs();
    while( it.hasNext() ){
        const Log & log = it.next();
        bool allTagsFound = true;
        for( int i=0; i<numberOfTags; i++ ){
            if( log.tags.find(tags[i]) == string::npos ) allTagsFound = false;
        }
        if( allTagsFound ) filteredSession->push(log);
    }
    return *filteredSession;
}


//-----------------------------------------------------------------------------

string LogSession::toString(){

    string str =
        "operationalLogName: " + operationalLogName + "\n" +
        "alertLogName: " + alertLogName + "\n" +
        "loaded: " + to_string(loaded) + "\n" +
        "numLogs: " + to_string((long long)counters.numLogs) + "\n" +
        "numInfos: " + to_string((long long)counters.numInfos) + "\n" +
        "numDebugs: " + to_string((long long)counters.numDebugs) + "\n" +
        "numTraces: " + to_string((long long)counters.numTraces) + "\n" +
        "numAlerts: " + to_string((long long)counters.numAlerts) + "\n" +
        "numErrors: " + to_string((long long)counters.numErrors) + "\n" +
        "numWarnings: " + to_string((long long)counters.numWarnings) + "\n" +
        "Logs: \n";
        
    for( size_t i=0; i<logs.size(); i++ ){
        str += to_string((long long)i) + ": " + logs.at(i).toString() + "\n";
    }
    return str;
}


//-----------------------------------------------------------------------------
// Log class functions
//-----------------------------------------------------------------------------

Log::LogLevel stringToLevel( string type )
{
    if( type == "[trace]" ) return Log::trace;
    else if( type == "[debug]" ) return Log::debug;
    else if( type == "[info]" ) return Log::info;
    else if( type == "[warning]" ) return Log::warning;
    else if( type == "[error]" ) return Log::error;
    else if( type == "[alert]" ) return Log::alert;
    else if( type == "[fatal]" ) return Log::fatal;
    else throw runtime_error("Invalid type logged to the operational or alert log: " + type);
}



Log::Log(const string & line, bool isAlert){
    // all attributes must be initialised
    raw = line;
    vector<string> fields = strictSplit(line, "\t");
    int size = fields.size();
    if( isAlert ){
        if(size < 5){
            throw runtime_error("Log::Log not enough Alert fields: "+line);
        }
        time = fields[0];
        product = fields[1];
        type = "[alert]";
        status = fields[2];
        tags = fields[3];
        message = fields[4];
    }else{
        type = "[unknown]";
        if (size > 0) time = fields[0];
        if (size > 1) product = fields[1];
        if (size > 2) type = fields[2];
        if (size > 3) status = fields[3];
        if (size > 4) tags = fields[4];
        if (size > 5) message = fields[5];
        if(size < 6){
            if ((type != "[trace]") && (type != "[debug]")) {
                throw runtime_error("Log::Log not enough Log fields: "+line);
            }
        }
    }
    level = stringToLevel(type);
}

string Log::toString(){ return raw; }


