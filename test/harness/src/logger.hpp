#pragma once

#include <iostream>
#include <string>

//-----------------------------------------------------------------------------

using namespace std;

//-----------------------------------------------------------------------------

#define LOG         log(_normal)
#define LOG_VERBOSE log(_verbose)
#define LOG_DEBUG   log(_debug)
#define LOG_TRACE   log(_trace)
#define LOG_ERROR   log(_error)
#define LOG_WARNING log(_warning)

//-----------------------------------------------------------------------------

#define DIVIDER "--------------------------------------------------------------------------------"
#define NEWLINE LOG << endl;

#define HEADING(text) endl << DIVIDER << endl << text << endl << DIVIDER << endl


//-----------------------------------------------------------------------------

enum LogLevel { _silent = 0, _off, _normal, _verbose, _debug, _trace, _error, _warning };

extern LogLevel logLevel;  // Logs >= to this logLevel will be logged


//-----------------------------------------------------------------------------

// a null outputstream for discarding logs with a severity lower than configured

class NullBuffer : public streambuf
{
public:
    int overflow(int c)
    {
        return c;
    }
};

extern ostream nullStream;


//-----------------------------------------------------------------------------

extern ostream& log(LogLevel level = _normal);


