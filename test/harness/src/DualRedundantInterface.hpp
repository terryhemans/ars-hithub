#pragma once

#include <string>

#include "ProxyController.hpp"

#define LINK_A DualRedundantInterface::Link_A
#define LINK_B DualRedundantInterface::Link_B
#define BOTH_LINKS DualRedundantInterface::BothLinks

using namespace std;

class DualRedundantInterface
{

public:

    enum LinkType { Link_A = 1, Link_B = 2, BothLinks = 3 };


    DualRedundantInterface( ProxyController & proxy, string linkAPrefix, string linkBPrefix );


    ///
    /// Breaks the link between the database and the Hitachi Hub so that the Hub experiences a disconnect
    /// Throws an exception if it cannot disconnect
    ///

    void simulateLinkFailure( LinkType link = BothLinks );


    ///
    /// Recovers the link between the database and the Hitachi Hub, allowing the Hub to connect
    /// Throws an exception if it cannot connect
    ///
    void simulateLinkRecovery( LinkType link = BothLinks );


    ///
    /// Returns true if the test harness is not simulating a link failure
    ///
    bool isLinkAvailable( LinkType link = BothLinks );


protected:

    ProxyController & proxy;
    string linkAPrefix;
    string linkBPrefix;

};
