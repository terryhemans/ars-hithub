#include "LinxMQ.hpp"
#include <stdexcept>
#include <iostream>

LinxMQ::LinxMQ( ImqQueueManager* mgr, string queueName ) :
    name( queueName )
{
    int openOptions = MQOO_OUTPUT + MQOO_INPUT_SHARED + MQOO_INQUIRE; // full access
    queue.setName(queueName.c_str());
    queue.setConnectionReference(mgr);
    queue.setOpenOptions( openOptions );
}

void LinxMQ::initialise()
{
    queue.open();
    if( queue.reasonCode() ){
        throw runtime_error( "Could not open " + name + " queue: reason code " + to_string((long long)queue.reasonCode()) );
    } else if (queue.completionCode() == MQCC_FAILED)
    {
        throw runtime_error( "Could not open " + name + " queue for input" );
    }
}

int LinxMQ::size()
{
    return queue.currentDepth();
}


bool LinxMQ::isEmpty()
{
    return ( size() == 0 );
}


void LinxMQ::clear()
{
    do {
        ImqMessage imqMsg;
        const int maxMessageLength = 100*1000;
        char buffer[maxMessageLength+1];
        imqMsg.useEmptyBuffer(buffer, maxMessageLength);
        queue.get(imqMsg);
    } while ( queue.completionCode() == MQCC_OK);
}


void LinxMQ::push( const string & msg )
{
    ImqMessage imqMessage;
    imqMessage.useFullBuffer( msg.c_str(), msg.length() );
    imqMessage.setFormat( MQFMT_STRING );
    if ( !queue.put( imqMessage ) )
    {
        throw runtime_error("Failed to add message to "+name+" queue "+to_string((long long int)queue.reasonCode()));
    }
}


string LinxMQ::pop()
{
    ImqMessage imqMsg;
    const size_t maxMessageLength = 100*1000;
    char buffer[maxMessageLength+1];
    imqMsg.useEmptyBuffer(buffer, maxMessageLength);
    queue.get(imqMsg);
    if ( queue.completionCode() != MQCC_OK) {
        throw runtime_error("Failed to pop message from "+name+" queue: completion code "+to_string((long long int)queue.completionCode()));
    }
    else{
        const size_t messageLength = imqMsg.dataLength();
        if (messageLength > messageLength) {
            throw runtime_error("LinxMQ::pop from "+name+" queue: message length too long");
        }
        buffer[messageLength] = '\0';  /* add null terminator */
        if (imqMsg.bufferPointer() != buffer) {
            throw runtime_error("LinxMQ::pop from "+name+" invalid buffer");
        }
        string msgStr = imqMsg.bufferPointer();
        return msgStr;
    }
}

/*
XmlMessage* LinxMQ::popXML( XmlMessage* msg )
{
    msg->parse( pop() );
    return msg;
}
*/
