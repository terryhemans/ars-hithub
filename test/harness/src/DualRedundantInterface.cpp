#include "DualRedundantInterface.hpp"
#include "assertions.hpp"

DualRedundantInterface::DualRedundantInterface( ProxyController & proxy, string linkAPrefix, string linkBPrefix ) :
    proxy(proxy), linkAPrefix(linkAPrefix), linkBPrefix(linkBPrefix)
{
};

void DualRedundantInterface::simulateLinkFailure( LinkType link )
{
    if (link & Link_A) {
        LOG << "disconnect a";
        string controllerResponse = proxy.issueCommand(linkAPrefix+"_OFF");
	assertEqualMsg("Proxy Controller failed to disconnect link A", controllerResponse, string("False"));
    }
    if (link & Link_B) {
        LOG << "disconnect b";
        string controllerResponse = proxy.issueCommand(linkBPrefix+"_OFF");
	assertEqualMsg("Proxy Controller failed to disconnect link B", controllerResponse, string("False"));
    }
}


void DualRedundantInterface::simulateLinkRecovery( LinkType link )
{
    if (link & Link_A) {
        string controllerResponse = proxy.issueCommand(linkAPrefix+"_ON");
	assertEqualMsg("Proxy Controller failed to connect link A", controllerResponse, string("True"));
    }
    if (link & Link_B) {
        string controllerResponse = proxy.issueCommand(linkBPrefix+"_ON");
	assertEqualMsg("Proxy Controller failed to connect link B", controllerResponse, string("True"));
    }
}


bool DualRedundantInterface::isLinkAvailable( LinkType link )
{
    bool linkAConnected = false;
    bool linkBConnected = false;
    if (link & Link_A) {
        linkAConnected = ( proxy.issueCommand(linkAPrefix+"_I") == "True" );
    }
    if (link & Link_B) {
        linkBConnected = ( proxy.issueCommand(linkBPrefix+"_I") == "True" );
    }
    return linkAConnected || linkBConnected;
}

