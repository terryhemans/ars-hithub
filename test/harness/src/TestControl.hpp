#pragma once

#include <cppunit/extensions/HelperMacros.h>
#include <unistd.h>


//-----------------------------------------------------------------------------
// Test case type macros to allow test cases to be excluded from a test run

#define PASS !theTestEnvironment.excludePassingTests
#define SOME_PASS !theTestEnvironment.excludeSomePassingTests
#define FAIL !theTestEnvironment.excludeFailingTests
#define TODO !theTestEnvironment.excludeTodoTests
#define MANUAL !theTestEnvironment.excludeManualTests
#define ALL true

#define SWRTS !theTestEnvironment.excludePassingTests && theTestEnvironment.hubHost == "localhost"
#define SATS !theTestEnvironment.excludePassingTests && theTestEnvironment.hubHost != "localhost"


#define TEST1(method) if( !theTestEnvironment.excludePassingTests ){ CPPUNIT_TEST( method ); }
#define TEST2(method, condition) if( condition ){ CPPUNIT_TEST( method ); }
#define TEST3(method, condition1, condition2) if( (condition1) && (condition2) ){ CPPUNIT_TEST( method ); }
#define GET_MACRO(_1,_2,_3,NAME,...) NAME
#define TEST(...) GET_MACRO(__VA_ARGS__, TEST3, TEST2, TEST1)(__VA_ARGS__)

//-----------------------------------------------------------------------------


class TestControl
{

public:

    static void fortywinks(){ usleep(300000); }

    static void usnooze( int microseconds ){ usleep(microseconds); }

    static void snooze( int seconds ){ usleep(seconds*1000000); }

};

