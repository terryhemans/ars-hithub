#include "logger.hpp"

LogLevel logLevel = _normal;


ostream nullStream( new NullBuffer );


ostream& log(LogLevel level)
{
    if( level == _error && logLevel != _silent )
    {
        cerr << "ERROR - ";
        return cerr;
    }
    if( level == _warning && logLevel != _silent )
    {
        cerr << "WARNING - ";
        return cerr;
    }
    else if( level <= logLevel )
    {
        return cout;
    }
    else return nullStream;
}
