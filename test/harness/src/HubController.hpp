// HubController.hpp

#pragma once

#include <string>

#include "LogSession.hpp"
#include "HithubConfigFile.hpp"

using namespace std;

class SingleRun;

///
/// HubController
///
class HubController
{

public:

    LogSession & logSession;


    ///
    /// starts the hub in a background process.  Takes an optional parameter line.
    /// Hub is left running until it ends or stop() is called.
    ///
    virtual void start( const string &params = "" ,
                        const string &alertlog = "",
                        const string &eventlog = "",
                        const string &name = "") = 0;


    ///
    /// stops the hub if running
    ///
    void stop();


    ///
    /// runs the hub in the current process.  Takes an optional parameter line.
    /// Will only return when the Hub exits
    ///
    void run( const string &params = "",
              const string &alertlog = "",
              const string &eventlog = "",
              const string &name = "" );


    ///
    /// returns true if the Hub is running
    ///
    bool isRunning();


    ///
    /// returns whether the Hub executable is on the local host or a remote host (e.g. VM)
    ///
    virtual string getHost() = 0;


    ///
    /// copies the given source file to the destination ready for testing
    ///
    virtual void installConfigFile( string sourceFile, string destinationFile = "") = 0;


    ///
    /// copies the given HithubConfigFile object to the destination ready for testing
    ///
    void installConfigFile( HithubConfigFile & cfg, string destinationFile = "");


    ///
    /// alternative way to start or run the hub by setting parameters explicitly.  See SingleRun.
    ///
    /// examples:
    ///    hubController.go().startTime("1-FEB-2018 02:30:00").runTime(5).run();
    ///    hubController.go().setConfigFile("test_config2.cfg").numiterations(100).timeDelta(3600).start();
    ///

    SingleRun & go();


    string testName;
    string alertLogName;
    string eventLogName;

protected:

    pid_t hithubPID;
    bool running;

    HubController() : 
        logSession( * new LogSession),
        hithubPID(0), 
        running(false) { }

    ///
    /// starts the hub in a background process.  Takes an optional parameter line.
    /// Hub is left running until it ends or stop() is called.
    ///
    void startProcess(const string &command,
                      const string &args = "",
                      const string &runInDir = "",
                      const string &alertlog = "",
                      const string &eventlog = "",
                      const string &name = "" );


};


///
/// Provides a way to set parameters then run the Hub
///
/// examples:
///    HubControl::go().startTime("1-FEB-2018 02:30:00").runTime(5).run();
///    HubControl::go().setConfigFile("test_config2.cfg").numiterations(100).timeDelta(3600).start();
///
class SingleRun
{

private:

    static SingleRun & instance;

    HubController* hub;
    string m_alertLog;
    string m_eventLog;
    string m_name;
    string params;

    SingleRun() : hub(0) { }

    void reset() { params = ""; }

public:


    static SingleRun & getInstance( HubController* hub );

    SingleRun & alerts( const string & alertLog ) { m_alertLog = alertLog; return instance; }

    SingleRun & events( const string & eventLog ) { m_eventLog = eventLog; return instance; }

    SingleRun & name( const string & _name ) { m_name = _name; return instance; }

    SingleRun & setConfig( const string & configfile );

    SingleRun & areIdentities( const string & filename );
    SingleRun & berthSteps( const string & filename );
    SingleRun & transitionBerths( const string & filename );

    SingleRun & iterations( int _iterations );

    SingleRun & startTime( const string & time );

    SingleRun & startTime( int seconds );

    SingleRun & logLevel( int level );

    SingleRun & runTime( int seconds );

    SingleRun & timeDelta( int seconds );

    SingleRun & timeRate( int rate );

    int searchAlerts( const string & key1, const string & key2 = "", const string & key3 = "" );
    int searchEvents( const string & key1, const string & key2 = "", const string & key3 = "" );

    string toString();

    void start();

    void run();
};
