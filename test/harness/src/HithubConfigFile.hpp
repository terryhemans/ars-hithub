#pragma once

//#include <occi.h>
#include <string>

using namespace std;

class HithubConfigFile
{

protected:

    string filename;

public:

    HithubConfigFile( string filename ){
        this->filename = filename;
    }

    string getFilename(){ return filename; }

    virtual string toString() = 0;

};
