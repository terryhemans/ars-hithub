#include "TestScript.hpp"
 
 
CPPUNIT_TEST_SUITE_REGISTRATION( class NTP_SATS_Tests );  // register the test script with CppUnit
 
 
class NTP_SATS_Tests : public TestScript
{
 
    ///
    /// Register the test cases with CppUnit: Add a CPPUNIT_TEST for each test case or it will not run!
    ///
 
    CPPUNIT_TEST_SUITE( NTP_SATS_Tests );
 
    TEST( Successful_Startup_Sync_Check );
    TEST( Unsuccessful_Startup_Sync_Check );
    TEST( Sync_On_Startup_Sync_Failure );
    TEST( Missing_Startup_File_Check );
    TEST( Missing_Startup_File_Key_Check );
    TEST( Invalid_Startup_File_Check );
    TEST( tearDownScript, ALL );
 
    CPPUNIT_TEST_SUITE_END();
 
 
public:
 
    ///
    /// Test Cases
    ///
 
    void Successful_Startup_Sync_Check()
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Successful_Startup.cfg", "bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().iterations(1).logLevel(0).run();
        
        assertMsg("No NTP startup sync message logged", Hub.go().searchEvents("<ntp>", "NTP Successfully Synced on VM Startup") > 0);
    }
    
    void Unsuccessful_Startup_Sync_Check() 
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Failed_Startup.cfg", "bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().iterations(1).logLevel(0).run();
        
        assertMsg("No NTP startup sync message logged", Hub.go().searchEvents("<ntp>", "NTP Unsuccessfully Synced on VM Startup") > 0);
    }
    
    void Sync_On_Startup_Sync_Failure()
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Failed_Startup.cfg", "bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().timeDelta(1000).runTime(300).logLevel(0).run();
        
        assertMsg("No NTP startup sync failure message logged", Hub.go().searchEvents("<ntp>", "NTP Unsuccessfully Synced on VM Startup") > 0);
        assertMsg("No NTP sync message logged", Hub.go().searchEvents("<ntp>", "NTP Synchronisation Status change: Synchronised") > 0);
    }
    
    void Missing_Startup_File_Check()
    {
        system("rm -f bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().iterations(1).logLevel(0).run();
        
        assertMsg("No Alert message logged", Hub.go().searchEvents("<ntp>", "Unable to read NTP Config File. Unable to determine success state of NTP Startup Synchronisation") > 0);
        assertMsg("No Alert message logged", Hub.go().searchAlerts("Failed to sync with the NTP server on Startup") > 0);
        
        system("touch bin/ntpStartupSyncStatus.cfg");
    }
    
    void Missing_Startup_File_Key_Check()
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Empty.cfg", "bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().iterations(1).logLevel(0).run();
        
        assertMsg("No Alert message logged", Hub.go().searchEvents("<ntp>", "Startup.Success.State is missing from NTP Config File. Unable to determine success state of NTP Startup Synchronisation") > 0);
        assertMsg("No Alert message logged", Hub.go().searchAlerts("Failed to sync with the NTP server on Startup") > 0);
    }
    
    void Invalid_Startup_File_Check()
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Invalid.cfg", "bin/ntpStartupSyncStatus.cfg");
        
        Hub.go().iterations(1).logLevel(0).run();
        
        assertMsg("No Alert message logged", Hub.go().searchEvents("<ntp>", "NTP Config File is invalid. Unable to determine success state of NTP Startup Synchronisation") > 0);
        assertMsg("No Alert message logged", Hub.go().searchAlerts("Failed to sync with the NTP server on Startup") > 0);
    }
    
    void tearDownScript()
    {
        Hub.installConfigFile("test_config/ntpStartupSyncStatus_Successful_Startup.cfg", "bin/ntpStartupSyncStatus.cfg");
    }
    
    ///
    /// Test case setup and clean up.  CppUnit runs setUp before each test case and tearDown after.
    /// Override if required.
    ///
 
    void setUp() {}
    void tearDown() {}
 
};