/* @(#) MQMBID sn=p800-004-151022.DE su=_8QwZKXivEeWg74sVC8pxOw pn=include/imqi.pre_hpp */
#ifndef _IMQI_HPP_
#define _IMQI_HPP_

//  Library:       WebSphere MQ
//  Component:     IMQI (WebSphere MQ C++ MQI)
//  Part:          IMQI.HPP
//
//  Description:   All WebSphere MQ MQI class declarations
//  <copyright 
//  notice="lm-source-program" 
//  pids="" 
//  years="1994,2005" 
//  crc="2103787634" > 
//  Licensed Materials - Property of IBM  
//   
//   
//   
//  (C) Copyright IBM Corp. 1994, 2005 All Rights Reserved.  
//   
//  US Government Users Restricted Rights - Use, duplication or  
//  disclosure restricted by GSA ADP Schedule Contract with  
//  IBM Corp.  
//  </copyright> 

#include <imqair.hpp> // ImqAuthenticationRecord
#include <imqbin.hpp> // ImqBinary
#include <imqcac.hpp> // ImqCache
#include <imqchl.hpp> // ImqChannel
#include <imqcih.hpp> // ImqCICSBridgeHeader
#include <imqdlh.hpp> // ImqDeadLetterHeader
#include <imqdst.hpp> // ImqDistributionList
#include <imqerr.hpp> // ImqError
#include <imqgmo.hpp> // ImqGetMessageOptions
#include <imqhdr.hpp> // ImqHeader
#include <imqiih.hpp> // ImqIMSBridgeHeader
#include <imqitm.hpp> // ImqItem
#include <imqmsg.hpp> // ImqMessage
#include <imqmtr.hpp> // ImqMessageTracker
#include <imqnml.hpp> // ImqNamelist
#include <imqobj.hpp> // ImqObject
#include <imqpro.hpp> // ImqProcess
#include <imqpmo.hpp> // ImqPutMessageOptions
#include <imqque.hpp> // ImqQueue
#include <imqmgr.hpp> // ImqQueueManager
#include <imqrfh.hpp> // ImqReferenceHeader
#include <imqstr.hpp> // ImqString
#include <imqtrg.hpp> // ImqTrigger
#include <imqwih.hpp> // ImqWorkHeader

#endif
